import React, {Component} from 'react'
import T from 'i18n-react';
import { Switch, Route } from 'react-router-dom'

import CreatePost from './SocialNetwork/CreatePost'
import SocialNetwork from './SocialNetwork/SocialNetwork'
import Login from './Login'
import Header from './Header/Header'
import HomePage from './pages/HomePage';
import MapPage from './pages/MapPage';
import ChargerDetailPage from './pages/ChargerDetailPage';
import MyAccount from './pages/MyAccount';
import Footer from './Header/Footer';
import PrivacyPolicy from './Header/PrivacyPolicy';
import TermsConditions from './Header/TermsConditions';
import AboutUs from './Header/AboutUs';
import ContactUs from './Header/ContactUs';
import FileUpload from './FileUpload/FileUpload';
import SearchPost from './SocialNetwork/SearchPost';



let language = (navigator.languages && navigator.languages[0]) || navigator.language || navigator.userLanguage;
// I'm not worried about pt_PT an pt_BR and so on. portuguese is portuguese and english is english
if (language.length > 2) {
    language = language.split("-")[0];
    language = language.split("_")[0];
}

T.setTexts(require('./i18n/'+language+'.json'));



class App extends Component {

  /* this callback this check when language change and will rerender the app */
  changeLanguage = (lang) => {
    let l = lang;
    if(lang === "gb"){l="en"}
    T.setTexts(require(`./i18n/${l}.json`));
    this.forceUpdate();
  }

  /* <div style={{ backgroundImage: `url("https://d2v9y0dukr6mq2.cloudfront.net/video/thumbnail/4RUieTItxik2c5fvx/simple-animated-geometric-background-with-squares-4k-ultra-high-definition-video-loop_hzhlwak8x_thumbnail-full01.png")`, height: "100%" }}> */

  render() {
    return (
      <div className='center w85' style={{ height: "100%" }} >
        <div style={{ height:"10%" }}><Header changeLanguageCallback={this.changeLanguage} /></div>
        <div className='ph3 pv1 background-gray' style={{ height:"85%" }}>
          <Switch>
            <Route exact path='/' component={HomePage}/>
            <Route exact path='/create' component={CreatePost}/>
            <Route exact path='/social' component={SocialNetwork}/>
            <Route exact path='/signin' component={Login}/>
            <Route exact path='/map' component={MapPage}/>
            <Route exact path='/charger' component={ChargerDetailPage} />
            <Route exact path='/my_account' component={MyAccount} />
            <Route exact path='/privacy_policy' component={PrivacyPolicy} />
            <Route exact path='/terms_and_conditions' component={TermsConditions} />
            <Route exact path='/about_us' component={AboutUs} />
            <Route exact path='/contact_us' component={ContactUs} />
            <Route exact path='/file_upload' component={FileUpload} />

            <Route exact path='/search' component={SearchPost} />
            
          </Switch>
        </div>
        <div style={{height:"5%"}}>
          <Footer />
        </div>
      </div>
   )
  }
} 

export default App
