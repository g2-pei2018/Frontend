import React, { Component } from 'react'
import { Feed, Card } from 'semantic-ui-react'
import T from  'i18n-react';

import rita_lang from '../../img/staff/rita_lang.png';
import rui_lang from '../../img/staff/rui_lang.png';
import bernas from '../../img/staff/bernas.png';
import carvalheira from '../../img/staff/carvalheira.png';
import lima from '../../img/staff/lima.png';
import mario from '../../img/staff/mario.png';
import nuno from '../../img/staff/nuno.png';
import rafa from '../../img/staff/rafa.png';
import rui from '../../img/staff/rui.png';
import zecarlos from '../../img/staff/zecarlos.png';

const consultants = [ {name: "Rita Faria",
                 username: "Rita Faria", 
                 avatar: rita_lang,
                 insertedAt: "2018-01-08 17:49:14.581924"
                },
                {name: "Rui Teixeira",
                 username: "Rui Teixeira", 
                 avatar: rui_lang,
                 insertedAt: "2018-01-08 17:49:14.581924"
                }
            ]
const team1 = [ {name: "João Costa",
                 username: "João Costa", 
                 avatar: bernas
                },
                {name: "José Brandão",
                 username: "José Brandão", 
                 avatar: zecarlos,
                 insertedAt: "2018-01-08 17:49:14.581924"
                },
                {name: "Mário Ferreira",
                 username: "Mário Ferreira", 
                 avatar: mario,
                 insertedAt: "2018-01-08 17:49:14.581924"
                },
                {name: "Nuno Cunha",
                 username: "Nuno Cunha", 
                 avatar: nuno,
                 insertedAt: "2018-01-08 17:49:14.581924"
                }
              ]

const team2 = [ 
             {name: "Rafael Barbosa",
              username: "Rafael Barbosa", 
              avatar: rafa,
              insertedAt: "2018-01-08 17:49:14.581924"
             },
             {name: "Rui Soares",
              username: "Rui Soares", 
              avatar: rui,
              insertedAt: "2018-01-08 17:49:14.581924"
             },
             {name: "Tiago Carvalheira",
              username: "Tiago Carvalheira", 
              avatar: carvalheira,
              insertedAt: "2018-01-08 17:49:14.581924"
             },
             {name: "Tiago Lima",
              username: "Tiago Lima", 
              avatar: lima,
              insertedAt: "2018-01-08 17:49:14.581924"
             }
           ]

class AboutUs extends Component {

  renderFeedTeam = (item, index, num) =>{
    return (
      <Feed.Event>
      <Feed.Label image={item.avatar} style={{ width: "30%" }} />
      <Feed.Content style={{ marginTop: "8%" }}>
        <Feed.Date content='Developer' />
        <Feed.Summary>
        {item.username}
        </Feed.Summary>
      </Feed.Content>
    </Feed.Event>)
  };

  renderFeedConsultant = (item, index) =>{
    return (
      <Feed.Event>
      <Feed.Label image={item.avatar} style={{ width: "30%" }}/>
      <Feed.Content style={{ marginTop: "8%" }}>
        <Feed.Date content='Consultant' />
        <Feed.Summary>
        {item.username}
        </Feed.Summary>
      </Feed.Content>
    </Feed.Event>)
  };

  render() {
    const teamList1 = team1.map((result, index) =>{
      return this.renderFeedTeam(result, index);
    })
    const teamList2 = team2.map((result, index) =>{
      return this.renderFeedTeam(result, index);
    })
    const consultantsList = consultants.map((result, index) =>{
      return this.renderFeedConsultant(result, index);
    })
    return <div style={{marginBottom:"50px"}}>
              <Card style={{marginTop:"40px", marginLeft:"300px"}}>
                  <Card.Content>
                    <Card.Header style={{textAlign:"center"}}>
                    {T.translate('aboutUs.aboutUs')}
                    </Card.Header>
                  </Card.Content>
                  <Card.Content>
                  {T.translate('aboutUs.about1')}
                  <br/>
                  <br/>
                  {T.translate('aboutUs.about2')}
                  </Card.Content>
                </Card> 
                <Card style={{ marginLeft:"650px", marginTop:"-320px", width: "17%" }}>
                    <Card.Header style={{textAlign:"center"}}>
                    {T.translate('aboutUs.team')}
                    </Card.Header>
                  <Card.Content>
                    <Feed>
                      { teamList1 }
                      { teamList2 }
                      { consultantsList }
                    </Feed>
                  </Card.Content>
                </Card>
          </div>
             
  }
}

export default AboutUs;

