import React, { Component } from 'react'
import { Card, Icon } from 'semantic-ui-react'
import T from 'i18n-react';

class ContactUs extends Component {
 state={}
 render() {
    return <div style={{marginBottom:"50px"}}>
            <Card centered style={{width: "700px", marginTop: "30px"}}>
                <Card.Content style={{textAlign:"center"}} header={T.translate('contactUs')} />
                <Card.Content description={T.translate('contactUs.support1')} />
                <Card.Content extra>
                    <span style={{marginRight: "20px"}}>{T.translate('contactUs.support2')}</span>
                    <Icon style={{marginRight: "150px"}} name='home'>
                        <a style={{marginLeft: "10px"}} target="_blank" rel="noopener noreferrer" href="https://www.wattcharger.eu">www.wattcharger.eu</a>
                    </Icon> 
                    <Icon style={{marginRight: "120px", marginLeft: "10px"}} name='facebook official'>
                        <a style={{marginLeft: "10px"}} href="https://www.facebook.com/WattCharger" target="_blank" rel="noopener noreferrer">WattCharger</a>
                    </Icon>  
                    <Icon name='twitter'>
                    <a style={{marginLeft: "10px"}} href="https://www.twitter.com/WattCharger" target="_blank" rel="noopener noreferrer">@WattCharger</a>
                    </Icon>    
                </Card.Content>
                <Card.Content extra>
                    <span style={{marginRight: "30px"}}>{T.translate('contactUs.support3')}</span>
                    <a href="mailto:wattchargerinfo@gmail.com" target="_top">wattchargerinfo@gmail.com</a>
                </Card.Content>
            </Card>
            </div>
  }
}

export default ContactUs;

