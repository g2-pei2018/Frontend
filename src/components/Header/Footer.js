import React, { Component } from 'react'
import { withRouter } from 'react-router'
import {Link} from 'react-router-dom';
import { List } from 'semantic-ui-react'
import T from 'i18n-react';
// import FooterGrid from './FooterGrid';

class Footer extends Component {
  state = {}
  render() {
    return (
      <div>
        <footer style={{ position: "fixed", backgroundColor:"#FFFFFF", right: "0", bottom: "0", left: "0", paddingBottom: "1rem", textAlign: "center" }}>
        {/* <a style={{ color: "teal" }} href="http://wattcharger.eu/"> © 2018 WattCharger</a> */}
        <List divided horizontal style={{ marginTop:"2px", display: "block", color: "grey", fontSize: "40px" }} >
          <List.Item  as={Link} to={`/about_us`} >{T.translate("footer.aboutUs")}</List.Item>
          <List.Item as={Link} to={`/contact_us`}>{T.translate("footer.contactUs")}</List.Item>
          <List.Item as={Link} to={`/terms_and_conditions`}>{T.translate("footer.termsConditions")}</List.Item>
          <List.Item as={Link} to={`/privacy_policy`}>{T.translate("footer.privacyPolicy")}</List.Item>
        </List>
        </footer>
      </div>
    )
  }

  // render() {
  //   return (
  //     <div>
  //       <footer style={{ position: "absolute", right: "0", bottom: "0", left: "0", padding: "1rem", backgroundColor: "#ffffff", textAlign: "center" }}>
  //       <FooterGrid />
  //       <a style={{ color: "teal" }} href="http://wattcharger.eu/"> © 2018 WattCharger</a>
  //       <List divided horizontal style={{ marginTop:"15px", display: "block", color: "grey", fontSize: "50px" }} >
  //         <List.Item>About Us</List.Item>
  //         <List.Item>Contact Us</List.Item>
  //         <List.Item>Terms and Conditions</List.Item>
  //         <List.Item>Support</List.Item>
  //         <List.Item>Privacy Policy</List.Item>
  //       </List>
  //       </footer>
  //     </div>
  //   )
  // }

}

export default withRouter(Footer);
