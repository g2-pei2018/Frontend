import React from 'react'
import { Grid } from 'semantic-ui-react'

// const FooterGrid = () => (
//   <Grid textAlign="center" style={{ marginBottom: "50px" }}>
//     <Grid.Row style={{color: "darkGray", width: "150px"}}>
//       <Grid.Column style={{ fontSize: "24px" }}>Product</Grid.Column>
//       <Grid.Column style={{ fontSize: "24px" }}>Help</Grid.Column>
//       <Grid.Column style={{ fontSize: "24px" }}>Community</Grid.Column>
//       <Grid.Column style={{ fontSize: "24px", marginLeft: "45px" }}>Company</Grid.Column>
//     </Grid.Row>
//     <Grid.Row style={{ width: "150px" }}>
//       <Grid.Column style={{ fontSize: "16px" }}>Explore</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px" }}>FAQ</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px" }}>Facebook</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px", marginLeft: "45px" }}>About Us</Grid.Column>
//     </Grid.Row>
//     <Grid.Row style={{ width: "150px" }}>
//       <Grid.Column style={{ fontSize: "16px" }}>Features</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px" }}>Feedback</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px" }}>Twitter</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px", marginLeft: "45px" }}>Terms & Conditions</Grid.Column>
//     </Grid.Row> 
//     <Grid.Row style={{ width: "150px" }}>
//       <Grid.Column style={{ fontSize: "16px" }}>Pricing</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px" }}>Report </Grid.Column>
//       <Grid.Column style={{ fontSize: "16px" }}/>
//       <Grid.Column style={{ fontSize: "16px", marginLeft: "45px" }}>Privacy Policy</Grid.Column>
//     </Grid.Row>
//     <Grid.Row style={{ width: "150px" }}>
//       <Grid.Column style={{ fontSize: "16px" }}>Developers</Grid.Column>
//       <Grid.Column style={{ fontSize: "16px" }}/>
//       <Grid.Column style={{ fontSize: "16px" }}/>
//       <Grid.Column style={{ fontSize: "16px", marginLeft: "45px" }}/>
//     </Grid.Row>
//   </Grid>
// )

const FooterGrid = () => (
    <Grid textAlign="center" style={{ marginBottom: "50px", marginLeft: "100px", backgroundColor:"lightGray", width:"90%" }}>
      <Grid.Row style={{ color: "darkGray" }}>
        <Grid.Column style={{ fontSize: "24px", marginRight: "100px" }}>Product</Grid.Column>
        <Grid.Column style={{ fontSize: "24px", marginRight: "100px" }}>Help</Grid.Column>
        <Grid.Column style={{ fontSize: "24px", marginRight: "100px" }}>Community</Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Explore</Grid.Column>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>FAQ</Grid.Column>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Facebook</Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Features</Grid.Column>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Feedback</Grid.Column>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Twitter</Grid.Column>
      </Grid.Row> 
      <Grid.Row>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Pricing</Grid.Column>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Report </Grid.Column>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}/>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}>Developers</Grid.Column>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}/>
        <Grid.Column style={{ fontSize: "16px", marginRight: "100px"  }}/>
      </Grid.Row>
    </Grid>
  )

export default FooterGrid