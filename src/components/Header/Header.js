import React, { Component } from 'react'
import { withRouter } from 'react-router'
import { Link } from 'react-router-dom'
// import { Menu, Message } from 'semantic-ui-react'
import { Menu } from 'semantic-ui-react'
import T from 'i18n-react';
// import {Link} from 'react-router-dom';
import {PropTypes} from 'prop-types';
import { GC_USER_ID, GC_AUTH_TOKEN } from '../Authentication/constants'
import LanguageSelector from './LanguageSelector';
import NotFound from './NotFound';
 
class Header extends Component {
  
  constructor(props){
    super(props);
    this.state = {activeItem:null,error:false}
    switch (window.location.pathname) {
      case '/':
        this.state.activeItem = T.translate('header.wattcharger');
        this.state.error = false;
      break;
      case '/social':
        this.state.activeItem = T.translate('header.social');
        this.state.error = false;
      break;
      case '/map':
      this.state.activeItem = T.translate('header.map');
      this.state.error = false;
      break;
      case '/signin':
      this.state.activeItem = T.translate('header.my_account');
      this.state.error = false;
      break;
      case '/my_account':
      this.state.activeItem = T.translate('header.my_account');
      this.state.error = false;
      break;
      case '/about_us':
      this.state.activeItem = T.translate('header.wattcharger');
      this.state.error = false;
      break;
      case '/contact_us':
      this.state.activeItem = T.translate('header.wattcharger');
      this.state.error = false;
      break;
      case '/terms_and_conditions':
      this.state.activeItem = T.translate('header.wattcharger');
      this.state.error = false;
      break;
      case '/privacy_policy':
      this.state.activeItem = T.translate('header.wattcharger');
      this.state.error = false;
      break;
      case '/file_upload':
      this.state.activeItem = T.translate('header.wattcharger');
      this.state.error = false;
      break;
      case '/search':
      this.state.activeItem = T.translate('header.wattcharger');
      this.state.error = false;
      break;
      default:
        // ERROR SEND
        this.state.activeItem = T.translate('header.wattcharger');
        this.state.error = true;
      break;
    }
  }
 
  state = {}

  /* this callback will check when the language change */
  changeLanguage = (newLang) => {
    this.props.changeLanguageCallback(newLang);
  }
 
  handleItemClick = (e, { name }) => this.setState({ activeItem: name })
  // <Menu.Item name={T.translate('header.submit')} href="/create" />
 
  render() {
 
    const userId = localStorage.getItem(GC_USER_ID)
    const {activeItem} = this.state
 
    return (<div>
      <Menu pointing secondary style={{ marginBottom: "20px" }}>
        <Menu.Item name={T.translate('header.wattcharger')} active={activeItem === T.translate('header.wattcharger') } as={Link} to={`/`} onClick={this.handleItemClick}/>
        <Menu.Item name={T.translate('header.social')} active={activeItem === T.translate('header.social') } as={Link} to={`/social`} onClick={this.handleItemClick}/>
        <Menu.Item name={T.translate('header.map')} active={activeItem === T.translate('header.map')} as={Link} to={`/map`} onClick={this.handleItemClick}/>
        <Menu.Menu position='right'>
         
        {
          (userId===null) ? <Menu.Item className="hide" as={Link} to={`/my_account`} name={T.translate('header.my_account')} active={activeItem === T.translate('header.my_account')} onClick={this.handleItemClick}/> :  
          <Menu.Item className="show" as={Link} to={`/my_account`} name={T.translate('header.my_account')} active={activeItem === T.translate('header.my_account')} onClick={this.handleItemClick}/>
        }
 
        {userId ?  <Menu.Item onClick={() => {
              localStorage.removeItem(GC_USER_ID)
              localStorage.removeItem(GC_AUTH_TOKEN)
            } }  href='/' name={T.translate('header.logout')} />
              :
        <Menu.Menu position='right'>
          <Menu.Item as={Link} to={`/signin`} active={activeItem === T.translate('header.signin')} onClick={this.handleItemClick} >{T.translate('header.signin') } </Menu.Item>
        </Menu.Menu>
        }
 
        {/* <Menu.Item href="/signin" name={T.translate('header.signin')+T.translate('header.or')+T.translate('header.signup')}/> */}
        </Menu.Menu>
        <LanguageSelector changeLanguage={this.changeLanguage} />
      </Menu>
       {/* {this.state.error ?  <Message style={{"textAlign":"center"}} negative>
                                <Message.Header>Page not found</Message.Header>
                            </Message> 
                          : null
       } */}
     
      { this.state.error ? <NotFound /> : null }
      
    </div>
    )
  }
  // To change background colors(current color is teal) https://semantic-ui.com/usage/theming.html
}
 
Header.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  changeLanguageCallback: PropTypes.func.isRequired
}
 
export default withRouter(Header);