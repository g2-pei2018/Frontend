import React, {Component} from 'react'
import { Dropdown, Flag } from 'semantic-ui-react'
import {PropTypes} from 'prop-types';
// import T from 'i18n-react';

// const countryFullOptions = [ 
//     { key: 'pt', value: 'pt', flag: 'pt', text: 'Português' },
//     { key: 'br', value: 'br', flag: 'br', text: 'Português BR' },
//     { key: 'fr', value: 'fr', flag: 'fr', text: 'Français' },
//     { key: 'de', value: 'de', flag: 'de', text: 'Deutsch' },
//     { key: 'es', value: 'es', flag: 'es', text: 'Espãnol' },
//     { key: 'gb', value: 'gb', flag: 'gb', text: 'English' },
//     { key: 'us', value: 'us', flag: 'us', text: 'English USA' }
//  ]

// const countryOptions = [ 
//     { key: 'pt', value: 'pt', text: <Flag style={{ display: "inline" }} name='portugal'><span style={{ paddingLeft: "15px" }}>Português</span></Flag> },
//     { key: 'fr', value: 'fr', text: <Flag style={{ display: "inline" }} name='france'><span style={{ paddingLeft: "15px" }}>Français</span></Flag> },
//     { key: 'de', value: 'de', text: <Flag style={{ display: "inline" }} name='germany'><span style={{ paddingLeft: "15px" }}>Deutsch</span></Flag> },
//     { key: 'es', value: 'es', text: <Flag style={{ display: "inline" }} name='spain'><span style={{ paddingLeft: "15px" }}>Espãnol</span></Flag> },
//     { key: 'gb', value: 'gb', text: <Flag style={{ display: "inline" }} name='united kingdom'><span style={{ paddingLeft: "15px" }}>English</span></Flag> }
//  ] 

 const countryOptions = [ 
  { key: 'pt', value: 'pt', text: <Flag style={{ display: "inline" }} name='portugal'><span style={{ paddingLeft: "15px" }}>Português</span></Flag> },
  { key: 'gb', value: 'gb', text: <Flag style={{ display: "inline" }} name='united kingdom'><span style={{ paddingLeft: "15px" }}>English</span></Flag> }
] 

 /* when change language pass it to parent */
 class LanguageSelector extends Component {
  changeHandle = (event, data) => {
    this.props.changeLanguage(data.value);
  }

  render() {
    return (
        <Dropdown defaultValue='gb' onChange={this.changeHandle} style={{ width: "150px", border:"None" }} placeholder='Language' fluid search selection options={countryOptions} />
   )
  }
 }

 LanguageSelector.propTypes = {
  /* history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired, */
  changeLanguage: PropTypes.func.isRequired
}

export default LanguageSelector