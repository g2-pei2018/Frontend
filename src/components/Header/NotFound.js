import React, { Component } from 'react'
import { Button, Header, Icon, Modal } from 'semantic-ui-react'

export default class NotFound extends Component {
  state = { modalOpen: true }


  handleOpen = () => this.setState({ modalOpen: true })

  handleClose = () => {
    this.setState({ modalOpen: false }); }
  
  handleClose2 = () => {
    this.setState({ modalOpen: false }); 
    window.location.href="/";  
  }

  render() {
    return (
      <Modal style={{marginLeft:"-150px"}}
        open={this.state.modalOpen}
        onClose={this.handleClose}
        basic
        size='small'
        closeIcon
      >
        <Header icon='warning sign' content='Error 404: Page Not Found' />
        <Modal.Content>
          <h3>To return to homepage</h3>
        </Modal.Content>
        <Modal.Actions style={{marginRight:"300px"}}>
        <Button color='red' onClick={this.handleClose} inverted>
            <Icon name='checkmark' /> No
        </Button>
        <Button color='green' onClick={this.handleClose2} inverted>
            <Icon name='checkmark' /> Yes
        </Button>
        </Modal.Actions>
      </Modal>
    )

  }
}