import React, { Component } from 'react'
import { Card } from 'semantic-ui-react'
import T from 'i18n-react';

class PrivacyPolicy extends Component {
 state = {}
 render() {
    return <Card centered style={{width: "700px", marginTop: "30px"}}>
                <Card.Content style={{textAlign:"center"}} header={T.translate('privacyPolicies.privacyPolicy')} />
                <Card.Content description={`1. ${T.translate('privacyPolicies.policy1')}`} />
                <Card.Content description={`2. ${T.translate('privacyPolicies.policy2')}`} />
                <Card.Content description={`3. ${T.translate('privacyPolicies.policy3')}`} />
            </Card>
  }
}

export default PrivacyPolicy;

