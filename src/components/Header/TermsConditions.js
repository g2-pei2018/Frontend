import React, { Component } from 'react'
import { Card } from 'semantic-ui-react'
import T from 'i18n-react';

class TermsConditions extends Component {
 state = {}
 render() {
    return <div style={{marginBottom:"50px"}}>
        <Card centered style={{width: "700px", marginTop: "30px"}}>
                <Card.Content style={{textAlign:"center"}} header={T.translate('termsConditions')} />
                <Card.Content description={`1. ${T.translate('termsConditions.term1')}`} />
                <Card.Content description={`2. ${T.translate('termsConditions.term2')}`} />
                <Card.Content description={`3. ${T.translate('termsConditions.term3')}`} />
                <Card.Content description={`4. ${T.translate('termsConditions.term4')}`} />
                <Card.Content description={`5. ${T.translate('termsConditions.term5')}`} />
                <Card.Content description={`6. ${T.translate('termsConditions.term6')}`} />
                <Card.Content description={`7. ${T.translate('termsConditions.term7')}`} />
                <Card.Content description={`8. ${T.translate('termsConditions.term8')}`} />
            </Card>
        </div>
  }
}

export default TermsConditions;

