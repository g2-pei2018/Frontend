import React, { Component } from 'react';
import { Grid, Tab} from 'semantic-ui-react';
import T from 'i18n-react';
import {PropTypes} from 'prop-types';
import SignupForm from './forms/SignupForm';
import SigninForm from './forms/SigninForm';

class Login extends Component {
  state = { }
  
  render() {
    return (
        <Grid centered>
          <Grid.Row>
            <Grid.Column width={6}>
              <Tab menu={{ secondary: true, pointing: true }} panes={[
                        { menuItem: T.translate('signin.signin'), render: () => <Tab.Pane history={'/'} attached={false}><SigninForm history={this.props.history}/></Tab.Pane> },
                        { menuItem: T.translate('signin.signup'), render: () => <Tab.Pane history={'/'} attached={false}><SignupForm history={this.props.history}/></Tab.Pane> },
                      ]} />
            </Grid.Column>
          </Grid.Row>
        </Grid>
    )
  }

}

Login.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};

  export default Login