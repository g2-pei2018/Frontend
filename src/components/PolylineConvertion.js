export const CHEAPEST_JOURNEY = "CHEAPEST_JOURNEY";
export const FASTEST_JOURNEY = "FASTEST_JOURNEY";
export const MO_FASTEST_JOURNEY = "MO_FASTEST_JOURNEY";

export const ROUTE_COLOR = "#aaaaaa";
export const BORDER_ROUTE_COLOR = "#888888";

export const CHEAPEST_ROUTE_COLOR = '#99CC00';
export const FASTEST_ROUTE_COLOR = '#FF4444';
export const MO_FASTEST_ROUTE_COLOR = '#1c94c4';

export const CHEAPEST_ROUTE_BORDER_COLOR = "#70A300";
export const FASTEST_ROUTE_BORDER_COLOR = "#D30808";
export const MO_FASTEST_ROUTE_BORDER_COLOR = "#165694";

const allChartOptions = {
  max:110,
  min:0,
  usedSteps: [],
  travelTimes: [],
  data: [] // 0-cheapest, 1-fastest
};

function createLatLng(coordinates) {
    const lat = coordinates.latE6 / 1E6;
    const lng = coordinates.lonE6 / 1E6;
    return {latitude: lat,longitude: lng};
}

export default function iterateAllPlans(_plans) {
    const plans = _plans; // JSON.parse(_plans).plans
     // console.log(plans)
    const finalResult = {routes: [], markers: []}
  allChartOptions.data = [];
  allChartOptions.usedSteps = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < plans.length; i++) {
      const oneBasicRouteLatLngs = [];
      const segments = plans[i].segments;
      const type = plans[i].type;
      const statesOfCharge = [];
      const lengths = [];
      const travelTimes = [];
      const XYData = [];
      const usedSteps = [];
      const chargers = [];
      let travelTime = 0;
      let length = 0;
      // eslint-disable-next-line no-unused-vars
      let numOfSOC = 0;

      // eslint-disable-next-line no-plusplus
      for (let j = 0; j < (segments.length); j++) {
          const from = segments[j].from;
          if (from.type === "SUPERCHARGER") {
              from.coordinates= createLatLng(from.coordinates)
              chargers.push(from);
          }

          let steps = [];
          steps = steps.concat(from, segments[j].viaSteps);

          const to = segments[j].to;
          if (to.type === "DESTINATION") {
              steps.push(to);
          }

          // eslint-disable-next-line no-plusplus
          for (let k = 1; k < (steps.length-1); k++) {
              const latLon = createLatLng(steps[k].coordinates);
              oneBasicRouteLatLngs.push(latLon);
              let stateOfCharge = steps[k].leavingStateOfCharge;
              const maxSoc = 85000;
              if (stateOfCharge > 0) {
                  // console.log(length+","+stateOfCharge + ", " + L.latLng(lat, lng));
                  stateOfCharge = (stateOfCharge/maxSoc)*100;
                  statesOfCharge.push(stateOfCharge);
                  // console.log(stateOfCharge + ", " + length);
                  lengths.push(length);
                  travelTimes.push(travelTime);
                  XYData.push([length, stateOfCharge]);
                  usedSteps.push(latLon);
                  // eslint-disable-next-line no-plusplus
                  numOfSOC++;
              }

              const ttToNext = steps[k].travelTimeToNextStep;
              const distToNext = steps[k].distanceToNextStep;
              if (distToNext > 0) {
                  length += distToNext;
                  travelTime += ttToNext;
              }
          }
      }

      chargers.map(x => finalResult.markers.push(x))
      finalResult.routes.push({route: oneBasicRouteLatLngs,type})

      // createButtonForRoute(plans[i], i);
      allChartOptions.data.push(XYData);
      allChartOptions.usedSteps.push(usedSteps);
      allChartOptions.travelTimes.push(travelTimes);
  }
  return(finalResult)
}
