import React, { Component } from 'react'
import {PropTypes} from 'prop-types';
import { graphql, compose } from 'react-apollo'
import { Form, Input } from 'semantic-ui-react'
import Dropzone from 'react-dropzone'
import axios from 'axios'
import { GC_USER_ID } from '../Authentication/constants'
import {CREATE_COMMENT_POST_MUTATION} from '../../graphql/mutations/Post/CreateCommentPostMutation'
import CREATE_COMMENT_MEDIA_MUTATION from '../../graphql/mutations/Post/CreateCommentMediaMutation'

const userId = localStorage.getItem(GC_USER_ID);

class CommentPost extends Component{

    constructor(props) {
        super(props);
        this.state = {
            comment: '',
            upvotes: 0, 
            downvotes: 0,
            postId: parseInt(this.props.postId,10),
            uploadedFileCloudinaryUrl:'',
            originalFileNameCloudinaryUrl:'',
            originalFileHeightCloudinaryUrl: 0,
            originalFileWidthCloudinaryUrl: 0,
            originalFileFormatCloudinaryUrl: '',
            createAtCloudinaryUrl: '',
            commentId: null
        };
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    state = {
        comment: '',
        uploadedFileCloudinaryUrl:'',
        originalFileNameCloudinaryUrl:'',
        originalFileHeightCloudinaryUrl: 0,
        originalFileWidthCloudinaryUrl: 0,
        originalFileFormatCloudinaryUrl: '',
        createAtCloudinaryUrl: ''
    }

    handleChange(event) {
        this.setState({comment: event.target.value});
    }

    handleSubmit(event) {
        // console.log(`User ${userId} commented: ${this.state.comment}`);
        // console.log(this.props.postId);
        // console.log(this.state)
        const media  = this.state.uploadedFileCloudinaryUrl
        console.log(media)
        
        const { comment, upvotes, downvotes, postId } = this.state
        this.props.CreatePostCommentMutation({
            variables: {
            comment, upvotes, downvotes, postId
            }
         }).then(res => {
                        // console.log(res.data.createComment.id)
                        this.setState({
                            commentId: res.data.createComment.id
                        })
                        const commentId = parseInt(res.data.createComment.id,10)
                        console.log(commentId)
                        if(media!==''){
                            this.props.CreateCommentMediaMutation({
                                variables: {
                                media,
                                commentId
                                }
                            }).then( res2 => console.log(res2.data.createCommentMedia)).catch( err => console.log(err))
                        }
                })
           .catch( err => console.log(err))

        
        event.preventDefault();
        this.setState({
            comment: '',
            uploadedFileCloudinaryUrl:'',
            originalFileNameCloudinaryUrl:'',
            originalFileHeightCloudinaryUrl: 0,
            originalFileWidthCloudinaryUrl: 0,
            originalFileFormatCloudinaryUrl: '',
            createAtCloudinaryUrl: '',
            commentId: null
        });
    }

    handleDrop = files => {
        // Push all the axios request promise into a single array
        const uploaders = files.map(file => {
          // Initial FormData
          const formData = new FormData();
          formData.append("file", file);
          formData.append("tags", `codeinfuse, medium, gist`);
          formData.append("upload_preset", "vpxzvsye"); // Replace the preset name with your own
          formData.append("api_key", "298453577541125"); // Replace API key with your own Cloudinary key
          formData.append("timestamp", (Date.now() / 1000) || 0);
          
          // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
          return axios.post("https://api.cloudinary.com/v1_1/wattcharger/image/upload", formData, {
            headers: { "X-Requested-With": "XMLHttpRequest" },
          }).then(response => {
            const data = response.data;
            const fileURL = data.secure_url // You should store this URL for future references in your app
            const fileName = data.original_filename
            const fileHeight = data.height
            const fileWidth = data.width
            const fileFormat = data.format
            const createAt = data.created_at
            // console.log(data);
            // console.log(fileURL);
            this.setState({uploadedFileCloudinaryUrl:fileURL});
            // console.log("setState... "+this.state.uploadedFileCloudinaryUrl);
            this.setState({originalFileNameCloudinaryUrl:fileName});
            // console.log("setState... "+this.state.originalFileNameCloudinaryUrl);
            this.setState({originalFileHeightCloudinaryUrl:fileHeight});
            // console.log("setState... "+this.state.originalFileHeightCloudinaryUrl);
            this.setState({originalFileWidthCloudinaryUrl:fileWidth});
            // console.log("setState... "+this.state.originalFileWidthCloudinaryUrl);
            this.setState({originalFileFormatCloudinaryUrl:fileFormat});
            // console.log("setState... "+this.state.originalFileFormatCloudinaryUrl);
            this.setState({createAtCloudinaryUrl:createAt});
            // console.log("setState... "+this.state.createAtCloudinaryUrl);
            
          })
        });  
      }

    render() {
        
        return (
            <div>
                <Dropzone
                    width="10px" 
                    onDrop={this.handleDrop} 
                    multiple 
                    accept="image/*" 
                    style={{marginLeft:"0px"}} >
                    <p style={{color: "grey"}}>Drop your files or click here to upload</p>
                    {this.state.uploadedFileCloudinaryUrl === '' ? null :
                        <img alt={this.state.originalFileNameCloudinaryUrl} style={{width:"100px", height:"100px", marginLeft: "50px", backgroundColor:"white"}} src={this.state.uploadedFileCloudinaryUrl} />
                    }
                </Dropzone>
                <br/>
                <Form onSubmit={this.handleSubmit}>
                    <Input transparent placeholder='Add a comment...' value={this.state.comment} onChange={this.handleChange}/>
                </Form>
            </div>
        );
    }
 
}

CommentPost.propTypes = {
    postId: PropTypes.string.isRequired,
    CreatePostCommentMutation: PropTypes.func.isRequired,
    CreateCommentMediaMutation: PropTypes.func.isRequired
};

export default compose(
    graphql(CREATE_COMMENT_POST_MUTATION, { name: 'CreatePostCommentMutation' }),
    graphql(CREATE_COMMENT_MEDIA_MUTATION, { name: 'CreateCommentMediaMutation' })
)(CommentPost)
