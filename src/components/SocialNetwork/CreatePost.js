import React, { Component } from 'react';
import {Message, Form, Grid, Button, Icon, Modal, Header} from 'semantic-ui-react';
import T from  'i18n-react';
import { graphql, compose } from 'react-apollo'
import PropTypes from 'prop-types'
import Dropzone from 'react-dropzone'
import axios from 'axios'
import { GC_USER_ID } from '../Authentication/constants'
import CREATE_POST_MUTATION from '../../graphql/mutations/Post/CreatePostMutation'
import CREATE_POST_MEDIA_MUTATION from '../../graphql/mutations/Post/CreatePostMediaMutation'


class CreatePost extends Component {

  constructor(props){
    super(props);

    this.state = {
      title: '',
      body: '',
      upvotes: 0,
      downvotes: 0,
      success: false,
      error: false,
      uploadedFileCloudinaryUrl:'',
      originalFileNameCloudinaryUrl:'',
      originalFileHeightCloudinaryUrl: 0,
      originalFileWidthCloudinaryUrl: 0,
      originalFileFormatCloudinaryUrl: '',
      createAtCloudinaryUrl: '',
      postId: null
    }
  }

  handleDrop = files => {
    // Push all the axios request promise into a single array
    const uploaders = files.map(file => {
      // Initial FormData
      const formData = new FormData();
      formData.append("file", file);
      formData.append("tags", `codeinfuse, medium, gist`);
      formData.append("upload_preset", "vpxzvsye"); // Replace the preset name with your own
      formData.append("api_key", "298453577541125"); // Replace API key with your own Cloudinary key
      formData.append("timestamp", (Date.now() / 1000) || 0);
      
      // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
      return axios.post("https://api.cloudinary.com/v1_1/wattcharger/image/upload", formData, {
        headers: { "X-Requested-With": "XMLHttpRequest" },
      }).then(response => {
        const data = response.data;
        const fileURL = data.secure_url // You should store this URL for future references in your app
        const fileName = data.original_filename
        const fileHeight = data.height
        const fileWidth = data.width
        const fileFormat = data.format
        const createAt = data.created_at
        // console.log(data);
        // console.log(fileURL);
        this.setState({uploadedFileCloudinaryUrl:fileURL});
        // console.log("setState... "+this.state.uploadedFileCloudinaryUrl);
        this.setState({originalFileNameCloudinaryUrl:fileName});
        // console.log("setState... "+this.state.originalFileNameCloudinaryUrl);
        this.setState({originalFileHeightCloudinaryUrl:fileHeight});
        // console.log("setState... "+this.state.originalFileHeightCloudinaryUrl);
        this.setState({originalFileWidthCloudinaryUrl:fileWidth});
        // console.log("setState... "+this.state.originalFileWidthCloudinaryUrl);
        this.setState({originalFileFormatCloudinaryUrl:fileFormat});
        // console.log("setState... "+this.state.originalFileFormatCloudinaryUrl);
        this.setState({createAtCloudinaryUrl:createAt});
        // console.log("setState... "+this.state.createAtCloudinaryUrl);
        
      })
    });  
  }

  createPost = async () => {
    const postedById = localStorage.getItem(GC_USER_ID)

    if (!postedById) {
      console.error('No user logged in')
      return
    }
    const { title, body, upvotes, downvotes } = this.state
    try{
      const result = await this.props.createPostMutation({
        variables: {
          title,
          body,
          upvotes,
          downvotes
        }
      })
      // console.log(result)
      // console.log(`New Post ID ${result.data.createPost.id}`)

      this.setState({
        postId: result.data.createPost.id
      })

      if(this.state.uploadedFileCloudinaryUrl!==''){
        this.createMedia()
      }else{
        this.setState({
          success: true,
          uploadedFileCloudinaryUrl:'',
          originalFileNameCloudinaryUrl:'',
          originalFileHeightCloudinaryUrl: 0,
          originalFileWidthCloudinaryUrl: 0,
          originalFileFormatCloudinaryUrl: '',
          createAtCloudinaryUrl: '',
          postId: null
        })
      }
    }
    catch(error){
      console.log(error)
      this.setState({
        error: true,
        uploadedFileCloudinaryUrl:'',
        originalFileNameCloudinaryUrl:'',
        originalFileHeightCloudinaryUrl: 0,
        originalFileWidthCloudinaryUrl: 0,
        originalFileFormatCloudinaryUrl: '',
        createAtCloudinaryUrl: '',
        postId: null
      })
    }
    
    // this.props.history.push(`/`)
  }

  createMedia = async () => {
    const postedById = localStorage.getItem(GC_USER_ID)

    if (!postedById) {
      console.error('No user logged in')
      return
    }
    const media  = this.state.uploadedFileCloudinaryUrl
    const postId = parseInt(this.state.postId,10)

    try{
      const result = await this.props.createPostMediaMutation({
        variables: {
          media,
          postId
        }
      })
      
      console.log(result)
      
      this.setState({
        success: true,
        uploadedFileCloudinaryUrl:'',
        originalFileNameCloudinaryUrl:'',
        originalFileHeightCloudinaryUrl: 0,
        originalFileWidthCloudinaryUrl: 0,
        originalFileFormatCloudinaryUrl: '',
        createAtCloudinaryUrl: '',
        postId: null
      })
    }
    catch(error){
      console.log(error)
      this.setState({
        error: true,
        uploadedFileCloudinaryUrl:'',
        originalFileNameCloudinaryUrl:'',
        originalFileHeightCloudinaryUrl: 0,
        originalFileWidthCloudinaryUrl: 0,
        originalFileFormatCloudinaryUrl: '',
        createAtCloudinaryUrl: '',
        postId: null
      })
    }
    
    // this.props.history.push(`/`)
  }


  render() {

    return (
      <Modal
          trigger={<Icon className="teal" name="add circle" size="big" style={{zoom: "80%",cursor: 'pointer', marginTop:"20px", marginRight: "20px"}} />}
          open={this.state.modalOpen}
          onClose={this.handleClose}
          basic
          size='small' 
          closeIcon
          >
          <Header style={{marginLeft:"120px"}} icon='comments' content={T.translate("social.addPost")} />
          <Modal.Content>
       
            {this.state.success ? <Message success header='Post added successfully'
                     content='You may now exit'
                     style={{marginTop:"1px",width: "70%", marginLeft:"150px"}}
            />: null }

            {this.state.error ? <Message negative header='Post was not created'
                     content='Something went wrong!! Please Try Again!!!'
                     style={{marginTop:"1px",width: "70%", marginLeft:"150px"}}
            />: null }
            
            <Form size="small" inverted>
              <Form.Group>
                  <Grid style={{marginLeft:"120px"}}>
                      <Grid.Row>
                          <Grid.Column>
                              <Form.Input required label={T.translate('social.title')} icon="comments"
                                  value={this.state.title}
                                  onChange={(e) => this.setState({ title: e.target.value })}
                                  placeholder='Insert your post title here...'
                                  style={{width: "95%"}}/>
                          </Grid.Column>
                      </Grid.Row>
                      <Grid.Row>
                          <Grid.Column>
                              <Form.TextArea label={T.translate('social.body')} placeholder='Share with our community something new...' 
                                             onChange={(e) => this.setState({ body: e.target.value })}
                                             style={{width: "95%"}}/>
                          </Grid.Column>
                      </Grid.Row>
                      <Grid.Row columns={1}>
                          <Grid.Column>
                            <div style={{marginLeft:"180px"}}>
                              <Dropzone
                                width="10px" 
                                onDrop={this.handleDrop} 
                                multiple 
                                accept="image/*" >
                              <p style={{color: "grey", textAlign:"center", marginTop:"80px"}}>Drop your files or click here to upload</p>
                              {this.state.uploadedFileCloudinaryUrl === '' ? null :
                                  <img alt={this.state.originalFileNameCloudinaryUrl} style={{width:"100px", height:"100px", marginTop: "-80px", marginLeft: "50px", backgroundColor:"white"}} src={this.state.uploadedFileCloudinaryUrl} />
                              }
                              </Dropzone>
                            </div>
                          </Grid.Column>
                      </Grid.Row>
                      <Grid.Row columns={1}>
                          <Grid.Column >
                              <Form.Field>
                                <Button color='green' inverted  onClick={() => this.createPost()}>
                                  <Icon name='checkmark' /> {T.translate('social.addPost')}
                                </Button>
                              </Form.Field>
                          </Grid.Column>
                      </Grid.Row>
                  </Grid>
              </Form.Group>
            </Form>    
          </Modal.Content>
        <Modal.Actions />
      </Modal>
    )
  }

}

CreatePost.propTypes = {
  createPostMutation: PropTypes.func.isRequired,
  createPostMediaMutation: PropTypes.func.isRequired
  // ,
  // history: PropTypes.shape({
  //   push: PropTypes.func.isRequired
  // }).isRequired,
};

export default compose(
  graphql(CREATE_POST_MUTATION, { name: 'createPostMutation' }),
  graphql(CREATE_POST_MEDIA_MUTATION, { name: 'createPostMediaMutation' })
)(CreatePost)