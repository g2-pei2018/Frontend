import React, { Component } from 'react'
import { Icon } from 'semantic-ui-react'
import { GC_USER_ID } from '../Authentication/constants'

const userId = localStorage.getItem(GC_USER_ID);

class Downvote extends Component{

    constructor(props) {
        super(props);
        this.state = {downvote: false};
    
        this.handleDownvote = this.handleDownvote.bind(this);
    }

    state = {
        downvote: false
    }

    handleDownvote(event) {
        console.log(`User ${userId} downvoted post`);
        event.preventDefault();
        this.setState({
            downvote: true
        });
    }

    render() {
        
        return (
           <Icon name='arrow down' style={{cursor: "pointer"}} onClick={this.handleDownvote}/>
        );
    }
 
}

export default Downvote