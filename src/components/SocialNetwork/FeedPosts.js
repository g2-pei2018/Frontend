import React, { Component } from 'react'
// import { Link } from 'react-router-dom'
import {PropTypes} from 'prop-types';
import { graphql } from 'react-apollo'
import { Comment, Accordion, Segment, Container, Rail, Sticky, Grid, Card, Icon, Popup } from 'semantic-ui-react'
import CreatePost from  './CreatePost';
import { timeDifferenceForDate } from '../Utils/utils';
import {ALL_POSTS_QUERY_2} from '../../graphql/queries/Post/AllPostsQuery'
import VotesPosts from './VotesPosts';
import VotesComments from './VotesComments';
import CommentPost from './CommentPost';

class FeedPosts extends Component {

  constructor(props){
    super(props);

    this.state = { 
          "posts": null,
          openedIdx: 1
        };
  }

  getPostsToRender() {
    return this.props.allPostsQuery.allPosts
  }

  handleAccordionClick = (e, titleProps) => {
    const { index } = titleProps;
    const { openedIdx } = this.state;
    const newIndex = openedIdx === index ? -1 : index;
  
    this.setState({
      openedIdx: newIndex
    });
  }

  renderPostComments = (result,i) =>{
      return <Accordion.Content active={this.state.openedIdx === 0}  key={i}>
                    <Comment.Group>
                        <Comment>
                          <Comment.Avatar src={result.user.avatar} />
                          <Comment.Content>
                            <Comment.Author as='a'>{result.user.username} </Comment.Author>
                            <Comment.Metadata>
                              <div>{timeDifferenceForDate(result.insertedAt)}</div>
                            </Comment.Metadata>
                            <Comment.Text>
                              {result.comment} 
                            </Comment.Text>
                            <Comment.Actions>
                                {result.medias.length>0 ? 
                                  result.medias.map((res,i2) => 
                                    <img key={`img${i2}`} alt={`img${i2}`} style={{width: "250px", height: "200px", paddingLeft: "10px", marginBottom: "10px"}} src={res.media} />
                                  )
                                : null
                                }
                                <br/>
                                <VotesComments commentId={parseInt(result.id,10)} upvotes={parseInt(result.upvotes,10)} downvotes={parseInt(result.downvotes,10)}/>
                            </Comment.Actions>
                          </Comment.Content>
                        </Comment>
                      </Comment.Group> 
                </Accordion.Content>
  }

  renderPosts = (item, index) =>{
    const postCommentsList = item.comments.map((result,i) => { 
      return this.renderPostComments(result,i);
    })

    return (<Card style={{ width: "400%" }} key={index}>
              <Card.Content>
                <Popup
                  trigger={<img className="ui avatar image" alt="avatar" src={item.user.avatar}/>}
                  header={item.user.username}
                  content={`${item.user.username} has been member since ${new Date(item.user.insertedAt).getFullYear()}`}
                />
                  <a>{item.user.username}</a>
                  <Card.Meta className="right floated">
                    {timeDifferenceForDate(item.insertedAt)}
                  </Card.Meta>
              </Card.Content>
              <Card.Content>
                <Card.Header>
                {item.title}
                </Card.Header>
                <Card.Description>
                  <p style={{marginBottom: "10px"}}>{item.body}</p>
                  {item.medias.length>0 ? 
                      item.medias.map((result,i) => 
                        <img key={`img${i}`} alt={`img${i}`} style={{width: "250px", height: "200px", paddingLeft: "10px"}} src={result.media} />
                      )
                    : null
                  }
                </Card.Description>
              </Card.Content>
              <Card.Content>
                <VotesPosts postId={parseInt(item.id,10)} upvotes={parseInt(item.upvotes,10)} downvotes={parseInt(item.downvotes,10)}/>
                <Accordion style={{marginTop:"-5px"}}>
                  <Accordion.Title active={this.state.openedIdx === 0 } index={0} onClick={this.handleAccordionClick}>
                    <Icon name='dropdown' />
                    {`${item.comments.length} comments`}
                  </Accordion.Title>
                  {postCommentsList}
                </Accordion>
              </Card.Content>
              <Card.Content extra>
                <CommentPost postId={item.id}/>
              </Card.Content>
            </Card>);
  };


  render() {
    const { contextRef } = this.state
    

    if (this.props.allPostsQuery && this.props.allPostsQuery.loading) {
      return <Grid.Column width={10}>
                <Segment textAlign="left">
                    Loading
                </Segment>
              </Grid.Column> 
    }
    if (this.props.allPostsQuery && this.props.allPostsQuery.error) {
      return <Grid.Column width={10}>
                <Segment textAlign="left">
                    Error
                </Segment>
              </Grid.Column> 
    }

    const postsToRender = this.getPostsToRender();

    const feedPostsList = postsToRender.map((result, index) =>{
      return this.renderPosts(result, index);
    })

    return <Grid.Column width={10} style={{ maxHeight: "650px", overflow: "auto" }}>
            <Segment textAlign="left">
              <Icon name='search' style={{cursor: "pointer"}} onClick={()=>{this.props.history.push(`/search`)}} size='large' />
              { feedPostsList }
              <Container textAlign="right">
                <Rail close internal position="right" style={{width: "100px"}}>
                  <Sticky context={contextRef} >
                      <CreatePost history={this.props.history} />
                  </Sticky>
                </Rail>
              </Container>
            </Segment>
          </Grid.Column>         
  }
}

FeedPosts.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired,
  allPostsQuery: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    error: PropTypes.object,
    allPosts: PropTypes.array
  }).isRequired
};

export const ALL_POSTS_QUERY = ALL_POSTS_QUERY_2

export default graphql(ALL_POSTS_QUERY, { name: 'allPostsQuery' })(FeedPosts)
