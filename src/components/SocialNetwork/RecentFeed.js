import React, { Component } from 'react'
import { graphql } from 'react-apollo'
import PropTypes from 'prop-types'
import { Feed, Grid, Card } from 'semantic-ui-react'
import T from  'i18n-react';
import { timeDifferenceForDate } from '../Utils/utils';
import {RECENT_POSTS_QUERY_2} from '../../graphql/queries/Post/RecentPostsQuery'

class RecentFeed extends Component {

    constructor(props){
      super(props);

      this.state = { 
            "feeds": null,
            addPostOpen: false,
            addPostColor: "blue",
            confirmOpen: false};
    }

    getPostsToRender() {
      return this.props.recentPostsQuery.recentPosts
    }

  renderFeed = (item, index) =>{
    return (
      <Feed.Event key={index}>
      {/* Se quisermos guardar url completo na DB */}
        <Feed.Label image={item.user.avatar} />
       {/* Se quisermos guardar apenas parte do url na DB
         *
         *  <Feed.Label image={`http://res.cloudinary.com/wattcharger/image/upload/${item.user.avatar}`} /> 
         * */}
        <Feed.Content>
          <Feed.Date content={`${timeDifferenceForDate(item.insertedAt)}`} />
          <Feed.Summary>
          <a>{item.user.username}</a>&nbsp;posted&nbsp;<i style={{color: "black"}}>&ldquo;{item.title}&rdquo;</i>.
          </Feed.Summary>
        </Feed.Content>
      </Feed.Event>);
  };

  // HTML Entities - https://www.freeformatter.com/html-entities.html

  render() {
    if (this.props.recentPostsQuery && this.props.recentPostsQuery.loading) {
      return <Grid.Column width={4}>
                <Card>
                <Card.Content>
                    <Card.Header>
                    {T.translate('social.recentActivity')}
                    </Card.Header>
                </Card.Content>
                <Card.Content style={{ maxHeight: "600px", overflow: "auto" }}>
                    Loading
                </Card.Content>
                </Card>
              </Grid.Column> 
    }
    if (this.props.recentPostsQuery && this.props.recentPostsQuery.error) {
      return <Grid.Column width={4}>
                <Card>
                <Card.Content>
                    <Card.Header>
                    {T.translate('social.recentActivity')}
                    </Card.Header>
                </Card.Content>
                <Card.Content style={{ maxHeight: "600px", overflow: "auto" }}>
                    Error
                </Card.Content>
                </Card>
              </Grid.Column> 
    }

    const postsToRender = this.getPostsToRender();
    const feedsList = postsToRender.map((result, index) =>{
      return this.renderFeed(result, index);
    })

    return <Grid.Column width={4}>
                <Card>
                <Card.Content>
                    <Card.Header>
                    {T.translate('social.recentActivity')}
                    </Card.Header>
                </Card.Content>
                <Card.Content style={{ maxHeight: "600px", overflow: "auto" }}>
                    <Feed>
                    { feedsList }
                    </Feed> 
                </Card.Content>
                </Card>
            </Grid.Column> 
  }
}

RecentFeed.propTypes = {
  recentPostsQuery: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
    error: PropTypes.object,
    recentPosts: PropTypes.array
  }).isRequired
};

export const RECENT_POSTS_QUERY = RECENT_POSTS_QUERY_2

export default graphql(RECENT_POSTS_QUERY, { name: 'recentPostsQuery' })(RecentFeed)