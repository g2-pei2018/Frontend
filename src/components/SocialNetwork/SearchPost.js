import React, { Component } from 'react'
import {PropTypes} from 'prop-types';
import { graphql, compose } from 'react-apollo'
import { Grid, Message, Form, Input } from 'semantic-ui-react'
import { GC_USER_ID } from '../Authentication/constants'
import ALL_POSTS_SEARCH_MUTATION_2 from '../../graphql/mutations/Search/AllPostsSearchMutation'
import ALL_COMMENTS_SEARCH_MUTATION_2 from '../../graphql/mutations/Search/AllCommentsSearchMutation'

const userId = localStorage.getItem(GC_USER_ID);

class SearchPost extends Component{

    constructor(props) {
        super(props);
        this.state = {search: '', success: true, matches: false, postResult: '', commentResult: ''};
    
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    state = {
        search: '',
        success: true,
        matchesP: false,
        matchesC: false,
        postResult: '',
        commentResult: ''
    }

    search = (wildcardSearch) => {
        const wildcard = wildcardSearch;
        // const wildcard = "%l%";
        this.props.AllPostsSearchMutation({
            variables: {
                wildcard
            }
        }).then( res => {
            // console.log(res)
            res.data.searchPosts.length===0 ? 
                this.setState({
                    success: false,
                    matchesP: false
                }) :
                this.setState({
                    success: true,
                    matchesP: true,
                    postResult: res.data.searchPosts,
                })
        }
        ).catch(err => console.log(err));


        this.props.AllCommentsSearchMutation({
            variables: {
                wildcard
            }
        }).then( res => {
            // console.log(res)
            res.data.searchComments.length===0 ? 
                this.setState({
                    success: false,
                    matchesC: false
                }) :
                this.setState({
                    success: true,
                    matchesC: true,
                    commentResult: res.data.searchComments,
                })
        }
        ).catch(err => console.log(err));
    }

    handleChange(event) {
        this.setState({search: event.target.value});
    }

    handleSubmit(event) {
        console.log(`User ${userId} searched: ${this.state.search}`);
        event.preventDefault();
        this.setState({
            search: ''
        });
        const wildcardSearch=`%${this.state.search}%`;
        // console.log(wildcardSearch);
        this.search(wildcardSearch);
    }

    render() {

        if (this.props.AllPostsSearchMutation && this.props.AllPostsSearchMutation.loading) {
            console.log("Loading posts...");
        }
        if (this.props.AllPostsSearchMutation && this.props.AllPostsSearchMutation.error) {
            console.log("Error posts...");
        }
        if (this.props.AllCommentsSearchMutation && this.props.AllCommentsSearchMutation.loading) {
            console.log("Loading comments...");
        }
        if (this.props.AllCommentsSearchMutation && this.props.AllCommentsSearchMutation.error) {
            console.log("Error comments...");
        }
            
        return ( <div>
                    <Grid centered columns='1'>
                        <Grid.Column width='12'>
                        <Form onSubmit={this.handleSubmit}>
                            <Input style={{width: "100%"}} action={{ icon: 'search' }} placeholder='Search...' value={this.state.search} onChange={this.handleChange}/>
                        </Form>
                        </Grid.Column>
                    </Grid>
                    <br/>
                    {!this.state.success ? <Message negative header='Found No Matches'
                            content='Search again...'
                    /> : null }

                    {this.state.matchesP ?  <div>
                                                <Message positive>
                                                    <Message.Header>{`${this.state.postResult.length} Posts Matches Found`}</Message.Header>
                                                    <br/>
                                                    <Message.List items={this.state.postResult.map((result,i) =>{
                                                        // return "Title: "+result.title+" Body: "+result.body
                                                        return <div key={i}>
                                                                    <ol>
                                                                        <li>
                                                                            <b>{`${result.title} By ${result.user.username}`}</b>
                                                                            <ul>
                                                                                <br/>
                                                                                <li>
                                                                                    {result.body}
                                                                                </li>
                                                                            </ul>
                                                                        </li>
                                                                    </ol>
                                                                    <br/>
                                                            </div>
                                                        })} />
                                                </Message>
                                                <br/>
                                            </div>
                                        : null 
                    }
                    <br/>
                    {this.state.matchesC ?  <div>
                                                <Message positive>
                                                    <Message.Header>{`${this.state.commentResult.length} Comments Matches Found`}</Message.Header>
                                                    <br/>
                                                    <Message.List items={this.state.commentResult.map((result,i) =>{
                                                        return <div key={i}>
                                                                    <ol>
                                                                        <li>
                                                                            <b>{`${result.comment} By ${result.user.username}`}</b>
                                                                        </li>
                                                                    </ol>
                                                                    <br/>
                                                            </div>
                                                        })} />
                                                </Message>
                                                <br/>
                                            </div>
                                        : null 
                    }
                </div>
        );
    }
 
}

SearchPost.propTypes = {
    AllPostsSearchMutation: PropTypes.func.isRequired,
    AllCommentsSearchMutation: PropTypes.func.isRequired
  };

export const ALL_POSTS_SEARCH_MUTATION = ALL_POSTS_SEARCH_MUTATION_2
export const ALL_COMMENTS_SEARCH_MUTATION = ALL_COMMENTS_SEARCH_MUTATION_2

export default compose(
    graphql(ALL_POSTS_SEARCH_MUTATION, { name: 'AllPostsSearchMutation' }),
    graphql(ALL_COMMENTS_SEARCH_MUTATION, { name: 'AllCommentsSearchMutation' })
)(SearchPost)