import React, { Component } from 'react'
import { Grid} from 'semantic-ui-react'
import {PropTypes} from 'prop-types';
import FeedPosts from './FeedPosts';
import RecentFeed from './RecentFeed';


class SocialNetwork extends Component {
  satte={}
  render() {
    return <div style={{marginBottom:"50px"}}>
            <Grid centered stackable>
                <Grid.Row>
                      <RecentFeed history={this.props.history} />
                      <FeedPosts history={this.props.history}/>
                </Grid.Row>
            </Grid>
          </div>   
 
  }
}

SocialNetwork.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
}

export default SocialNetwork;

