import React, { Component } from 'react'
import { Icon } from 'semantic-ui-react'
import { graphql, compose } from 'react-apollo'
import {PropTypes} from 'prop-types';
import { GC_USER_ID } from '../Authentication/constants'
import ADD_UPVOTE_COMMENT from '../../graphql/mutations/Post/AddUpvoteCommentMutation'
import REMOVE_UPVOTE_COMMENT from '../../graphql/mutations/Post/RemoveUpvoteCommentMutation'
import ADD_DOWNVOTE_COMMENT from '../../graphql/mutations/Post/AddDownvoteCommentMutation'
import REMOVE_DOWNVOTE_COMMENT from '../../graphql/mutations/Post/RemoveDownvoteCommentMutation'
import USER_HAS_VOTED_COMMENT from '../../graphql/mutations/Post/UserHasVotedCommentMutation'


const userId = localStorage.getItem(GC_USER_ID);

class VotesComments extends Component{
    constructor(props) {
        super(props);
        this.state = {upvote: false, downvote: false, commentId: this.props.commentId, upvotes: this.props.upvotes, downvotes: this.props.downvotes};
    
        this.handleUpvote = this.handleUpvote.bind(this);
        this.handleDownvote = this.handleDownvote.bind(this);


        const id  = this.state.commentId

        console.log(id)
        this.props.UserHasVotedCommentMutation({
            variables: {
              id
            }
        }).then(result => {
            // console.log(result.data.userHasVotedComment.length)
            if(result.data.userHasVotedComment.length>0){ 
                console.log(result.data.userHasVotedComment[0])
                if(result.data.userHasVotedComment[0]==="upvote"){
                    this.setState({upvote: true, downvote: false})
                }else if(result.data.userHasVotedComment[0]==="downvote"){
                    this.setState({upvote: false, downvote: true})
                }
            }
        }).catch(err => {
            console.log(err)
        })

    }

    state = {
        upvote: false,
        downvote: false,
        commentId: this.props.commentId,
        upvotes: this.props.upvotes,
        downvotes: this.props.downvotes
    }

    handleDownvote(event) {
        // console.log(`User ${userId} downvoted post`);
        // console.log(this.state.commentId)
        const id  = this.state.commentId

        // console.log(this.state.downvote)

        if(this.state.downvote){
            this.props.RemoveDownvoteCommentMutation({
                variables: {
                  id
                }
            }).then(result => {
                console.log(result)
                this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes-1})
            }).catch(err => {
                console.log(err)
            })
        } else 
            if(this.state.upvote){
                this.props.RemoveUpvoteCommentMutation({
                    variables: {
                      id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes-1})
                    this.props.AddDownvoteCommentMutation({
                        variables: {
                        id
                        }
                    }).then(result1 => {
                        console.log(result1)
                        this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes+1})
                    }).catch(err => {
                        console.log(err)
                    })

                }).catch(err => {
                    console.log(err)
                })
            }
            else{
                this.props.AddDownvoteCommentMutation({
                    variables: {
                    id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes+1})
                }).catch(err => {
                    console.log(err)
                })
            }

        event.preventDefault();
    }

    handleUpvote(event) {
        // console.log(`User ${userId} Upvoted post`);
        // console.log(this.state.commentId)
        const id  = this.state.commentId

        // console.log(id)
      
        // console.log(this.state.upvote)
        
        if(this.state.upvote){
            this.props.RemoveUpvoteCommentMutation({
                variables: {
                  id
                }
            }).then(result => {
                console.log(result)
                this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes-1})
            }).catch(err => {
                console.log(err)
            })
        } else 
            if(this.state.downvote){
                this.props.RemoveDownvoteCommentMutation({
                    variables: {
                        id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes-1})
                    this.props.AddUpvoteCommentMutation({
                        variables: {
                        id
                        }
                    }).then(result2 => {
                        console.log(result2)
                        this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes+1})
                    }).catch(err => {
                        console.log(err)
                    })
                }).catch(err => {
                    console.log(err)
                })
            }
            else {
                this.props.AddUpvoteCommentMutation({
                    variables: {
                    id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes+1})
                }).catch(err => {
                    console.log(err)
                })
            }

        event.preventDefault();
    }

    render() {
        
        return (
            <span className="right floated">
                {!this.state.upvote ? <Icon name='arrow up' style={{cursor: "pointer"}} onClick={this.handleUpvote}/> 
                : <Icon name='arrow up' style={{cursor: "pointer", color: "blue"}} onClick={this.handleUpvote}/> }
                {!this.state.upvote ? <b>{`${this.state.upvotes} upvotes`}</b> : <b style={{color: "blue"}}>{`${this.state.upvotes} upvotes`}</b>}
                &nbsp;&nbsp;
                {!this.state.downvote ? <Icon name='arrow down' style={{cursor: "pointer"}} onClick={this.handleDownvote}/> 
                : <Icon name='arrow down' style={{cursor: "pointer", color: "blue"}} onClick={this.handleDownvote}/> }
                {!this.state.downvote ? <b>{`${this.state.downvotes} downvotes`}</b> : <b style={{color: "blue"}}>{`${this.state.downvotes} downvotes`}</b>}
            </span> 
        );
    }
 
}

VotesComments.propTypes = {
    AddUpvoteCommentMutation: PropTypes.func.isRequired,
    RemoveUpvoteCommentMutation: PropTypes.func.isRequired,
    AddDownvoteCommentMutation: PropTypes.func.isRequired,
    RemoveDownvoteCommentMutation: PropTypes.func.isRequired,
    UserHasVotedCommentMutation: PropTypes.func.isRequired
};

export default compose(
    graphql(USER_HAS_VOTED_COMMENT, { name: 'UserHasVotedCommentMutation' }),
    graphql(ADD_UPVOTE_COMMENT, { name: 'AddUpvoteCommentMutation' }),
    graphql(REMOVE_UPVOTE_COMMENT, { name: 'RemoveUpvoteCommentMutation' }),
    graphql(ADD_DOWNVOTE_COMMENT, { name: 'AddDownvoteCommentMutation' }),
    graphql(REMOVE_DOWNVOTE_COMMENT, { name: 'RemoveDownvoteCommentMutation' })
)(VotesComments)
