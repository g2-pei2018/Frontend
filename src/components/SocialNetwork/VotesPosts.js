import React, { Component } from 'react'
import { Icon } from 'semantic-ui-react'
import { graphql, compose } from 'react-apollo'
import {PropTypes} from 'prop-types';
import { GC_USER_ID } from '../Authentication/constants'
import ADD_UPVOTE_POST from '../../graphql/mutations/Post/AddUpvotePostMutation'
import REMOVE_UPVOTE_POST from '../../graphql/mutations/Post/RemoveUpvotePostMutation'
import ADD_DOWNVOTE_POST from '../../graphql/mutations/Post/AddDownvotePostMutation'
import REMOVE_DOWNVOTE_POST from '../../graphql/mutations/Post/RemoveDownvotePostMutation'
import USER_HAS_VOTED_POST from '../../graphql/mutations/Post/UserHasVotedPostMutation'


const userId = localStorage.getItem(GC_USER_ID);

class VotesPosts extends Component{

    constructor(props) {
        super(props);
        this.state = {upvote: false, downvote: false, postId: this.props.postId, upvotes: this.props.upvotes, downvotes: this.props.downvotes};
    
        this.handleUpvote = this.handleUpvote.bind(this);
        this.handleDownvote = this.handleDownvote.bind(this);


        const id  = this.state.postId

        console.log(id)
        this.props.UserHasVotedPostMutation({
            variables: {
              id
            }
        }).then(result => {
            // console.log(result.data.userHasVotedPost.length)
            if(result.data.userHasVotedPost.length>0){ 
                console.log(result.data.userHasVotedPost[0])
                if(result.data.userHasVotedPost[0]==="upvote"){
                    this.setState({upvote: true, downvote: false})
                }else if(result.data.userHasVotedPost[0]==="downvote"){
                    this.setState({upvote: false, downvote: true})
                }
            }
        }).catch(err => {
            console.log(err)
        })

    }

    state = {
        upvote: false,
        downvote: false,
        postId: this.props.postId,
        upvotes: this.props.upvotes,
        downvotes: this.props.downvotes
    }

    handleDownvote(event) {
        // console.log(`User ${userId} downvoted post`);
        // console.log(this.state.postId)
        const id  = this.state.postId

        // console.log(this.state.downvote)

        if(this.state.downvote){
            this.props.RemoveDownvotePostMutation({
                variables: {
                  id
                }
            }).then(result => {
                console.log(result)
                this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes-1})
            }).catch(err => {
                console.log(err)
            })
        } else 
            if(this.state.upvote){
                this.props.RemoveUpvotePostMutation({
                    variables: {
                      id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes-1})
                    this.props.AddDownvotePostMutation({
                        variables: {
                        id
                        }
                    }).then(result1 => {
                        console.log(result1)
                        this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes+1})
                    }).catch(err => {
                        console.log(err)
                    })

                }).catch(err => {
                    console.log(err)
                })
            }
            else{
                this.props.AddDownvotePostMutation({
                    variables: {
                    id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes+1})
                }).catch(err => {
                    console.log(err)
                })
            }

        event.preventDefault();
    }

    handleUpvote(event) {
        // console.log(`User ${userId} Upvoted post`);
        // console.log(this.state.postId)
        const id  = this.state.postId

        // console.log(id)
      
        // console.log(this.state.upvote)
        
        if(this.state.upvote){
            this.props.RemoveUpvotePostMutation({
                variables: {
                  id
                }
            }).then(result => {
                console.log(result)
                this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes-1})
            }).catch(err => {
                console.log(err)
            })
        } else 
            if(this.state.downvote){
                this.props.RemoveDownvotePostMutation({
                    variables: {
                        id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({downvote: !this.state.downvote, downvotes: this.state.downvotes-1})
                    this.props.AddUpvotePostMutation({
                        variables: {
                        id
                        }
                    }).then(result2 => {
                        console.log(result2)
                        this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes+1})
                    }).catch(err => {
                        console.log(err)
                    })
                }).catch(err => {
                    console.log(err)
                })
            }
            else {
                this.props.AddUpvotePostMutation({
                    variables: {
                    id
                    }
                }).then(result => {
                    console.log(result)
                    this.setState({upvote: !this.state.upvote, upvotes: this.state.upvotes+1})
                }).catch(err => {
                    console.log(err)
                })
            }

        event.preventDefault();
    }

    render() {
        
        return (
            <span className="right floated">
                {!this.state.upvote ? <Icon name='arrow up' style={{cursor: "pointer"}} onClick={this.handleUpvote}/> 
                : <Icon name='arrow up' style={{cursor: "pointer", color: "blue"}} onClick={this.handleUpvote}/> }
                {!this.state.upvote ? <b>{`${this.state.upvotes} upvotes`}</b> : <b style={{color: "blue"}}>{`${this.state.upvotes} upvotes`}</b>}
                &nbsp;&nbsp;
                {!this.state.downvote ? <Icon name='arrow down' style={{cursor: "pointer"}} onClick={this.handleDownvote}/> 
                : <Icon name='arrow down' style={{cursor: "pointer", color: "blue"}} onClick={this.handleDownvote}/> }
                {!this.state.downvote ? <b>{`${this.state.downvotes} downvotes`}</b> : <b style={{color: "blue"}}>{`${this.state.downvotes} downvotes`}</b>}
            </span> 
        );
    }
 
}

VotesPosts.propTypes = {
    AddUpvotePostMutation: PropTypes.func.isRequired,
    RemoveUpvotePostMutation: PropTypes.func.isRequired,
    AddDownvotePostMutation: PropTypes.func.isRequired,
    RemoveDownvotePostMutation: PropTypes.func.isRequired,
    UserHasVotedPostMutation: PropTypes.func.isRequired
};

export default compose(
    graphql(USER_HAS_VOTED_POST, { name: 'UserHasVotedPostMutation' }),
    graphql(ADD_UPVOTE_POST, { name: 'AddUpvotePostMutation' }),
    graphql(REMOVE_UPVOTE_POST, { name: 'RemoveUpvotePostMutation' }),
    graphql(ADD_DOWNVOTE_POST, { name: 'AddDownvotePostMutation' }),
    graphql(REMOVE_DOWNVOTE_POST, { name: 'RemoveDownvotePostMutation' })
)(VotesPosts)
