import React, { Component } from 'react';
import {Form, Grid, /* TextArea, */ Button, Icon, Dropdown, Image} from 'semantic-ui-react';
import T from  'i18n-react';
import { graphql, compose } from 'react-apollo'
/* import PropTypes from 'prop-types' */
import Card from 'semantic-ui-react/dist/commonjs/views/Card/Card';
// import { ALL_POSTS_QUERY } from './PostList';
// import { GC_USER_ID, POSTS_PER_PAGE } from '../Authentication/constants'
import { GC_USER_ID } from '../Authentication/constants'
import { GET_EVS_QUERY } from '../../graphql/queries/EV/GetEVsQuery';
import RemoveEvMutation from '../../graphql/mutations/User/RemoveEvMutation';
import Container from 'semantic-ui-react/dist/commonjs/elements/Container/Container';
import GridColumn from 'semantic-ui-react/dist/commonjs/collections/Grid/GridColumn';
import AddEvMutation from '../../graphql/mutations/User/AddEvMutation';

const centerText = {
  textAlign: 'center'
};

class AddEvToUser extends Component {
  state = {
    ev:undefined,
    fullEvList:undefined,
    year:undefined,
    brand:undefined,
    model:undefined,
    vn:undefined,
    disabled:true
  }


  fetchColor = (ev) => {
      if (!this.state.ev) return 'blue'
      if((ev.brand === this.state.ev.brand) && (ev.year === this.state.ev.year) && (ev.model === this.state.ev.model)) return 'green'
      return 'blue'
  }

  carChoice = (ev) => {
      if (!this.state.ev) return 'Chose Car'
      if((ev.brand === this.state.ev.brand) && (ev.year === this.state.ev.year) && (ev.model === this.state.ev.model)) return 'Chosen Car'
      return 'Chose Car'
  }

  choiceClickHandler = (ev) => {
      this.setState({ev:ev})
      this.setState({model:ev.model})
  } 


    createMap = (ev) => {
        return( 
            <Card centered color={this.fetchColor(ev)}>
            <Image size="medium" src={ev.avatar} />
            <Card.Content>
                <Card.Header>
                {ev.brand} {ev.model}
                </Card.Header>
                <Card.Meta>
                {ev.chargingStandard}
                </Card.Meta>
                <Card.Description>
                <b>Year:</b> {ev.year}
                </Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Grid centered>
                <Grid.Row columns='1' >
                <Grid.Column textAlign= 'center'>
                <Button basic color={this.fetchColor(ev)} onClick={(e) => this.choiceClickHandler(ev)} >{this.carChoice(ev)}  </Button>
                </Grid.Column>
                </Grid.Row >
                </Grid>
            </Card.Content>
            </Card>
        )
    }

    checkConstraint = (ev) => {
      let retval = false
        if(this.state.brand){
            if(ev.brand == this.state.brand){retval = true}
            else{ return false}
        }
        if(this.state.model){
          if(ev.model == this.state.model){retval = true}
          else{return false}
        }
        if(this.state.year){
          if(ev.year == this.state.year){retval = true}
          else{return false}
        }
        return retval
    }

    createCars = () =>{
       if(this.props.getEvs.listCars.length === 0) return(
        <div> <Container inline='centered' > <h1 style = {centerText}>No cars in the database </h1> </Container> </div>
      )
      if(this.state.year === undefined && this.state.brand === undefined && this.state.model === undefined){ 
        return <div> <Container inline='centered' > <h1 style = {centerText}>No selection has been made </h1> </Container> </div>
      }
      return this.props.getEvs.listCars.filter(this.checkConstraint).map((ev) => this.createMap(ev))
    }

  addEV = async () => {
    const postedById = localStorage.getItem(GC_USER_ID)
    if (!this.state.vn || !this.state.ev) return
    const id = this.state.ev.id
    const savings = 0.0
    const consumption = 0.0
    const name = this.state.vn
    try{
      const result = await this.props.addEv({
        variables: {
          id,
          savings,
          consumption,
          name
        }
      })
      console.log(result)
    }
    catch(error){
      console.log(error)
    }
    this.props.toggleCompleted()
    this.props.toggleShow()
  }

  changeHandler = (tochange,value) =>{
    if (tochange === 'Model'){
      this.setState({model:value})
    }
    if (tochange === 'Year'){
      this.setState({year:value})
    }
    if (tochange === 'Brand'){
      this.setState({brand:value})
      this.setState({year:undefined})
      this.setState({model:undefined})
      this.setState({disabled:false})

    } 
  }

  createoption = (ev) => {
    return {key:ev, text: ev, value: ev}
  }

  filtercar = (ev) => {
    return ev.brand === this.state.brand
  }

  removerepeatmodel = () => {
    const array = this.props.getEvs.listCars.filter(this.filtercar).map((ev) => ev.model)
    const nodups = Array.from(new Set(array));
    const retval = nodups.map((ev) => this.createoption(ev))
    return retval

  }

  removerepeatbrand = () => {

    const array = this.props.getEvs.listCars.map((ev) => ev.brand)
    const nodups = Array.from(new Set(array));
    const retval = nodups.map((ev) => this.createoption(ev))
    return retval

  }

  removerepeatyear = () => {

    const array = this.props.getEvs.listCars.filter(this.filtercar).map((ev) => ev.year.toString())
    const nodups = Array.from(new Set(array));
    const retval = nodups.map((ev) => this.createoption(ev))
    return retval

  }

  createDropDownItems = () => {
    return this.removerepeatbrand().map((ev)=>{
        <Dropdown.Item text= {ev.brand} value={ev.brand} onClick = {(e) => this.changeHandler('brand',e.target.value)}/>
    })
  }

  render() {
    if (this.props.getEvs && this.props.getEvs.loading){return <div></div>}
    if (this.props.getEvs && this.props.getEvs.error){return <div></div>}
    return (
        <Form size="small" inverted>
        <Form.Group>
            <Grid centered>
                <Grid.Row >
                    <Grid.Column width= '12'>
                        <Form.Input 
                            required 
                            label={'Vehicle name description (Licence Plate/Custom Name)'} 
                            icon="car"
                            value={this.state.carname}
                            onChange={(e) => this.setState({ vn: e.target.value })}/>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns='3'>
                    <Grid.Column width= '4'>
                       <Dropdown placeholder='Brand' onChange={(ev,data) => this.changeHandler('Brand',data.value)} fluid search selection options= {this.removerepeatbrand()}  />
                    </Grid.Column>
                    <Grid.Column width= '4'>
                    <Dropdown placeholder='Model' disabled = {this.state.disabled}  onChange={(ev,data) => this.changeHandler('Model', data.value)} fluid search selection options= {this.removerepeatmodel()}/> 
                    </Grid.Column>
                    <Grid.Column width= '4'>
                       <Dropdown placeholder='Year' disabled = {this.state.disabled} onChange={(ev,data) => this.changeHandler('Year',data.value)} fluid search selection options= {this.removerepeatyear()}  />
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row >
                    <Grid.Column>
                      <Card.Group>
                       {this.createCars()}
                      </Card.Group>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2}>
                    <Grid.Column textAlign="center">
                        <Form.Field>
                          <Button color='green' inverted  onClick={() => this.addEV()}>
                            <Icon name='checkmark' /> {'Add Vehicle'}
                          </Button>
                        </Form.Field>
                        </Grid.Column>
                        <Grid.Column textAlign="center">
                        <Form.Field>
                          <Button onClick ={ (e) => this.props.toggleShow()} basic color='red' inverted>
                            <Icon name='remove'  /> {T.translate('social.cancel')}
                          </Button>
                        </Form.Field>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Form.Group>

        </Form>
    )
  }

}

export default compose( 
  graphql(GET_EVS_QUERY, {name:'getEvs'}),
  graphql(AddEvMutation, {name:'addEv'})
)(AddEvToUser);