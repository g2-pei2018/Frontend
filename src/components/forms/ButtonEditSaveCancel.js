import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Popup, Icon} from 'semantic-ui-react';


export default class ButtonEditSaveCancel extends Component {
  state = {
      showEdit: false,
      showSave: false,
      showCancel: false,
      btnCancel: null,
      btnSave: null,
      btnEdit: this.constButtonEdit
  }

  constButtonEdit = <Popup trigger={<Icon name='edit' key="editName"  color="grey" fitted onClick={(event) => {this.changeButtons(event, "edit")}} />}
      content="Editar" />;
  
  constButtonSave = <Popup trigger={<Icon name="check circle" key="confirmName" color="green"  fitted onClick={(event) => {this.changeButtons(event, "save")}} />}
      content="Guardar" />;
  
  constButtonCancel = <Popup trigger={<Icon name='remove circle' key="cancelName" color="red" style={{ marginLeft: "5px" }} fitted onClick={(event) => {this.changeButtons(event, "cancel")}} />}
      content="Cancelar" />;


  changeButtons = (event, button) => {
            switch(button){
                case "edit" : 
                        this.setState({btnEdit: null,
                            btnSave : this.constButtonSave,
                            btnCancel: this.constButtonCancel
                                });
                        this.props.changed("edit");
                break;
                case "save" :
                        this.setState({
                            btnEdit: this.constButtonEdit,
                            btnSave : null,
                            btnCancel: null
                        });
                        this.props.changed("save");
                break;
                case "cancel" :
                        this.setState({
                            btnEdit: this.constButtonEdit,
                            btnSave : null,
                            btnCancel: null
                        });
                        this.props.changed("cancel");
                break;
                default:
                        this.setState({
                            btnEdit: this.constButtonEdit,
                            btnSave : null,
                            btnCancel: null
                        });
                break;
            }      
        }
  
  static propTypes = {
        showEdit: PropTypes.bool,
        showSave: PropTypes.bool,
        showCancel: PropTypes.bool
  }

  constructor(props) {
    super(props)
  
    this.state = {
       showEdit : this.props.showEdit,
       showSave : this.props.showSave,
       showCancel : this.props.showCancel,
       btnCancel: null,
       btnSave: null,
       btnEdit: this.constButtonEdit
       
    }
  }

  render() {
    return (
      <span>
        {this.state.btnEdit} 
        {this.state.btnSave}
        {this.state.btnCancel}           
      </span>
    )
  }
}
