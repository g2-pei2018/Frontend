import React, { Component } from 'react';
import { Progress, Form, Grid, Button/* , Icon */ } from 'semantic-ui-react';
import T from 'i18n-react';
import Autocomplete from 'react-google-autocomplete';
import {PropTypes} from 'prop-types';
import { graphql } from 'react-apollo'
import {GET_EVS_QUERY} from '../../graphql/queries/EV/GetEVsQuery';

class HomeForm extends Component {
    state = { percent: 100 }

    getCarList() {
        const cars = this.props.listCarsQuery.listCars;
        let carMod ;
        if(cars !== undefined){
            carMod = [].concat(cars)
                    .sort((a,b) => {
                        const aa = a.brand.toUpperCase() + a.model.toUpperCase();
                        const bb = b.brand.toUpperCase() + b.model.toUpperCase();
                        if(aa < bb){return -1}
                        if(aa === bb){return 0}
                        if(aa > bb){return 1}
                        return 0;
                    })
                    .map((result, index) => {
                return {text: `${result.brand} > ${result.model}`, value: result.id, image:{
                avatar: true,
                src: result.avatar, 
                floated: "right"
            } }
          })
        }
        return carMod;
        }

    handleRouteTypeChange = (e, {value}) => {
        const routeType = value;
        this.setState({routeType})
    }

    gotoMap = () =>{
        if(this.state.fromPlace !== undefined && this.state.toPlace !== undefined){
            const from = this.state.fromPlace.formatted_address;
            const to = this.state.toPlace.formatted_address;
            const percentage = this.state.percent;
            this.props.history.push(`/map`,{state: {fromPlace: from, toPlace: to, percent: percentage, car: this.state.selectedCar}})
        }
    }    

    increment = () => this.setState({
      percent: this.state.percent >= 100 ? 100 : this.state.percent + 50,
    })

    decrement = () => this.setState({
      percent: this.state.percent <= 0 ? 0 : this.state.percent - 50,
    })

    render() {
        let evs = this.getCarList();
        /** selection requires options even if it is [] */
        if(evs === undefined){evs=[]}
        return (
            <Form size="small">
                <Form.Group>
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column>
                                <Form.Field required > 
                                    <label>{T.translate('home.selectOrigin')}</label>
                                    <Autocomplete ref={(c) => { this.refFrom = c; }}  style={{width: '100%'}} onPlaceSelected={(place) => { this.setState({fromPlace: place})}} types={['(regions)']} />
                                </Form.Field>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Form.Field required>
                                    <label>{T.translate('home.selectDestination')}</label>
                                    <Autocomplete onPlaceSelected={(place) => { this.setState({toPlace: place})}} types={['(regions)']} />
                                </Form.Field>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Form.Dropdown onChange={(event, data) => {
                                    this.setState({selectedCar: data.value})
                                }} label={T.translate('home.selectCar')} fluid selection options={evs} />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Form.Field>
                                    <Progress percent={this.state.percent} indicating size="small">
                                        <Button icon="plus" content={T.translate('home.power')} basic floated="left" size="mini" color="red" onClick={this.decrement} />
                                        <Button icon="plus" content={T.translate('home.eco')} basic floated="right" size="mini" color="green" onClick={this.increment} />
                                    </Progress>
                                </Form.Field>
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Form.Field />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column >
                                <Form.Button onClick={() => {
                                            this.gotoMap()
                                        }
                                    } size="small">{T.translate('home.calculateRoute')}</Form.Button>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form.Group>
            </Form>
        );
    }
}

HomeForm.propTypes = {
    history: PropTypes.shape({
      push: PropTypes.func.isRequired
    }).isRequired,
    listCarsQuery: PropTypes.shape({
        loading: PropTypes.bool.isRequired,
        error: PropTypes.object,
        listCars: PropTypes.array
    }).isRequired
  }

// export default HomeForm;
export const LIST_CARS_QUERY = GET_EVS_QUERY
export default graphql(LIST_CARS_QUERY, { name: 'listCarsQuery' })(HomeForm);