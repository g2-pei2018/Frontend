import React, { Component } from 'react';
import {Card, Button, Image, Grid, Loader, Container, Rail, Sticky, Modal,Header, Icon} from 'semantic-ui-react';
import {USER_EVS_QUERY} from '../../graphql/queries/User/GetUserEvs';
import RemoveEvMutation from '../../graphql/mutations/User/RemoveEvMutation'
import AddEvToUser from '../../components/forms/AddEvToUser'
import { graphql, compose } from 'react-apollo'
import T from 'i18n-react';

const centerText = {
  textAlign: 'center'
};

class MyCars extends Component {
  state = {
    modalOpen: false,
    completed: false
  }
  


  removeElectricVehicle = async(ev) => {
      const id = parseInt(ev.id)
    const answer = await this.props.deleteEv({
      variables: {
        id
      }
    })
    this.props.getUserEvs.refetch()
  }

  createMap = (ev) => {
   return( 
    <Card centered key={ev.name}>
      <Image size="medium" src={ev.ev.avatar} />
      <Card.Content>
        <Card.Header>
          {ev.ev.brand} {ev.ev.model}
        </Card.Header>
        <Card.Meta>
        <b>Name:</b> {ev.name}
        </Card.Meta>
        <Card.Meta>
          {ev.ev.chargingStandard}
        </Card.Meta>
        <Card.Description>
        <b>Year:</b> {ev.ev.year} | <b>Max Charge:</b> {ev.ev.maxCharge}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <div className='ui two buttons'>
          <Button basic color='green' onClick={(e) => console.log(ev)} >{T.translate('myAccount.changeInfo')}  </Button>
          <Button basic color='red'   onClick={(e) => this.removeElectricVehicle(ev)} >{T.translate('myAccount.remove')} </Button>
        </div>
      </Card.Content>
    </Card>
   )
  }

  createCars = () =>{
    if(this.props.getUserEvs.getEvsFromUser.length === 0) return(
    <div> <Container inline='centered' > <h1 style = {centerText}> You still haven't added an electric vehicle </h1> </Container> </div>
  )
    return this.props.getUserEvs.getEvsFromUser.map((ev) => this.createMap(ev))
  }

  loadingRender(){
    return <div>
              <Loader active inline='centered' />
           </div>
  }

  errorRender(){
    return <div >
      <Grid centered>
          <Card>
              <Card.Content>
                  <Card.Header>
                  Oops! An Error has ocurred
                  </Card.Header>
                  <Card.Description>
                      An error has ocurred while loading this page
                  </Card.Description>
              </Card.Content>
              <Card.Content extra>
                  <div className='ui one buttons'>
                  <Button basic color='green' href='/my_account'>Refresh Page</Button>
                  </div>
              </Card.Content>
          </Card>
          </Grid>
    </div>
  }

  toggleShow = () => {
    this.setState({modalOpen: !this.state.modalOpen})
  }

  toggleCompleted = () => {
    this.setState({completed: !this.state.completed})
    this.props.getUserEvs.refetch()
    this.setState({completed: false})
  }


  completeRender(){
    return (
      <div>
            <Card.Group itemsPerRow = '5' >
            {this.createCars()}
            </Card.Group> 
          <Container textAlign = 'right' style= {{width: '100%', height:'30px', textAlign: 'right'}}> <br/></Container>
          <Container textAlign = 'right' style= {{width: '100%', height:'30px', textAlign: 'right'}}>
              <Modal  trigger={ <div style= {{verticalAlign: 'bottom'}} > <b> Add a Vehicle </b> <Icon onClick = {(e) => this.toggleShow()} className="teal" name="add circle" size="big" style={{ float: 'bottom', zoom: "100%",cursor: 'pointer'}} /> </div> }
                  open={this.state.modalOpen}
                  //onClose={this.handleClose}
                  basic
                  size='large' >
                  <Header icon='comments' content="Add an electric vehicle to your account" />
                  <Modal.Content >
                  <AddEvToUser toggleShow = {this.toggleShow} toggleCompleted = {this.toggleCompleted}/>
                  </Modal.Content>
                  <Modal.Actions />
                </Modal>
              </Container>
      </div>
    )
  }

  render() {
    if (this.props.getUserEvs && this.props.getUserEvs.loading){return this.loadingRender()}
    if (this.props.getUserEvs && this.props.getUserEvs.error){return this.errorRender() }
    return this.completeRender()
  }
}

export default compose( 
  graphql(USER_EVS_QUERY, {name:'getUserEvs' }),
  graphql(RemoveEvMutation, {name:'deleteEv'})
)(MyCars);