import React, { Component } from 'react';
import {Form, Grid, Confirm, Button, Card} from 'semantic-ui-react';
import { graphql, compose } from 'react-apollo'
import T from 'i18n-react';
import PropTypes from 'prop-types';
import ButtonEditSaveCance from './ButtonEditSaveCancel';
import {USER_DETAILS_QUERY} from '../../graphql/queries/User/UserDetailsQuery';
import UpdateUserMutation, { UPDATE_USER_MUTATION } from '../../graphql/mutations/User/UpdateUserMutation'
import FileUpload from './../FileUpload/FileUpload.js';


class PersonalDataForm extends Component {

  state = {
      isNameDisabled: true,
      isUsernameDisabled: true,
      isEmailDisabled: true,
      isPasswordDisabled: true,
      passwordConfirm: false,
      openConfirmDialog: false,
      name: undefined, 
      relatedto: "nothing",
      confirmDialogMessage: "",
      password: "******"
  }

  messageConfirmSave = T.translate('myAccount.areYouSureSave');
  messageConfirmCancel = T.translate('myAccount.areYouSureCancel');

  changeNameMutation = async () => {
    const name = this.state.name

      await this.props.updateUserMutation({
        variables: {
          name
        }
      })

      this.props.getUserDetails.refetch()
  }

  changePasswordMutation = async () => {
    const password = this.state.password

    await this.props.updateUserMutation({
      variables: {
        password
      }
    })

    this.props.getUserDetails.refetch()
  }

  handleConfirm = (event,data) =>{
    this.setState({openConfirmDialog: false});
    if (this.state.relatedto === 'isNameDisabled'){
        this.changeNameMutation()
    }
    if (this.state.relatedto === 'isPasswordDisabled'){
        this.changePasswordMutation()
    }
    this.setState({relatedto:"nothing"});

  }
  

  handleCancel = () => {
    this.setState({openConfirmDialog: false});
    console.log("handle Cancel")
 }


  handleButtonsChange = (event, variable) => {
    switch(event){
        case 'edit':
            /*in edit case, i'll enable field for edit*/
            this.setState({[variable]: false});
        break;
        case 'save':
            /*in case of save, i'll disable field and save it to DB*/
            this.setState({[variable]: true});
            this.setState({confirmDialogMessage: this.messageConfirmSave});
            this.setState({relatedto: variable , openConfirmDialog: true});

        break;
        case 'cancel':
            /* is cancel case, i'll disable field and discard any change */
            this.setState({[variable]: true});
            this.setState({password: "******"})
            this.setState({name: this.props.getUserDetails.getCurrentUser.name})
            this.setState({confirmDialogMessage: this.messageConfirmCancel});
            /* TODO: discard any change made, i need to store the value on a variable */
        break;
        default:
            this.setState({[variable]: true});
        break;
    }
  }

  handleChangePassword = (event) => {
      this.setState({password: event.target.value})
  }

  handleChangeName = (event) => {
    this.setState({name: event.target.value})
}

  handleFocusPW = (event) => {
      event.target.value = ''
  }

  formLoading = () => {
    return(<div>
                <Form size="small" loading>
                    <Form.Group>
                        <Grid centered>
                            <Grid.Row columns={2}>
                                <Grid.Column width={15}>
                                    <Form.Input label={T.translate('signin.fullName')} disabled={this.state.isEmailDisabled} />
                                </Grid.Column>
                                <Grid.Column width={1}  verticalAlign="bottom" style={{ horizontalAlign: "left", marginBottom: "8px" }} >
                                    <ButtonEditSaveCance showEdit={true} changed={(event) => {this.handleButtonsChange(event,'isNameDisabled')}} />
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row columns={2}>
                                <Grid.Column width={15}>
                                    <Form.Input label={T.translate('signin.username')} disabled={this.state.isEmailDisabled}/>
                                </Grid.Column>
                                <Grid.Column width={1}>
                                    </Grid.Column>
                            </Grid.Row>
                            <Grid.Row columns={2}>
                                <Grid.Column width={15}>
                                    <Form.Input label={T.translate('signin.email')} disabled={this.state.isEmailDisabled}/>
                                </Grid.Column>
                                <Grid.Column width={1}  verticalAlign="bottom" style={{ horizontalAlign: "left", marginBottom: "8px" }} >
                                </Grid.Column>
                            </Grid.Row>
                            <Grid.Row columns={2}>
                                <Grid.Column width={15}>
                                    <Form.Input type="password" label={T.translate('signin.password')} disabled={this.state.isPasswordDisabled} />
                                </Grid.Column>
                                <Grid.Column width={1}  verticalAlign="bottom" style={{ horizontalAlign: "left", marginBottom: "8px" }} >
                                    <ButtonEditSaveCance showEdit={true} changed={(event) => {this.handleButtonsChange(event,'isPasswordDisabled')}} />
                                </Grid.Column>
                            </Grid.Row>
                        </Grid>
                    </Form.Group>
                </Form>
                <Confirm
                    open={this.state.openConfirmDialog}
                    onCancel={this.handleCancel}
                    onConfirm={this.handleConfirm}
                    content={this.state.confirmDialogMessage}
                    cancelButton={T.translate('myAccount.no')}
                    confirmButton={T.translate('myAccount.yes')}
                />
                </div>)
  }


  formError = () => {
    return(<div >
        <Grid centered>
            <Card>
                <Card.Content>
                    <Card.Header>
                    Oops! An Error has ocurred
                    </Card.Header>
                    <Card.Description>
                        An error has ocurred while loading this page
                    </Card.Description>
                </Card.Content>
                <Card.Content extra>
                    <div className='ui one buttons'>
                    <Button basic color='green' href='/my_account'>Refresh Page</Button>
                    </div>
                </Card.Content>
            </Card>
            </Grid>
            </div>)
  }

  formDefault = () => {
    return( <div>
            <Form size="small">
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column width={15}>
                                <Form.Input key="name" label={T.translate('signin.fullName')} disabled={this.state.isNameDisabled} type='text' onChange={this.handleChangeName} value={this.state.name} defaultValue={this.props.getUserDetails.getCurrentUser.name} />
                            </Grid.Column>
                            <Grid.Column width={1} verticalAlign="bottom" style={{ horizontalAlign: "left", marginBottom: "8px" }}>
                                <ButtonEditSaveCance showEdit={true} changed={(event) => {this.handleButtonsChange(event,'isNameDisabled')}} />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={15}>
                                <Form.Input label={T.translate('signin.username')} disabled defaultValue={this.props.getUserDetails.getCurrentUser.username} />
                            </Grid.Column>
                            <Grid.Column width={1}>
                                </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={15}>
                                <Form.Input label={T.translate('signin.email')} disabled={this.state.isEmailDisabled} defaultValue={this.props.getUserDetails.getCurrentUser.email}/>
                            </Grid.Column>
                            <Grid.Column width={1}  verticalAlign="bottom" >
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row columns={2}>
                            <Grid.Column width={15}>
                                <Form.Input id="password" type="password" label={T.translate('signin.password')} disabled={this.state.isPasswordDisabled} onChange={this.handleChangePassword} value={this.state.password}/>
                            </Grid.Column>
                            <Grid.Column width={1}  verticalAlign="bottom" style={{ horizontalAlign: "left", marginBottom: "8px" }} >
                                <ButtonEditSaveCance showEdit={true} changed={(event) => {this.handleButtonsChange(event,'isPasswordDisabled')}} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
            </Form>
            <Confirm
                open={this.state.openConfirmDialog}
                onCancel={this.handleCancel}
                onConfirm={this.handleConfirm}
                content={this.state.confirmDialogMessage}
                cancelButton={T.translate('myAccount.no')}
                confirmButton={T.translate('myAccount.yes')}
            />
            </div>
    )
  }


  render() {
        if (this.props.getUserDetails && this.props.getUserDetails.loading){
            return this.formLoading()
            
        }
        if (this.props.getUserDetails && this.props.getUserDetails.error){
            return this.formError()
            
        }
            return  this.formDefault()
       }
}

PersonalDataForm.propTypes = {
    getUserDetails: PropTypes.shape({
        loading: PropTypes.bool.isRequired,
        error: PropTypes.object,
    }).isRequired,
  };
  

export default compose( 
    graphql(USER_DETAILS_QUERY, {name:'getUserDetails' }),
    graphql(UpdateUserMutation,{name:'updateUserMutation'})
)(PersonalDataForm);