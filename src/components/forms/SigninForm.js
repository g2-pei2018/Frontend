import React, { Component } from 'react';
import { Divider, Form, Grid, Button} from 'semantic-ui-react';
import T from  'i18n-react';
import { graphql } from 'react-apollo'
import PropTypes from 'prop-types';
import { GC_USER_ID, GC_AUTH_TOKEN } from '../Authentication/constants'
import AUTHENTICATE_USER_MUTATION from '../../graphql/mutations/Login/AuthenticateUserMutation'



class SigninForm extends Component {
    state = {
        login: true, // switch between Login and SignUp
        email: '',
        password: '',
        name: ''
      }

      confirm = async () => {
        // const { name, email, password } = this.state          
        const { email, password } = this.state
        if (this.state.login) {
          const result = await this.props.authenticateUserMutation({
            variables: {
              email,
              password
            }
          })
          this.saveUserData(email, result.data.login.token)
        }
        this.props.history.push(`/`)
      }
    
      saveUserData = (email, token) => {
        localStorage.setItem(GC_USER_ID, email)
        localStorage.setItem(GC_AUTH_TOKEN, token)
      }
    
handleRouteTypeChange = (e, {value}) => {
        const routeType = value;
        this.setState({routeType})}

    render() {
        return (
            <div>
            <Form size="small">
                <Form.Group>
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column style={{ width: "90%" }}>
                                <Form.Input value={this.state.email}
                                            onChange={(e) => this.setState({ email: e.target.value })} label={T.translate('signin.email')}  />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column style={{ width: "90%" }}>
                                <Form.Input 
                                value={this.state.password}
                                onChange={(e) => this.setState({ password: e.target.value })}
                                type="password" label={T.translate('signin.password')} />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Button  
                                    role = 'button'// divs must specify their role, see  https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/no-static-element-interactions.md
                                    tabIndex = {0} // button class must be focusable, this means that object can be selected with tab key, see https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/interactive-supports-focus.md
                                    onClick={() => this.confirm()}
                                    className="teal" 
                                    style={{ marginLeft: "22px", width: "90%" }}>
                                    {T.translate('header.signin') }
                                </Button>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form.Group>
            </Form>
            <Divider horizontal>{T.translate('or')}</Divider>
            <span style={{ marginRight: "5%", marginLeft: "30%" }}>{T.translate('signin.signinWith')}</span>
            <Button circular color='facebook' icon='facebook' />
            <Button circular color='twitter' icon='twitter' />
            <Button circular color='google plus' icon='google plus' />
            </div>
        );
    }
}

SigninForm.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
      }).isRequired,
    authenticateUserMutation: PropTypes.func.isRequired
  };
  

export default graphql(AUTHENTICATE_USER_MUTATION, { name: 'authenticateUserMutation' })(SigninForm);