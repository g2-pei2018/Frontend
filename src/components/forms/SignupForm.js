import React, { Component } from 'react';
import { Divider, Form, Grid, Button} from 'semantic-ui-react';
import T from  'i18n-react';
import { graphql, compose } from 'react-apollo'
import PropTypes from 'prop-types';
import Dropzone from 'react-dropzone'
import axios from 'axios'
import { GC_USER_ID, GC_AUTH_TOKEN } from '../Authentication/constants'
import SIGNUP_USER_MUTATION_WITH_AVATAR from '../../graphql/mutations/Login/SignupUserMutationWithAvatar'
import SIGNUP_USER_MUTATION_WITHOUT_AVATAR from '../../graphql/mutations/Login/SignupUserMutationWithoutAvatar'

class SignupForm extends Component {
    state = {
        login: false, // switch between Login and SignUp
        email: '',
        password: '',
        name: '',
        username: '',
        uploadedFileCloudinaryUrl:'',
        originalFileNameCloudinaryUrl:'',
        originalFileHeightCloudinaryUrl: 0,
        originalFileWidthCloudinaryUrl: 0,
        originalFileFormatCloudinaryUrl: '',
        createAtCloudinaryUrl: ''
      }

      confirm = async () => {
        const { name, username, email, password } = this.state
        // const avatar = "https://res.cloudinary.com/wattcharger/image/upload/v1517418590/dwnixwo8wg03zcelwpgq.png"
        const avatar = this.state.uploadedFileCloudinaryUrl
        if (!this.state.login) {
            let result=null;
            if(avatar===''){
                result = await this.props.signupUserMutationWithoutAvatar({
                    variables: {
                    name,
                    username,
                    email,
                    password
                    }
                })
            }else{
                result = await this.props.signupUserMutationAvatar({
                    variables: {
                    name,
                    username,
                    email,
                    password,
                    avatar
                    }
                })
            }
            this.saveUserData(email, result.data.signup.token)
        }
        this.props.history.push(`/`)
      }
    
      saveUserData = (email, token) => {
        localStorage.setItem(GC_USER_ID, email)
        localStorage.setItem(GC_AUTH_TOKEN, token)
      }
    
    handleRouteTypeChange = (e, {value}) => {
        const routeType = value;
        this.setState({routeType})
    }

    handleDrop = files => {
        // Push all the axios request promise into a single array
        const uploaders = files.map(file => {
          // Initial FormData
          const formData = new FormData();
          formData.append("file", file);
          formData.append("tags", `codeinfuse, medium, gist`);
          formData.append("upload_preset", "vpxzvsye"); // Replace the preset name with your own
          formData.append("api_key", "298453577541125"); // Replace API key with your own Cloudinary key
          formData.append("timestamp", (Date.now() / 1000) || 0);
          
          // Make an AJAX upload request using Axios (replace Cloudinary URL below with your own)
          return axios.post("https://api.cloudinary.com/v1_1/wattcharger/image/upload", formData, {
            headers: { "X-Requested-With": "XMLHttpRequest" },
          }).then(response => {
            const data = response.data;
            const fileURL = data.secure_url // You should store this URL for future references in your app
            const fileName = data.original_filename
            const fileHeight = data.height
            const fileWidth = data.width
            const fileFormat = data.format
            const createAt = data.created_at
            // console.log(data);
            // console.log(fileURL);
            this.setState({uploadedFileCloudinaryUrl:fileURL});
            // console.log("setState... "+this.state.uploadedFileCloudinaryUrl);
            this.setState({originalFileNameCloudinaryUrl:fileName});
            // console.log("setState... "+this.state.originalFileNameCloudinaryUrl);
            this.setState({originalFileHeightCloudinaryUrl:fileHeight});
            // console.log("setState... "+this.state.originalFileHeightCloudinaryUrl);
            this.setState({originalFileWidthCloudinaryUrl:fileWidth});
            // console.log("setState... "+this.state.originalFileWidthCloudinaryUrl);
            this.setState({originalFileFormatCloudinaryUrl:fileFormat});
            // console.log("setState... "+this.state.originalFileFormatCloudinaryUrl);
            this.setState({createAtCloudinaryUrl:createAt});
            // console.log("setState... "+this.state.createAtCloudinaryUrl);
            
          })
        });  
      }

    render() {
        return (
            <div>
             <Form size="small">
                <Form.Group>
                    <Grid centered>
                        <Grid.Row>
                            <Grid.Column style={{ width: "90%" }}>
                                <Form.Input value={this.state.email}
                                            onChange={(e) => this.setState({ email: e.target.value })} label={T.translate('signin.email')}  />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column style={{ width: "90%" }}>
                                <Form.Input value={this.state.name}
                                            onChange={(e) => this.setState({ name: e.target.value })} label={T.translate('signin.fullName')}  />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column style={{ width: "90%" }}>
                                <Form.Input value={this.state.username}
                                            onChange={(e) => this.setState({ username: e.target.value })} label={T.translate('signin.username')}  />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column style={{ width: "90%" }}>
                                <Form.Input 
                                value={this.state.password}
                                onChange={(e) => this.setState({ password: e.target.value })}
                                type="password" label={T.translate('signin.password')} />
                            </Grid.Column>
                        </Grid.Row>
                        <Grid.Row>
                            <Dropzone
                                    width="10px" 
                                    onDrop={this.handleDrop} 
                                    multiple 
                                    accept="image/*" 
                                    style={{marginLeft:"0px"}} >
                                    <p style={{color: "grey"}}>Drop your avatar image or click here to upload</p>
                                    {this.state.uploadedFileCloudinaryUrl === '' ? null :
                                        <img alt={this.state.originalFileNameCloudinaryUrl} style={{width:"100px", height:"100px", marginLeft: "50px", backgroundColor:"white"}} src={this.state.uploadedFileCloudinaryUrl} />
                                    }
                            </Dropzone>
                        </Grid.Row>
                        <Grid.Row>
                            <Grid.Column>
                                <Button  
                                    role = 'button'// divs must specify their role, see  https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/no-static-element-interactions.md
                                    tabIndex = {0} // button class must be focusable, this means that object can be selected with tab key, see https://github.com/evcohen/eslint-plugin-jsx-a11y/blob/master/docs/rules/interactive-supports-focus.md
                                    onClick={() => this.confirm()}
                                    className="teal" 
                                    style={{ marginLeft: "22px", width: "90%" }}>
                                    {T.translate('header.signup') }
                                </Button>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Form.Group>
            </Form>
            
            <Divider horizontal>{T.translate('or')}</Divider>
            <span style={{ marginRight: "5%", marginLeft: "30%" }}>{T.translate('signin.signupWith')}</span>
            <Button circular color='facebook' icon='facebook' />
            <Button circular color='twitter' icon='twitter' />
            <Button circular color='google plus' icon='google plus' />
            </div>
        );
    }
}

SignupForm.propTypes = {
    history: PropTypes.shape({
        push: PropTypes.func.isRequired
      }).isRequired,
    signupUserMutationAvatar: PropTypes.func.isRequired,
    signupUserMutationWithoutAvatar: PropTypes.func.isRequired
  };
  

export default compose(
    graphql(SIGNUP_USER_MUTATION_WITH_AVATAR, { name: 'signupUserMutationAvatar' }),
    graphql(SIGNUP_USER_MUTATION_WITHOUT_AVATAR, { name: 'signupUserMutationWithoutAvatar' })
)(SignupForm);