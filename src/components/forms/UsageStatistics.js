import React, { Component } from 'react';
import {Tab} from 'semantic-ui-react';



const panes = [
    { menuItem: 'Tesla Model S', render: () => <Tab.Pane>Colocar aqui informação estatística sobre o carro. Quantos abastecimentos, o valor ja gasto, comparativo com o carro anterior, quer a nível de gastos de combostível quer de CO2 / CO que não produziu, etc.</Tab.Pane> },
    { menuItem: 'Nissan Leaf', render: () => <Tab.Pane>Colocar aqui informação estatística sobre o carro. Quantos abastecimentos, o valor ja gasto, comparativo com o carro anterior, quer a nível de gastos de combostível quer de CO2 / CO que não produziu, etc.</Tab.Pane> },
    { menuItem: 'Geral', render: () => <Tab.Pane>Juntar a informação dos carros anteriores e dar mais alguma informação relevante.</Tab.Pane> },
  ]


export default class UsageStatistics extends Component {

  render() {
    return (
      <div>
        <Tab menu={{fluid:true, vertical: true}} panes={panes} />
      </div>
    )
  }
}
