import React, { Component } from 'react';
import {Card, Image, Rating} from 'semantic-ui-react';
//import SignupForm from '../forms/SignupForm';


class ChargerDetailPage extends Component {
    state = {  }

    
    render() {
      
        return (
            <div>
                <Card>
                    <Card.Content>
                        <Image floated='right' size='mini' src='/assets/wattcharger.png' />
                        <Card.Header>
                            UM Charger
                        </Card.Header>
                        <Card.Meta>
                            CaDeMo
                        </Card.Meta>
                        <Card.Description>
                            This is a little description we could put here
                        </Card.Description>
                    </Card.Content>
                    <Card.Content extra>
                        <Rating size="mini" icon='star' defaultRating={3} maxRating={5} />
                    </Card.Content>
                </Card>
            </div>
        );
    }
}

export default ChargerDetailPage;