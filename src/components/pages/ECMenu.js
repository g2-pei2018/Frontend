import React, { Component } from 'react'
import {Menu, Icon} from 'semantic-ui-react';


class WCMenu extends Component {
    state = {  }
    handleItemClick = (e, { name }) => this.setState({ activeItem: name })
    render() {
        const { activeItem } = this.state
        return (
            <Menu stackable icon="labled">
                <Menu.Item onClick={() => { this.props.history.push('/')} }>
                <img src='assets/wattcharger.png' alt="Wattcharger logo" />
                </Menu.Item>

                <Menu.Menu position="right">
                    <Menu.Item 
                        name='show-map'
                        active={activeItem === 'features'}
                        onClick={this.handleItemClick} 
                        >
                        <Icon name="map" />
                        Show me the map
                    </Menu.Item>

                    <Menu.Item 
                        name='forum'
                        active={activeItem === 'testimonials'}
                        onClick={this.handleItemClick} 
                        >
                        <Icon name="comments" />
                        Forum
                    </Menu.Item>

                    <Menu.Item
                        name='sign-in'
                        active={activeItem === 'sign-in'}
                        onClick={this.handleItemClick} 
                        >
                        <Icon name="sign in" />
                        Sign-in
                    </Menu.Item>
                    {/* este menu so deve aparecer quando não há utilizador loggedin */}
                    <Menu.Item
                        name='signup'
                        active={activeItem === 'signup'}
                        onClick={this.handleItemClick} 
                        >
                        <Icon name="signup"/>{/* or add user */}
                        Signup
                    </Menu.Item>
                    {/* este menu so deve aparecer quando o utilizador esta loggedin */}
                    <Menu.Item
                        name='logout'
                        active={activeItem === 'sign-in'}
                        onClick={this.handleItemClick} 
                        >
                        <Icon name="sign out"/>
                        Logout
                    </Menu.Item>
                    
                </Menu.Menu>
            </Menu>
        );
    }
}

export default WCMenu;