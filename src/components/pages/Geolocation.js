import React from 'react';
import { geolocated } from 'react-geolocated';

class Geolocation extends React.Component {
  render() {
    return !this.props.isGeolocationAvailable
      ? <div>Your browser does not support Geolocation</div>
      : !this.props.isGeolocationEnabled
        ? <div>Geolocation is not enabled</div>
        : this.props.coords
          ? <div></div>
          : <div>Getting the location data&hellip; </div>;
  }
  // render() {
  //   return !this.props.isGeolocationAvailable
  //     ? <div>Your browser does not support Geolocation</div>
  //     : !this.props.isGeolocationEnabled
  //       ? <div>Geolocation is not enabled</div>
  //       : this.props.coords
  //         ? <table>
  //           <tbody>
  //             <tr><td><strong>Current Latitude:</strong></td><td>{this.props.coords.latitude}</td>
  //             <td><strong>&nbsp;</strong></td>
  //             <td><strong>Current Longitude:</strong></td><td>{this.props.coords.longitude}</td></tr>
  //           </tbody>
  //         </table>
  //         : <div>Getting the location data&hellip; </div>;
  // }
}

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false,
  },
  userDecisionTimeout: 5000,
})(Geolocation);