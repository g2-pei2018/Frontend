import React, { Component } from 'react';
import { Grid} from 'semantic-ui-react';
import {PropTypes} from 'prop-types';
import HomeForm from '../forms/HomeForm';
/* import '../../styles/HomePage.css'; */
class HomePage extends Component {
    state = {  }

    
    render() {
      
        return (
          <Grid centered>
            <Grid.Row>
              <Grid.Column width={6}>
                <HomeForm history={this.props.history}/>
              </Grid.Column>
            </Grid.Row>
          </Grid>
        );
    }
}

HomePage.propTypes = {
  history: PropTypes.shape({
    push: PropTypes.func.isRequired
  }).isRequired
};
export default HomePage;