import React, { Component } from 'react';
import PropTypes from 'prop-types'
import {Grid, Form, Dropdown } from 'semantic-ui-react';
import GoogleMapReact from 'google-map-react';
import { geolocated, geoPropTypes } from 'react-geolocated';
import T from 'i18n-react';
import Autocomplete from 'react-google-autocomplete';
import {BeatLoader} from 'react-spinners';
import Marker from './Marker';
import Geolocation from './Geolocation';
import Polyline from './Polyline';
import iterateAllPlans from  '../PolylineConvertion'

let updateCount=0;

class MapPage extends Component {
  
  constructor(params){
    super(params);
    let from;
    let to;
    let rt = 'CHEAPEST_JOURNEY';
    if(params.location.state !== undefined){
      from = params.location.state.state.fromPlace;
      to = params.location.state.state.toPlace;

     /** if i have percentage defined i'll try to translate it into my routeTypes and set the incoming routType */
    const p = params.location.state.state.percent;
      if(p !==undefined){
            if(p < 50){
              rt = 'FASTEST_JOURNEY';
            }else if(p <= 100){
              rt='MO_FASTEST_JOURNEY';
            }else{ /** ==100 */
              rt='CHEAPEST_JOURNEY';
            }
          }else{
            rt = 'CHEAPEST_JOURNEY';
          }
        }
    this.state = { 
      center : {lat: 41.5472695, lng: -8.4464406},
      zoom : 6,
      showMarkers: true,
      allMarkers: [],
      shownMarkers: [],
      apiKey: "AIzaSyCNx6g2L6ZMfeOzlYyaol7Y7f7HqsZhgU0",
      from:undefined ,
      to:undefined,
      fromPlace: from,
      toPlace: to,
      route: [],
      routeType: rt,
      placesChanged: false,
      routeChanged: false,
      loading:false
      }
    }

  componentDidMount = () => {
    if(this.state.fromPlace !== undefined){
      this.refFrom.refs.input.value=this.state.fromPlace;
    }
    if(this.state.toPlace !== undefined){
      this.refTo.refs.input.value=this.state.toPlace;
    }
    this.callApi();
    this.setVisibleMarkers();;
    this.calculateRoute();
  }  
  
  setVisibleMarkers = (nw,se) => {
    if(this.state.routeChargers === undefined){
      this.setState({shownMarkers: this.state.allMarkers})
    } else{
      this.setState({shownMarkers: this.state.routeChargers})
    }
  }

  handleChangedType = (ev, data) => {
    this.setState({routeType: data.value});
    if(!this.state.placesChanged){
        // change poly color
        this.setState({polyRoute: <Polyline routeType={data.value} map={this.state.map} maps={this.state.maps} route={this.state.routes} />,
          routeLoaded:true});
        }
  }

  /** Calculate route with API */
  calculateRoute = () => {
    this.setState({loading:true})
          if(this.state.from === undefined || this.state.to === undefined){
            return 
          }
          const from = this.state.from;
          const to = this.state.to;
          const data = {
            client:"journey-planner-for-EV",
            origin: {
            latE6: from.lat * 1E6,
            lonE6: from.lng * 1E6
            },
            destination: {
              latE6: to.lat * 1E6,
              lonE6: to.lng * 1E6
            },
            stateOfChargeInPerc:50,
            centsPerHour:40
          } 
      fetch("https://db1.umotional.net/charge-here/v1/journeys" , {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        })
        .then((response) => response.json())
        .then((res) =>{
          const result = iterateAllPlans(res.plans)
          this.setState({polyRoute: null});        
          this.setState({routes: result.routes, locations: result.markers})
          const marks = [];
          result.markers.forEach((item, index) => {
            const power = String("20");
            /* TODO: calculate marker color */
              marks.push(<Marker key={item.chargingTime} id={item.chargingTime} lat={item.coordinates.latitude} lng={item.coordinates.longitude} power={power} color={"#FF0000"} standards={[]} />);
          });
          this.setState({polyRoute: <Polyline routeType={this.state.routeType} map={this.state.map} maps={this.state.maps} route={result.routes} />,
                          routeLoaded:true, routeChargers: marks});
          this.setState({shownMarkers: marks, loading: false})
        })
      } 

  handleZoomChanged = (ev) => {
    this.setState({zoom: ev.zoom})
    if(this.state !== undefined){
      this.setVisibleMarkers(ev.bounds.nw,ev.bounds.se);
      if(this.state.zoom < 14){
        /* change this var to false to hide chargers on far zoom */
        this.setState({showMarkers:true})
      }else{
        this.setState({ showMarkers: true })
      }
    }
  }

  handleClickCalculateRoute = (ev) => {
    if(this.state.placesChanged){
      this.setState({placesChanged: false})
      this.calculateRoute();
    }else{
      // console.log("There are no changes. I'll do nothing");
    }
  }

  callApi = () => {
    this.setState({loading:true});
    const markers = [];
    fetch('https://api.openchargemap.io/v2/poi/?output=json&countrycode=DE&compact=true&verbose=false')
    .then((result) => {
      return result.json();
    }).then((jsonResult) => {
       
      jsonResult.forEach((item, index) => {
        const power = String(item.Connections[0].PowerKW);
        const chargerType = String(item.Connections[0].LevelID);

        markers.push(
          <Marker key={item.ID} id={item.ID} lat={item.AddressInfo.Latitude} lng={item.AddressInfo.Longitude} power={power} description={item.AddressInfo.Title} chargerType={chargerType} standards={["CHAdeMO","SAE"]} />
      );
    });
      this.setState({allMarkers: markers, loading:false});
    })
  }

  render = () => {
    geolocated({
      positionOptions: {
        enableHighAccuracy: false,
      },
      userDecisionTimeout: 5000,
    })

    // Using Object.assign
    MapPage.propTypes = Object.assign({}, MapPage.propTypes, geoPropTypes);
    
    if(!this.props.isGeolocationAvailable)
      console.log("Your browser does not support Geolocation");
    else if(!this.props.isGeolocationEnabled)
      console.log("Geolocation is not enabled");
      else if(this.props.coords){
          // console.log(`Current Latitude: ${this.props.coords.latitude} and Current Longitude: ${this.props.coords.longitude}`);
          if(updateCount===0){
            this.setState({ 
              // center : {lat: this.props.coords.latitude, lng: this.props.coords.longitude},
              // center to germany, it's there where we haver chargers
              center: {lat: 52.522391,lng: 13.404101},
              // zoom : 15
              zoom: 8
            })
            updateCount = 1;
          }
      }
    /*  else
          console.log("Getting the location ...");
    const Markers = [<Marker key="marker1" id="marker1" lat={41.557964} lng={-8.398414} power={'43 kW'} color={"#FF5555"} standards={["CHAdeMO","SAE"]} />,
                    <Marker key="marker2" id="marker2" lat={41.556923} lng={-8.399946} power={'11 kW'} color={"#55FF55"} standards={["CCS","CHAdeMO"]} />,
                    <Marker key="marker3" id="marker3" lat={41.556225} lng={-8.396674} power={'50 kW'} color={"#5555FF"} standards={["Tesla Supercharger"]} />
    ]; */

    /* these are routeTypes i have so far. How can i translate this? */
    const routeTypes = [{ key: 'CHEAPEST_JOURNEY', value: 'CHEAPEST_JOURNEY', text : 'Cheapest journey'},
                    {key: 'FASTEST_JOURNEY', value: 'FASTEST_JOURNEY', text : 'Fastest journey'},
                    {key: 'MO_FASTEST_JOURNEY', value: 'MO_FASTEST_JOURNEY', text: ' Most fastest journey'}]

    return (
        <div id="map" style={{height: "100%"}}>
          <Grid style={{height:"85%"}}>
            <Grid.Row >
                        <Form>
                          <Form.Group widths='equal'>
                            <Form.Input>
                                <Autocomplete placeholder={T.translate("map.selectOrigin")} id="from" ref={(c) => {this.refFrom = c}} onPlaceSelected={(place) => { this.setState({from: {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()}, placesChanged: true, fromPlace:place.formatted_address})}} types={['(regions)']} />
                            </Form.Input>
                            <Form.Input>
                                <Autocomplete placeholder={T.translate("map.selectDestination")} id="to" ref={(c) => {this.refTo = c}} onPlaceSelected={(place) => { this.setState({to: {lat: place.geometry.location.lat(), lng: place.geometry.location.lng()}, placesChanged: true,toPlace: place.formatted_address})}} types={['(regions)']}/>
                            </Form.Input>
                            <Form.Field>
                                <Dropdown selection value={this.state.routeType} onChange={this.handleChangedType} placeholder={T.translate('map.routeType')} options={routeTypes} />
                            </Form.Field>
                            <Form.Button onClick={this.handleClickCalculateRoute} content={T.translate('map.calculateNewRoute')} icon="play" labelPosition="right" />
                          </Form.Group>
                        </Form>
            
                    <GoogleMapReact onChange={this.handleZoomChanged.bind(this)} 
                        center={this.state.center}

                        onGoogleApiLoaded={
                                (map) => {
                                    this.setState({map: map.map, maps: map.maps, mapLoaded: true})
                                }
                              }
                        yesIWantToUseGoogleMapApiInternals
                        zoom={this.state.zoom}
                        onChildClick={this._onChildClick}
                      >{this.state.showMarkers === true ? this.state.shownMarkers: []}</GoogleMapReact>
                      <Geolocation /> 
                      { /* this.state.mapLoaded && this.state.routeLoaded && <Polyline map={this.state.map} maps={this.state.maps} route={this.state.route} /> */ }
                      { 
                        this.state.routeLoaded ? this.state.polyRoute: []
                        }
            </Grid.Row>
          </Grid>
          <div style={{position:"absolute",margin:"auto"}}>
          <BeatLoader color={'#D0021B'}
              size={20}
              margin="8px"
              loading={this.state.loading}
              />
              </div>
      </div>
    );
  }
}

MapPage.propTypes= {
  isGeolocationAvailable: PropTypes.bool.isRequired,
  isGeolocationEnabled: PropTypes.bool.isRequired,
  coords:PropTypes.shape({
    latitude: PropTypes.number,
    longitude: PropTypes.number
  })
}

MapPage.defaultProps = {
  coords:PropTypes.shape({
    latitude: 41.5472695,
    longitude: -8.4464406
  })
};

export default geolocated()(MapPage);