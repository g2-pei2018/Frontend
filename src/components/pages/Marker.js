import React, {Component} from 'react';
import { Popup, Card, Rating, Image, Statistic, Modal, Button, Icon, Comment, Form, Visibility} from 'semantic-ui-react';
import {PropTypes} from 'prop-types';
import T from 'i18n-react';
import POI from './POI';
/* import { prototype } from 'zen-observable'; */
import { MarkerStyle, MarkerStyleHover, MarkerTextStyle, MarkerTextStyleHover, 
          Level0Style, Level1Style, Level2Style, Level3Style, Level4Style,
          Level0Bolt, Level1Bolt, Level2Bolt, Level3Bolt, Level4Bolt} from './Marker_style';

export default class Marker extends Component {

  static propTypes = {
    text: PropTypes.string,
    power: PropTypes.string,
    standards: PropTypes.array,
    chargerPhoto: PropTypes.string,
    chargerType: PropTypes.string
  };

  constructor(props){
    super(props);
    
    const markerStyle = this.getChargerSytle(props.chargerType);


    this.state = {
      style : MarkerStyle,
      styleText : MarkerTextStyle,
      isPopupOpen: false,
      chargerImage: 'https://insideevs.com/wp-content/uploads/2015/12/CUH33X-WoAA-zfT-750x500.jpg',
      statistics: [
        { key: 'faves', label: 'comments', value: '2,321' },
        { key: 'users', label: 'users', value: '65' },
      ],
      markerColor: markerStyle.markerColor,
      boltColor: markerStyle.boltColor,
      isModalOpen: false, 
      isPoisOpen: false,
      description: props.description,
      comments: [<Comment key="comment1">
                    <Comment.Avatar as='a' src='https://image.flaticon.com/icons/png/512/78/78373.png' />
                    <Comment.Content>
                      <Comment.Author>Joe Henderson</Comment.Author>
                      <Comment.Metadata>
                        <div>1 day ago</div>
                      </Comment.Metadata>
                      <Comment.Text>
                        <p>The hours, minutes and seconds stand as visible reminders that your effort put them all there.</p>
                      </Comment.Text>
                      <Comment.Actions>
                        <Comment.Action>Reply</Comment.Action>
                      </Comment.Actions>
                    </Comment.Content>
               </Comment>,
                <Comment key="comment2">
                  <Comment.Avatar as='a' src='https://image.flaticon.com/icons/png/512/78/78373.png' />
                  <Comment.Content>
                    <Comment.Author>Christian Rocha</Comment.Author>
                    <Comment.Metadata>
                      <div>2 days ago</div>
                    </Comment.Metadata>
                    <Comment.Text>I re-tweeted this.</Comment.Text>
                    <Comment.Actions>
                      <Comment.Action>Reply</Comment.Action>
                    </Comment.Actions>
                  </Comment.Content>
                </Comment>,
                <Comment key="comment3">
                  <Comment.Avatar as='a' src='https://image.flaticon.com/icons/png/512/78/78373.png' />
                  <Comment.Content>
                    <Comment.Author>Mad Max</Comment.Author>
                    <Comment.Metadata>
                      <div>2 days ago</div>
                    </Comment.Metadata>
                    <Comment.Text>I re-tweeted this.</Comment.Text>
                    <Comment.Actions>
                      <Comment.Action>Reply</Comment.Action>
                    </Comment.Actions>
                  </Comment.Content>
                </Comment>,
                <Comment key="comment4">
                  <Comment.Avatar as='a' src='https://image.flaticon.com/icons/png/512/78/78373.png' />
                  <Comment.Content>
                    <Comment.Author>John Wick</Comment.Author>
                    <Comment.Metadata>
                      <div>2 days ago</div>
                    </Comment.Metadata>
                    <Comment.Text>I re-tweeted this.</Comment.Text>
                    <Comment.Actions>
                      <Comment.Action>Reply</Comment.Action>
                    </Comment.Actions>
                  </Comment.Content>
                </Comment>],
      calculations: {
        direction: 'none',
        height: 0,
        width: 0,
        topPassed: false,
        bottomPassed: false,
        pixelsPassed: 0,
        percentagePassed: 0,
        topVisible: false,
        bottomVisible: false,
        fits: false,
        passing: false,
        onScreen: false,
        offScreen: false,
      }
    };
   /*  if(this.props.chargerPhoto === undefined){
      this.state = {chargerImage:'assets/cars/tesla-model-s.jpg'};
    } */
  }


  componentDidUpdate(prevProps) {
    const K_TRANS_DELAY = 30;

    if (prevProps.$hover !== this.props.$hover) {
      setTimeout(() => this._onHoverStateChange(this.props.$hover), K_TRANS_DELAY);
    }

    if (prevProps.showBallon !== this.props.showBallon) {
      setTimeout(() => this._onShowBallonStateChange(this.props.showBallon), K_TRANS_DELAY);
    }
  }

  getChargerSytle = (type) => {
    let style;
      switch (type) {
        case undefined:
          style = {markerColor: Level1Style, boltColor: Level1Bolt};
        break;
        case "0":
          style = {markerColor: Level0Style, boltColor: Level0Bolt};
        break;
        case "1":
          style = {markerColor: Level1Style, boltColor: Level1Bolt};
        break;
        case "2":
          style = {markerColor: Level2Style, boltColor: Level2Bolt}
        break;
        case "3":
          style = {markerColor: Level3Style, boltColor: Level3Bolt}
        break;
        case "4":
          style = {markerColor: Level4Style, boltColor: Level4Bolt}
          break;
        default:
          style = {markerColor: Level0Style, boltColor: Level0Bolt}
          break;
        }
        return style;
      }
  _onShowBallonStateChange = (...args) => {
    // if (!this.alive) return;
    // this.props.onShowBallonStateChange(...args);
    console.log("Mostrei balao");;
  }

  _onHoverStateChange = (...args) => {
    // if (!this.alive) return;
    // this.props.onHoverStateChange(...args);
    if(args[0]){
      this.setState({style : MarkerStyleHover});
      this.setState({styleText : MarkerTextStyleHover});
      
    }else{
      this.setState({style : MarkerStyle});      
      this.setState({styleText : MarkerTextStyle});
      // this.setState({ isPopupOpen: false })
    }
  }
  
  handleOpen = () => {
    this.setState({ isPopupOpen: true })
  }

  handleClose = () => {
    this.setState({ isPopupOpen: false })
  }

  handleOpenModal = () => {
    this.setState({isPopupOpen: false})
    this.setState({ isModalOpen: true })
  }
  handleCloseModal = () => {
    this.setState({ isModalOpen: false })
    this.setState({isPopupOpen: true})
  }

  handleOpenModalPois = () => {
    this.setState({isPopupOpen: false})
    this.setState({isPoisOpen: true})
  }
  handleCloseModalPois = () => {
    this.setState({ isPoisOpen: false })
    this.setState({isPoisOpen: false})
  }

  _onMouseEnterContent = (/* e */) => {
    // this.props.Mouse entrei(false); // disable mouse move hovers
    console.log("Mouse entrei");
  }

  _onMouseLeaveContent = (/* e */) => {
    // this.props.$onMouseAllow(true); // enable mouse move hovers
    console.log("Mouse sai");
  }

  _onCloseClick = () => {
    // if (this.props.onCloseClick) {
      // this.props.onCloseClick();
    // }
    console.log("Cliquei para fechar");
  }
  _onChildClick = (key, childProps) => {
    //  this.props.onCenterChange([childProps.lat, childProps.lng]);
    console.log("Cliquei no charger");
  }


  handleUpdate = (e, { calculations }) => {
    this.setState({ calculations });
    if(this.state.calculations.bottomVisible){
      console.log("Cheguei ao fundo, vou fazer reload");
    }
  }

  render() {
    // const style = this.props.$hover ? MarkerStyleHover : MarkerStyle;
    return (<div>
        <Popup ref="popup" on='click'
         trigger={<div style={this.state.style}>
                  <div width={"100%"}>
                    {/* <svg viewBox="0 0 62 60" version="1.1" xmlns="http://www.w3.org/2000/svg">
                        <g id="icon_w_text" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                            <g id="map39-+-Rectangle-1">
                                <g id="map39" >
                                    <path d="M11.625,0 C5.20327734,0 0,5.03377156 0,11.2462955 L0,29.9901212 C0,36.2026451 5.20327734,41.2364167 11.625,41.2364167 L16.6658284,41.5617849 L15.153142,59.0262667 L44.1359347,41.5004739 L50.375,41.2364167 C56.7967227,41.2364167 62,36.2026451 62,29.9901212 L62,11.2462955 C62,5.03377156 56.7967227,0 50.375,0 L11.625,0 Z" id="Path-2" fillOpacity="0.6" fill="#FFFFFF" />
                                    <path style={{fill: this.state.colorRect}} d="M12.875,2 C6.86758203,2 2,6.75611069 2,12.6259542 L2,30.3358779 C2,36.2057214 6.86758203,40.9618321 12.875,40.9618321 L20.1231875,40.9618321 L14.9538879,60.0458313 L41.875,40.9618321 L49.125,40.9618321 C55.132418,40.9618321 60,36.2057214 60,30.3358779 L60,12.6259542 C60,6.75611069 55.132418,2 49.125,2 L12.875,2 Z" id="Shape" fill="#FF0000" />
                                </g>
                                <rect id="Rectangle-1" fill="#FFFFFF"x="6" y="7" width="50" height="29" rx="7" />
                            </g>
                        </g>
                    </svg> */}

                    <svg
                      
                      viewBox="0 0 210 297"
                      version="1.1"
                      id="svg8">
                      <g id="layer1">
                        <g
                          id="layer1-4"
                          transform="matrix(1.979049,0,0,1.979049,17.176864,8.2317467)">
                          <path
                            d="m 87.302,43.651 c 0,24.108 -23.511,67.46 -43.651,99.206 C 26.488,110.117 0,67.755 0,43.648 5.2381e-8,19.54 19.543,-0.003 43.651,-0.003 c 24.107,8e-8 43.651,19.543 43.651,43.651 z"
                            id="marker" style={this.state.markerColor} />
                        </g>
                        <g
                          id="g59"
                          transform="matrix(0.14146078,0,0,0.14146078,33.461552,21.256343)">
                          <g style={this.state.boltColor}
                            id="lightning">
                            <g
                              id="c144_ray">
                              <path
                                id="path22"
                                d="M 500,10 C 229.4,10 10,229.4 10,499.9 c 0,270.7 219.4,490 490,490 270.7,0 490,-219.4 490,-490 C 990,229.4 770.7,10 500,10 Z m 0,929.8 C 257.1,939.8 60.1,742.8 60.1,499.9 60.1,257.1 257.1,60.2 500,60.2 c 242.9,0 439.9,197 439.9,439.8 0,242.9 -197,439.8 -439.9,439.8 z"
                                />
                              <polygon
                                id="polygon24"
                                points="430.5,545.4 322.8,830.3 709.7,454.7 569.5,454.7 668.2,169.7 290.4,545.4 " />
                            </g>
                          </g>
                        
                        </g>

                      </g>
                    </svg>
                  </div>
                  {/* <div style={this.state.styleText}>{this.props.power}</div> */}
              </div>}
          /* header={this.props.power} */
          content={<Card >
                      <Card.Content>
                        <Card.Header>
                            {this.props.description}
                        </Card.Header>
                        <Image onClick={this.handleOpenModal} size='small' src={this.state.chargerImage} />
                        <Card.Meta>
                            {T.translate('map.marker.charger')}
                        </Card.Meta>
                        <Card.Description>
                            {<div>{this.props.standards}</div>}
                        </Card.Description>
                      </Card.Content>
                      <Card.Content>
                        {/* <Rating size="mini" icon='star' defaultRating={3} maxRating={5} />
                        <Statistic.Group size="mini" items={this.state.statistics} /> */}
                        <Button onClick={this.handleOpenModal}>{T.translate('map.marker.showComments')}</Button>
                        <Button onClick={this.handleOpenModalPois}>{T.translate('map.marker.pointsOfInteress')}</Button>
                      </Card.Content>
                    </Card>
                    
                }
          hoverable
          open={this.state.isPopupOpen}
          onClose={this.handleClose}
          onOpen={this.handleOpen}
            />
          <Modal id="charger" open={this.state.isModalOpen}
                  onClose={this.handleCloseModal}
                  size='small' >
                  
                  <Modal.Content>
                  <Comment.Group>
                              {this.state.comments}
                              <Form reply>
                                <Form.TextArea />
                                <Button content='Add Comment' labelPosition='left' icon='edit' primary />
                              </Form>
                        </Comment.Group>
                  </Modal.Content>

                  <Modal.Actions>
                    <Button color='green' onClick={this.handleCloseModal} inverted>
                      <Icon name='checkmark'/> {T.translate("map.marker.gotIt")}
                    </Button>
                  </Modal.Actions>
                </Modal>

                <Modal id="pois" open={this.state.isPoisOpen}
                  onClose={this.handleCloseModalPois}
                  size='small' >
                  
                  <Modal.Content>
                    <h3>{T.translate("map.pois")}</h3>
                  </Modal.Content>
                  
                    <Visibility onUpdate={this.handleUpdatePois}>
                      <Modal.Content>
                        {/* <Comment.Group>
                          <Comment key="comment10">
                            <Comment.Avatar as='a' src='/assets/cars/nissan-leaf.jpg' />
                            <Comment.Content>
                              <Comment.Author>Here willl appear some POIs near charger</Comment.Author>
                              <Comment.Metadata>
                                <div>2 days ago</div>
                              </Comment.Metadata>
                              <Comment.Text>I re-tweeted this.</Comment.Text>
                              <Comment.Actions>
                                <Comment.Action>Reply</Comment.Action>
                              </Comment.Actions>
                            </Comment.Content>
                          </Comment>
                        </Comment.Group> */}
                        <POI />
                        </Modal.Content>
                    </Visibility>

                  <Modal.Actions>
                    <Button color='green' onClick={this.handleCloseModalPois} inverted>
                      <Icon name='checkmark'/> {T.translate("map.marker.gotIt")}
                    </Button>
                  </Modal.Actions>
                </Modal>
        </div>
    );
  }

  
}