const K_WIDTH = 30;
const K_HEIGHT = 30;

const MarkerStyle = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  width: K_WIDTH,
  height: K_HEIGHT,
  left: -K_WIDTH / 2,
  top: -K_HEIGHT / 2,
  border: '0px',
  textAlign: 'center',
  color: '#a5aaaf',
  fontSize: 8,
  fontWeight: 'bold',
  padding: 4
};

const MarkerTextStyle = {
    position: 'relative',
    top: -2* K_HEIGHT / 3
};


const MarkerStyleHover = {
    // initially any map object has left top corner at lat lng coordinates
    // it's on you to set object origin to 0,0 coordinates 
    position: 'absolute',
    width: (K_WIDTH * 1.4),
    height: (K_HEIGHT * 1.4),
    left: -(K_WIDTH*0.7),
    top: -(K_HEIGHT*1),
    // border: '5px solid #f44336',
    border: '0px',
    textAlign: 'center',
    color: '#343638',
    fontSize: 16,
    fontWeight: 'bold',
    padding: 4
  };

const MarkerTextStyleHover = {
  position: 'relative',
  top: -4 * K_HEIGHT/3 - 4
};

const MarkerGreen = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates 
  fill:'#FF0000'
};


/** markers style */
const Level0Style ={
  fill: '#979797'
}
const Level1Style ={
  fill: '#b19e52'
}
const Level2Style ={
  fill: '#a6c575'
}
const Level3Style ={
  fill: '#ff9650'
}
const Level4Style ={
  fill: '#ff2e2e'
}



/** bolt style */
const Level0Bolt ={
  fill: '#ffffff'
}
const Level1Bolt ={
  fill: '#ffffff'
}
const Level2Bolt ={
  fill: '#ffffff'
} 
const Level3Bolt ={
  fill: '#ffffff'
}
const Level4Bolt ={
  fill: '#ffffff'
}

export { MarkerGreen, MarkerStyle, MarkerTextStyle, MarkerStyleHover, MarkerTextStyleHover, Level0Style, Level1Style, Level2Style, Level3Style, Level4Style, Level0Bolt, Level1Bolt, Level2Bolt, Level3Bolt, Level4Bolt};