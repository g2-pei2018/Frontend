import React, { Component } from 'react';
import {Accordion, Icon } from 'semantic-ui-react';
import T from 'i18n-react';
import PersonalDataForm from '../forms/PersonalDataForm';
import MyCars from '../forms/MyCars';
// import UsageStatistics from '../forms/UsageStatistics';



class MyAccount extends Component {
    state = {
        openedIdx:0
    }

  handleAccordionClick = (e, tittleProps) => {
      const { index } = tittleProps;
      const { openedIdx } = this.state;
      const newIndex = openedIdx === index ? -1 : index;

      this.setState({openedIdx : newIndex});
  }

  render() {
    return (
      <div>
        <Accordion styled fluid >
            <Accordion.Title active={this.state.openedIdx === 0 } index={0} onClick={this.handleAccordionClick}>
                <Icon name='dropdown' />
                {T.translate('myAccount.personalData')}
            </Accordion.Title>
            <Accordion.Content active={this.state.openedIdx === 0}>
                <PersonalDataForm />
            </Accordion.Content>
            <Accordion.Title active={this.state.openedIdx === 1} index={1} onClick={this.handleAccordionClick}>
                <Icon name='dropdown' />
                {T.translate("myAccount.myEv")}
            </Accordion.Title>
            <Accordion.Content active={this.state.openedIdx === 1}>
                <MyCars />
            </Accordion.Content>
            {/* <Accordion.Title active={this.state.openedIdx === 2} index={2} onClick={this.handleAccordionClick}>
                <Icon name='dropdown' />
                {T.translate("myAccount.usageStatistics")}
            </Accordion.Title>
            <Accordion.Content active={this.state.openedIdx === 2}>
                <UsageStatistics />
            </Accordion.Content> */}
        </Accordion>
      </div>
    )
  }
}

export default MyAccount;