import React, { Component } from 'react';
import { Button, Header, Icon, Modal, Card, /* Rating, */ Image } from 'semantic-ui-react';
import T from 'i18n-react';

class MapPage extends Component {

    state = { modalOpen: false }

  	handleOpen = () => this.setState({ modalOpen: true })

  	handleClose = () => this.setState({ modalOpen: false })
    
    render() {
      
        return (
        	<div style={{ maxHeight: "700px", overflow: "auto" }}>
        		<Card style={{ marginLeft: "5px", marginTop: "1%", width: "90%" }}>
            		<Card.Content>
            	        <Card.Header>
            	            <Modal
      						  trigger={<a onClick={this.handleOpen}>Famous bakery</a>}
      						  open={this.state.modalOpen}
      						  onClose={this.handleClose}
      						  basic
      						  size='small'
      						>
      						  <Header icon='coffee' content='Famous bakery' />
      						  <Modal.Content>
      						  	<Image wrapped size='medium' src='http://www.allrestaurants.eu/img/af/52413/14954495069948.jpg' />
      						    <h3>This bakery is famous not only by its name nut also by its cakes!</h3>
      						  </Modal.Content>
      						  <Modal.Actions>
      						    <Button color='green' onClick={this.handleClose} inverted>
      						      <Icon name='checkmark' /> {T.translate('map.poi.gotit')}
      						    </Button>
      						  </Modal.Actions>
      						</Modal>
            	        </Card.Header>
            	        <Card.Meta>
            	            Cake shop
            	        </Card.Meta>
            	        <Card.Description>
            	            You have to taste our cream cake!!!
            	        </Card.Description>
            	    </Card.Content>
            	    {/* <Card.Content extra>
            	        <Rating size="mini" icon='star' defaultRating={2} maxRating={5} />
            	    </Card.Content> */}
            	</Card>
            </div>
        );
    }
}

export default MapPage;