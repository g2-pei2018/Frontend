import React, { Component } from 'react';
import {Card, Image, Rating, Input, Icon, Grid} from 'semantic-ui-react';


class POIList extends Component {
    state = {  }

    
    render() {
      
        return (
                <Grid>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Input
                                    icon={<Icon name='search' inverted circular link />}
                                    placeholder='Search...' 
                                />
                        </Grid.Column>
                    </Grid.Row>
                    <Grid.Row columns={1}>
                        <Grid.Column>
                            <Card.Group>
                                <Card>
                                    <Card.Content>
                                        <Image floated='right' size='mini' src='/assets/images/avatar/large/steve.jpg' />
                                        <Card.Header>
                                            Pastelaria Montalegrense
                                        </Card.Header>
                                        <Card.Meta>
                                            Cake shop
                                        </Card.Meta>
                                        <Card.Description>
                                            This is a little description we could put here
                                        </Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Rating size="mini" icon='star' defaultRating={3} maxRating={5} />
                                    </Card.Content>
                                </Card>
                                <Card>
                                    <Card.Content>
                                        <Image floated='right' size='mini' src='/assets/images/avatar/large/steve.jpg' />
                                        <Card.Header>
                                            Universidade do Minho
                                        </Card.Header>
                                        <Card.Meta>
                                            Shoolar
                                        </Card.Meta>
                                        <Card.Description>
                                            Campus de Gualtar - University of Minho
                                        </Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Rating size="mini" icon='star' defaultRating={4} maxRating={5} />
                                    </Card.Content>
                                </Card>
                                <Card>
                                    <Card.Content>
                                        <Image floated='right' size='mini' src='/assets/images/avatar/large/steve.jpg' />
                                        <Card.Header>
                                            Braga Parque
                                        </Card.Header>
                                        <Card.Meta>
                                            Shopping
                                        </Card.Meta>
                                        <Card.Description>
                                            A shooping inside city
                                        </Card.Description>
                                    </Card.Content>
                                    <Card.Content extra>
                                        <Rating size="mini" icon='star' defaultRating={3} maxRating={5} />
                                    </Card.Content>
                                </Card>
                            </Card.Group>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
        );
    }
}

export default POIList;