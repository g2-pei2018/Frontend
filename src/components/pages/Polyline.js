import { PureComponent/* , React  */} from 'react'
import {CHEAPEST_ROUTE_COLOR, FASTEST_ROUTE_COLOR, MO_FASTEST_ROUTE_COLOR} from  '../PolylineConvertion'
/* import {BeatLoader} from 'react-spinners'; */

export default class Polyline extends PureComponent {
  
  constructor(props){
    super(props);
    this.state = {
                  route: this.props.route,
                  routeType: this.props.routeType,
                  typeIndex: 0,
                  polyColor: CHEAPEST_ROUTE_COLOR,
                  routeChanged: this.props.routeChanged
                    };
    
  }
  state = { 
  }

  componentDidMount(){
    this.setRouteColor();
  }

    componentWillUnmount(){
      if(this.linex !== undefined){
        this.linex.setMap(null)
      }
    }
    
    setRouteColor(){
      switch (this.props.routeType) {
        case "CHEAPEST_JOURNEY":
          this.setState({polyColor: CHEAPEST_ROUTE_COLOR})
          break;
        case "FASTEST_JOURNEY":
          this.setState({polyColor: FASTEST_ROUTE_COLOR})
          break;
        case "MO_FASTEST_JOURNEY":
          this.setState({polyColor: MO_FASTEST_ROUTE_COLOR})
          break;
      
        default:
          this.setState({polyColor: CHEAPEST_ROUTE_COLOR})
          break;
      }
    }

    setMap = (m) => {
      this.setState({map: m});
    }

    removeLine(){
      this.linex.setMap(null)
    }

    componentWillUnmout(){
      if(this.linex !== undefined){
        this.linex.setMap(null);
      }
    }

    renderPolyline() {
      let nonGeodesicPolyline;
      if(this.state.route !== undefined){
            this.setRouteColor();
              const { map, maps } = this.props
              if(this.linex !== undefined){
                this.linex.setMap(null);
              }
              let pathNew = []
              let idx = 0;
              this.state.route.forEach((element, index) => {
                if(element.type === this.props.routeType){
                  idx = index;
                }
              });

              pathNew = this.state.route[idx].route.map(el => {
                return {lat: el.latitude, lng: el.longitude}
              })

              nonGeodesicPolyline = new maps.Polyline({
                path: pathNew,
                geodesic: false,
                strokeColor: this.state.polyColor,
                strokeOpacity: 0.7,
                strokeWeight: 4
              })
              nonGeodesicPolyline.setMap(map)
            }
        return nonGeodesicPolyline;
      }

      render() {
        const renderedPolyline = this.renderPolyline()
        this.linex = renderedPolyline;
        const paths = { path: this.state.route }
        this.line = new Polyline(Object.assign({}, renderedPolyline, paths, this.state.routeType))
        return null;
      }
    }
    /* class Normal extends Polyline {
      renderPolyline() {
        return {
          geodesic: true,
          strokeColor: this.props.color || '#ffffff',
          strokeOpacity: 1,
          strokeWeight: 4
        }
    }
} */