import React, { Component } from 'react';
import {Image, Input, Grid} from 'semantic-ui-react';

import '../../styles/RangeSelect.css';

class RangeSelect extends Component {
    state = {  }

    
    render() {
      
        return (
            <div className="wc-slider">
                <div className="wc-left"> Faster </div>
                <div className="wc-track">
                    <input type="range" min={0} max={5}/>
                </div>
                <div className="wc-right"> Few stops </div>
            </div>
        );
    }
}

export default RangeSelect;