import gql from 'graphql-tag'

const AUTHENTICATE_USER_MUTATION = gql`
mutation AuthenticateUserMutation($email: String!, $password: String!) {
  login(
    email: $email,
    password: $password
  ) {
    token
  }
}
`

export default AUTHENTICATE_USER_MUTATION;