import gql from 'graphql-tag'

const SIGNUP_USER_MUTATION = gql`
mutation SignupUserMutation($name: String!, $username: String!, $email: String!, $password: String!, $avatar: String!) {
  signup(
    name: $name, 
    username: $username, 
    email: $email, 
    password: $password,
    avatar: $avatar
  ) {
    token
  }
}
`

export default SIGNUP_USER_MUTATION;