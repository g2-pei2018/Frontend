import gql from 'graphql-tag'

export const ADD_DOWNVOTE_COMMENT = gql`
mutation AddDownvoteCommentMutation($id:Int!){
  addDownvoteComment(id:$id){
    comment{
      comment,
      upvotes,
      downvotes
    },
    user{
      username,
      avatar
    }
  }
}
`

export default ADD_DOWNVOTE_COMMENT;