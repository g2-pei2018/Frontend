import gql from 'graphql-tag'

export const ADD_DOWNVOTE_POST = gql`
mutation AddDownvotePostMutation($id:Int!){
  addDownvotePost(id:$id){
    post{
      title,
      body,
      upvotes,
      downvotes
    },
    user{
      username,
      avatar
    }
  }
}
`

export default ADD_DOWNVOTE_POST;