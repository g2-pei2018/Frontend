import gql from 'graphql-tag'

export const ADD_UPVOTE_COMMENT = gql`
mutation AddUpvoteCommentMutation($id:Int!){
  addUpvoteComment(id:$id){
    comment{
      comment,
      upvotes,
      downvotes
    },
    user{
      username,
      avatar
    }
  }
}
`

export default ADD_UPVOTE_COMMENT;