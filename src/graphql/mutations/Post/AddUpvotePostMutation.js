import gql from 'graphql-tag'

export const ADD_UPVOTE_POST = gql`
mutation AddUpvotePostMutation($id:Int!){
  addUpvotePost(id:$id){
    post{
      title,
      body,
      upvotes,
      downvotes
    },
    user{
      username,
      avatar
    }
  }
}
`

export default ADD_UPVOTE_POST;