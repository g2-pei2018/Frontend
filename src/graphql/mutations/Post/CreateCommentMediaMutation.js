import gql from 'graphql-tag'

export const CREATE_COMMENT_MEDIA_MUTATION = gql`
mutation CreateCommentMediaMutation($media: String!, $commentId: Int!) {
  createCommentMedia(media: $media, commentId: $commentId) {
      media
  }
} 
`

export default CREATE_COMMENT_MEDIA_MUTATION;