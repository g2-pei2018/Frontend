import gql from 'graphql-tag'

export const CREATE_COMMENT_POST_MUTATION = gql`
mutation CreatePostCommentMutation($comment: String!, $upvotes: Int!, $downvotes: Int!, $postId: Int!) {
    createComment(comment: $comment, upvotes: $upvotes, downvotes: $downvotes, postId: $postId) {
        id,
        upvotes,
        downvotes,
  	    comment,
    	user{
          username,
          avatar
        },
    	insertedAt,
    	post{
          id
        }
    }
} 
`

export default CREATE_COMMENT_POST_MUTATION;