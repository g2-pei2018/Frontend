import gql from 'graphql-tag'

export const CREATE_POST_MEDIA_MUTATION = gql`
mutation CreatePostMediaMutation($media: String!, $postId: Int!) {
  createPostMedia(media: $media, postId: $postId) {
      media
  }
} 
`

export default CREATE_POST_MEDIA_MUTATION;