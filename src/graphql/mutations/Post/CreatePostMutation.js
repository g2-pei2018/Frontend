import gql from 'graphql-tag'

const CREATE_POST_MUTATION = gql`
  mutation CreatePostMutation($title: String!, $body: String!, $upvotes: Int, $downvotes: Int) {
    createPost(
      title: $title,
      body: $body,
      upvotes: $upvotes,
      downvotes: $downvotes
    ) {
      id
      insertedAt
      title
      body
      upvotes
      downvotes
      user {
        id
        username
      }
    }
  }
`

export default CREATE_POST_MUTATION;