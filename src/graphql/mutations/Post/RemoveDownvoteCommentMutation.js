import gql from 'graphql-tag'

export const ADD_DOWNVOTE_POST = gql`
mutation RemoveDownvoteCommentMutation($id:Int!){
  removeDownvoteComment(id:$id){
    result
  }
}

`

export default ADD_DOWNVOTE_POST;