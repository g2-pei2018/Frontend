import gql from 'graphql-tag'

export const REMOVE_DOWNVOTE_POST = gql`
mutation RemoveDownvotePostMutation($id:Int!){
  removeDownvotePost(id:$id){
    result
  }
}
`

export default REMOVE_DOWNVOTE_POST;