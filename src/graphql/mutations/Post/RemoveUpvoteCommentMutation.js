import gql from 'graphql-tag'

export const REMOVE_UPVOTE_COMMENT = gql`
mutation RemoveUpvoteCommentMutation($id:Int!){
  removeUpvoteComment(id:$id){
    result
  }
}
`

export default REMOVE_UPVOTE_COMMENT;