import gql from 'graphql-tag'

export const REMOVE_UPVOTE_POST = gql`
mutation RemoveUpvotePostMutation($id:Int!){
  removeUpvotePost(id:$id){
    result
  }
}
`

export default REMOVE_UPVOTE_POST;