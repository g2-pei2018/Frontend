import gql from 'graphql-tag'

export const USER_HAS_VOTED_COMMENT = gql`
mutation UserHasVotedCommentMutation($id:Int!){
  userHasVotedComment(id: $id)
}
`

export default USER_HAS_VOTED_COMMENT;