import gql from 'graphql-tag'

export const USER_HAS_VOTED_POST = gql`
mutation UserHasVotedPostMutation($id:Int!){
  userHasVotedPost(id: $id)
}
`

export default USER_HAS_VOTED_POST;