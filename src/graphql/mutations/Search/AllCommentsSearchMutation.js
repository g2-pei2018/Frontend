import gql from 'graphql-tag'

// Args like this

// {
//   "wildcard": "%l%"
// }

export const ALL_COMMENTS_SEARCH_MUTATION = gql`
mutation SearchCommentMutation($wildcard: String!) {
    searchComments(wildcard: $wildcard) {
        id,
    	comment
    	upvotes,
        downvotes,
        insertedAt,
    	user{
          username,
          avatar
      }
    }
  }
`



export default ALL_COMMENTS_SEARCH_MUTATION;