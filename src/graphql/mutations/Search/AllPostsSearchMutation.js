import gql from 'graphql-tag'

// Args like this

// {
//   "wildcard": "%l%"
// }

export const ALL_POSTS_SEARCH_MUTATION = gql`
mutation SearchPostMutation($wildcard: String!) {
  searchPosts(wildcard: $wildcard) {
    id,
    title,
    body,
    upvotes,
    downvotes,
    insertedAt,
    user{
      username,
      avatar
    }
    comments{
      comment,
      user{
        username,
        avatar
      }
    }
    
  }
}
`



export default ALL_POSTS_SEARCH_MUTATION;