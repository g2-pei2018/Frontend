import gql from 'graphql-tag'

const ADD_EV_MUTATION = gql`
mutation addEvToUser($id:ID!,$savings: Float,$consumption: Float, $name:String) {
    addEvToUser(id:$id,
    stats:{savings:$savings,consumption:$consumption, name:$name}){
      savings,
      consumption,
      name
    }
  }
`

export default ADD_EV_MUTATION;