import gql from 'graphql-tag'

const REMOVE_EV_MUTATION = gql`
mutation RemoveEvMutation($id:Int!){
    deleteEvToUser(id:$id){id}
    }
`

export default REMOVE_EV_MUTATION;