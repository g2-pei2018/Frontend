import gql from 'graphql-tag'

const UPDATE_USER_MUTATION = gql`
mutation UpdateUserMutation($name: String, $password: String) {
  updateUser(
      user:{
    name: $name, 
    password: $password}
  ) {
    id
  }
}
`

export default UPDATE_USER_MUTATION;