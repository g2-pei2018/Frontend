import gql from 'graphql-tag'

export const GET_EVS_QUERY = gql`
  query UserEvsQuery{listCars{
        id,
        avatar,
        brand,
        model,
        chargingStandard,
        year
      }
    }

`

export default GET_EVS_QUERY;