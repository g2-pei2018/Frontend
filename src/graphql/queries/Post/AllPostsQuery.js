import gql from 'graphql-tag'

export const ALL_POSTS_QUERY_2 = gql`
  query AllPostsQuery {allPosts{
    id,
    title,
    body,
    downvotes,
    upvotes,
    insertedAt,
    user {
      username,
      avatar,
      insertedAt
    },
    medias {
      media
    },
    comments{
      id,
      comment,
      upvotes,
      downvotes,
      insertedAt,
      user{
        username,
        avatar
      },
      medias {
        media
      }
    }
   }
  }
`

export default ALL_POSTS_QUERY_2;