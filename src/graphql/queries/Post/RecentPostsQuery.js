import gql from 'graphql-tag'

export const RECENT_POSTS_QUERY_2 = gql`
  query RecentPostsQuery {recentPosts{
    id,
    title,
    body,
    downvotes,
    upvotes,
    insertedAt,
    user {
      username,
      avatar
    }
   }
  }
`

export default RECENT_POSTS_QUERY_2;