import gql from 'graphql-tag'

export const USER_EVS_QUERY = gql`
  query UserEvsQuery{getEvsFromUser{
    ev {
      avatar,
      brand,
      chargingStandard,
      maxCharge,
      model,
      year
    },
    id,
    consumption,
    savings,
    name
      }
    }

`

export default USER_EVS_QUERY;