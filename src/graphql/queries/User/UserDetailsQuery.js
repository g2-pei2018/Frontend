import gql from 'graphql-tag'

export const USER_DETAILS_QUERY = gql`
  query UserDetailsQuery {getCurrentUser{
    id,
    name,
    username,
    email
  }}
`


export default USER_DETAILS_QUERY;