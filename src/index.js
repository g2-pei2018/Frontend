import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { ApolloLink } from 'apollo-client-preset'
// import { ApolloLink, split } from 'apollo-client-preset'
// import { WebSocketLink } from 'apollo-link-ws'
// import { getMainDefinition } from 'apollo-utilities'
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import './styles/index.css'
import App from './components/App'
import registerServiceWorker from './registerServiceWorker'
import { GC_AUTH_TOKEN } from './components/Authentication/constants'
import './styles/semantic-ui-css/semantic.min.css'

// 2
const httpLink = new HttpLink({ uri: 'https://wattcharger-api.herokuapp.com/api' })

// 3
// const client = new ApolloClient({
//   link: httpLink,
//   cache: new InMemoryCache()
// })

const middlewareAuthLink = new ApolloLink((operation, forward) => {
  const token = localStorage.getItem(GC_AUTH_TOKEN)
  const authorizationHeader = token ? `Bearer ${token}` : null;
  operation.setContext({
    headers: {
      authorization: authorizationHeader
    }
  })
  return forward(operation)
})

const httpLinkWithAuthToken = middlewareAuthLink.concat(httpLink)


const client = new ApolloClient({
  link: httpLinkWithAuthToken,
  cache: new InMemoryCache()
})


// const wsLink = new WebSocketLink({
//   uri: `wss://subscriptions.graph.cool/v1/cjcgfwqx03z5v0110asrhwn95`,
//   options: {
//     reconnect: true,
//     connectionParams: {
//       authToken: localStorage.getItem(GC_AUTH_TOKEN),
//     }
//   }
// })

// const link = split(
//   ({ query }) => {
//     const { kind, operation } = getMainDefinition(query)
//     return kind === 'OperationDefinition' && operation === 'subscription'
//   },
//   wsLink,
//   httpLinkWithAuthToken,
// )

// const client = new ApolloClient({
//   link,
//   cache: new InMemoryCache()
// })

// 4
// ReactDOM.render(
//   <ApolloProvider client={client}>
//     <App />
//   </ApolloProvider>
//   , document.getElementById('root')
// )

ReactDOM.render(
  <BrowserRouter>
    <ApolloProvider client={client}>
      <App style={{height:"100%"}} />
    </ApolloProvider>
  </BrowserRouter>
  , document.getElementById('root')
)


registerServiceWorker()