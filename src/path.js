"path" : [{
    "lat" : 41.54550649999999,
    "lng" : -8.425875
 },                     {
                        "lat" : 41.5445497,
                        "lng" : -8.4258229
                     },
                     {
                        "lat" : 41.5445497,
                        "lng" : -8.4258229
                     },
                     {
                        "lat" : 41.5429956,
                        "lng" : -8.430073799999999
                     } ,
                     {
                        "lat" : 41.5429956,
                        "lng" : -8.430073799999999
                     },
                     {
                        "lat" : 41.5395501,
                        "lng" : -8.4335284
                     },
                     {
                        "lat" : 41.5395501,
                        "lng" : -8.4335284
                     },
                     {
                        "lat" : 41.53477280000001,
                        "lng" : -8.437436699999999
                     },
                     {
                        "lat" : 41.53477280000001,
                        "lng" : -8.437436699999999
                     },
                     {
                        "lat" : 41.5350547,
                        "lng" : -8.4397295
                     },
                     {
                        "lat" : 41.5350547,
                        "lng" : -8.4397295
                     },
                     {
                        "lat" : 41.5349502,
                        "lng" : -8.4450459
                     },
                     {
                        "lat" : 41.5349502,
                        "lng" : -8.4450459
                     },
                     {
                        "lat" : 41.5349064,
                        "lng" : -8.446148899999999
                     },
                     {
                        "lat" : 41.5349064,
                        "lng" : -8.446148899999999
                     },
                     {
                        "lat" : 41.535245,
                        "lng" : -8.446173399999999
                     },
                     {
                        "lat" : 41.535245,
                        "lng" : -8.446173399999999
                     },
                     {
                        "lat" : 41.5100775,
                        "lng" : -8.4447417
                     },
                     
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "1.1 km",
                        "value" : 1108
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 67
                     },
                     "end_location" : {
                        "lat" : 41.5027406,
                        "lng" : -8.440446099999999
                     },
                     "html_instructions" : "Take the \u003cb\u003eA11\u003c/b\u003e exit toward \u003cb\u003eGuimarães\u003c/b\u003e/\u003cb\u003eVila Real\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "maneuver" : "ramp-right",
                     "polyline" : {
                        "points" : "_mj|Frjpr@TLDBHDPFPDNDNHHDFDDFFFBDFFHLBDDHDJ@FHVFZH`@F^J^DLDJBBDFHNFFHHHBFBDBF@D@F?F?F?FANAVGbAQzB_@\\IVKl@WRKj@]f@a@TWTWRWNU`@q@`@u@pBsDz@}AbB{CzAiC\\o@Xg@Rc@~@mB^y@"
                     },
                     "start_location" : {
                        "lat" : 41.5100775,
                        "lng" : -8.4447417
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "13.5 km",
                        "value" : 13504
                     },
                     "duration" : {
                        "text" : "7 mins",
                        "value" : 447
                     },
                     "end_location" : {
                        "lat" : 41.43172,
                        "lng" : -8.336782399999999
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eA11\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "polyline" : {
                        "points" : "c_i|Fxoor@n@eBVs@N_@d@sATu@^eATw@d@kBf@wBd@_C\\wBRyAVoBFe@PqBNmBHoADaABi@FeADiAH}B?GXoJDw@B_@HmB@SFw@Fm@Dc@B[D_@Hq@BQTwALs@F]ZuANo@Lg@Ni@DORm@Ng@`@eAZu@^y@\\u@N]f@aAp@qAHOJU@CJSxFeL|BqExBkEpDkH|@eBvDsHP]@CR_@v@_BN[d@{@JUJS`EgIb@{@h@}@NWRYLSRWb@k@j@o@d@g@XWd@a@f@_@VSTOl@[f@YZO`DyAn@[l@WdAe@dAe@zAq@fAe@f@Ux@_@`Ai@|@i@t@e@j@c@~@y@z@}@\\a@Z_@p@aAb@s@^s@^u@Zs@Xs@Z}@Z_A\\sAT_ANw@Hg@TuALeAFq@Fm@Fm@d@_FXcDL_BNaBL}AN_BJaAB_@PwANkAN_ALo@RcA\\qAVy@\\aATm@Zu@`@y@\\o@`@q@V_@PWb@m@`@e@LMLOh@i@JKXWh@c@z@m@f@]JG^SdAk@~BwAv@i@f@_@n@i@`@c@XWRUb@g@PUV]`@o@`@o@\\o@l@oA\\w@JWVy@`@uARw@H]F_@F[D[De@DUBWFk@J_ALwABe@F}@DcABcABcABaADcA@i@D{@D_AHaALeAL{@N_AR_AR}@T{@T{@Vy@Xw@Zu@Zu@^s@^q@^o@b@m@PURUPWRSRUZ[JIJKd@a@ROTSx@k@|@m@j@a@b@[`@]n@k@t@u@`@g@X]f@u@\\i@Ze@b@}@^w@Vm@^eADKLe@Ne@f@gB\\yA`@}A\\wA^{AX}@Tu@X{@J_@X{@Xu@h@wA\\w@Xq@`@{@j@mA^u@^q@`@s@`@s@`@q@^m@LOd@u@d@o@`@i@f@m@f@m@f@k@NSd@g@d@g@|@{@~@{@pAeArAeAf@a@PMb@]\\W|@s@|@y@j@m@\\a@b@k@j@w@d@w@\\o@`@w@`@aAJSBG?CDK^gA`@sAT_ALq@Nu@\\qB@KP}AJqAL_CToE@YNkDJgCNcDD{ABa@FkBFaBJ}CDq@@UJ{AHcAH_ANcBD]RcBR}APcAHg@F[F[N}@P}@R_AT_Ah@wB`@}Az@qCd@uAt@uBdAiCZs@\\y@`AuBJWLWbA}Bp@yAdAyBdBmDhAsBlAqBv@oAX_@LQRWh@o@t@y@~@_AjAeAhA{@rA}@~A}@fAe@j@Wp@Wn@SZITGLErA[fAQj@Gp@G|@Gn@Cp@AZAjA?zADv@DhAJf@H\\FlCh@v@V|@X\\Lh@Rl@VFBHFJDHB@@HDJDJFHDn@^jBjAj@`@h@b@nCxBvAlANNtAlA~@x@pAjA|@v@TRTThA`A|@v@pAjAp@l@FF|@x@|@v@z@t@z@v@~@v@|@x@"
                     },
                     "start_location" : {
                        "lat" : 41.5027406,
                        "lng" : -8.440446099999999
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.7 km",
                        "value" : 735
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 37
                     },
                     "end_location" : {
                        "lat" : 41.4287849,
                        "lng" : -8.3336598
                     },
                     "html_instructions" : "Take exit \u003cb\u003e9\u003c/b\u003e for \u003cb\u003eA7\u003c/b\u003e toward \u003cb\u003eGuimarães Sul\u003c/b\u003e/\u003cb\u003eA11\u003c/b\u003e/\u003cb\u003eFelgueiras\u003c/b\u003e/\u003cb\u003eA24\u003c/b\u003e/\u003cb\u003eVila Real\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "maneuver" : "ramp-right",
                     "polyline" : {
                        "points" : "gc{{Fzg{q@HTBDDFVXVZJNFJJPHNJRHJFJFLDHBBFJHJFJHHHHDFHFHHJFHFJDTHLDJ@JBL?N?L?NCJCNCNEPGLGJIJGHIFIHKHMHKDKFODOFQDUDOBOBOBQ@Q@S?[AY?a@A]A]Cc@Ao@Cm@Ao@As@Ao@@u@@cA@_AN_A"
                     },
                     "start_location" : {
                        "lat" : 41.43172,
                        "lng" : -8.336782399999999
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "65.3 km",
                        "value" : 65281
                     },
                     "duration" : {
                        "text" : "34 mins",
                        "value" : 2064
                     },
                     "end_location" : {
                        "lat" : 41.5020629,
                        "lng" : -7.660159600000001
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eA7\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "polyline" : {
                        "points" : "{pz{Fjtzq@Fm@Dc@Hi@Hi@T{@Vu@P_@JWNYR[PWNUTWNQVUROTQl@_@nAm@f@M\\G\\Eh@EV?`@?p@BhBHx@H`@Bl@Dn@BX?X?PAT?PCn@ETEr@Kv@Uh@SVMTKRKfAw@n@g@h@g@VYPUPUpBoCbAcBv@iAv@gAnBkCV[NSJMXY\\]d@_@d@]^YPKTOXMVKj@Sv@Sn@OZGVEd@EVCLATAVA\\?t@?t@@dBDhA@r@?^?VAXAXCXCXCVEf@Id@KTGRG\\Mr@Wl@WVMXOTMVSf@_@h@g@Z[\\a@PSPUPWPW`@o@P]LWNYLYHSRg@J[J[Lc@J[H]Li@No@He@D[Fa@D]Dc@JaAFy@FiA@c@@g@@k@@i@Ae@Ac@AqAEcAGiAEc@E_@Im@c@{Bg@}B_@{Aw@aDg@{BYoAYmAe@{B}BiKS{@Q{@Qy@S{@S{@_@cBg@}Be@{BUeAYwACOKq@O_AE]Gc@C_@Gg@C_@C_@Cc@Ce@AYA_@Ag@?c@A_@@a@?eABy@@Q?SB_@Be@HaADa@Da@D]T_BPiAd@{B^}AH_@R{@h@_C\\_BHg@No@Lo@^uBLaAN_AFg@`@cDJeAJ_AJeAL}AHgADy@Dm@HyANiC@_@BcAPmF@g@D}@Bg@Ba@L}AFc@LcAFa@Fa@Ha@Hc@Nm@Ha@`@sBDQXy@Z{@Zs@\\w@N]NU`@s@b@s@b@q@bA_BRURYNWNY`@s@\\s@^y@N]L[J[Xy@Lc@\\wAH]H]F_@N}@Fa@Da@R{ADc@Fa@HeABa@@a@@c@@a@@a@?_@?e@?Y?c@AeBCcAAa@?g@?_A?gADiB@a@@[Dg@H_AJiAFa@DWNaAHc@Pu@Je@Vy@J[L]L]L[LY`@{@j@mAn@qAN[^{@N]Xy@L]V}@HYJa@Ha@Fa@Ny@Fe@D]Da@D_@Ba@Bq@Bu@@g@?W?a@AgAAc@CiAEcAOkBKwA]}CKcAOyAOgCIiCAeBDiBFgBFeAJcAJeAN_ANcAR_ATcA^wAb@{Ad@{A`AcDFQDM@EHY@Ed@_Bt@cC\\aA@GFONk@BGPk@d@}AdAqD~AwEtB_G~@eCVo@n@kBr@}Bt@gCh@gB`@qATeALo@Lq@NiALy@Hs@Du@Be@B{@DcADaA@w@B_A?{@@}@Aw@Ao@A_ACq@E{@Es@IiAGu@OcAMeAGa@Kg@Kg@Mc@Og@Oi@Qi@[{@_@w@_@u@a@w@c@s@y@eAi@m@mAuAoBiBuAsA}CcDIGCEMKaLqLMMAAMMcCiCmAuAUW]e@CEo@y@e@u@OYIMACEIg@aAk@qAg@mAe@wAc@yA]wAe@aCU}AY}BMaBCm@Ey@Ac@A_@A_@?[Ag@?a@?}@BgABaADaADaABc@B_@FaAD_AFeAF_ADcAHaAFcAFcAH_BHcBJcBHcBD_A@a@Ba@@i@@{@@cA?_@?cAAc@A_@Ac@E_ACe@Aa@Ca@Ea@E_@Ga@G_@G]I_@I_@K[K][u@O]]s@_@s@a@u@MWQ[QWSUe@i@i@i@i@g@i@c@m@c@m@]UM_@SOIUKk@UYIUIYIYGm@M]Eo@IcBOiAIs@Gq@Eo@G]CWEYCWEYGWEWGWGYIUIWIWKWKUKYKUMUMUOUOSOSQUQQQSSSQUSSSUQSSg@e@e@g@g@i@i@k@c@k@QUc@g@w@aAQUu@}@g@k@y@}@{@aAaBeBOQOOY]kAsAy@aAy@aAu@{@i@k@e@k@QUQWQWQW]m@OYMUO]]s@Ys@[w@Yw@Wy@Sy@W_ASy@Mi@Mq@Ga@E]ESUgCC_@C_@AUEmAEmAAy@CaAAaAEeCCcAE_AEaAC_@GcAC]Gc@CYGc@O{@I_@G]S_AG]Ma@Uy@Us@M[[w@Q_@O]IMGMWg@QWOW[c@IMAAKMSYQQSWe@c@IIKKQOSOUSWOi@]c@WUM[OQIg@Se@QUG[IWGYGWGo@Kq@KcBUaBSgASWGq@OGCe@OYKa@O_@SYMUOm@_@CCe@[i@c@USe@c@c@g@OQSUQW[c@_@k@_@q@[m@O]Yq@Si@Qe@Og@Uw@I]Ka@Q{@O}@Kw@Ec@McAIaAGy@GcAIeAG{@IiAI}@KiAS}AIg@ESEUEWMs@S}@UaASu@Sy@]kAeAaE[kA[kAMc@iAkEg@eBk@eBKYM[Oa@[u@O[MWKUSa@_@o@]k@a@o@_@o@QYa@m@_@m@MSMSOU_@m@_@k@c@s@[g@Ua@OUQ[OWKSQ[_@y@O[O]Ws@g@{AI[I]I]I]Q}@Q}@Gc@K}@Gc@Ec@Ea@Ec@Cc@GcACe@EmBAgA@c@@i@@_AFqAFiAHq@H_AJu@Hk@VyALo@^{ARs@J_@L_@BGFQPe@JSFON[r@uA`@q@f@s@d@i@z@aABC|AeB~AeBbBiB`BeBpAsA|@_Ad@i@f@k@rBsC|@aB`@}@h@oAXw@X{@Lc@@Ed@aBXoAToATuAHm@Hu@Fo@Dk@FkABeA@sA@_A?U?]C}@KkCGcAG{@YaCWaBY}A]wAc@}A_@uAWy@U{@IYmEcOeAsD}D_NgAwDy@yCk@wBg@yB]{A[}AWuAUuAq@yEYoB]eCc@eDW{BWmCo@eGeBeOa@{Da@aEs@qGi@gEa@{Dk@sFg@sE{AsNqAqLs@cGm@cF]gD[_D_@gDYgDOcBGeAEcAEu@Au@Cs@?w@Au@?iABiBFeCHcCJgBHiAFy@NeBN}ADW?CD]n@yFP_BH{@FgAHgBDgABeB@aB?iBCcBGwBAc@?CCi@I_BIeAMeBWcCY_CYaBc@{Bg@wBu@wCu@wCm@yBi@wBeHwXe@sB[wAWyAU{AM}@MgAO_BK}AGaBGeBCcC@cC@gBD}AJgBFcAFaAF{@Fc@L_APeAV}AL}@LcAV{ATcBNcBPmCD_A@g@?gA?iACaDCcAGeBIaBO}A[}B[}A]wAq@yBu@oBm@wA_@y@gDmI[u@m@kA_@s@]m@]q@o@kA]s@k@sAw@mBo@wAy@kBYs@IUa@aAwBuEWo@m@uAw@gBWk@Sm@[_Ac@eBQq@UiAIi@Ik@Q_BImAScEIoAQkBIq@Gi@SsAO{@G]Kc@Mi@g@kBU_AMg@Ma@IUISKc@a@kBGWOe@M]Qa@]i@QUe@k@QSUUYUSMSMsAm@{@a@wAs@cAk@aAk@c@YaAu@g@g@e@g@_@g@i@q@{@gAk@{@U]c@s@MWQ[O[KYO_@Qg@CGISMc@Oa@e@gBK]g@aBKa@I[Ia@G_@G_@Qy@G_@My@Ky@Ec@Go@Gy@UqDQaCSgCKiACMCYEe@[eDc@qFa@eFIs@UcCe@kESsBUmCKiBCc@CWGq@G_@EQGe@I}@MaBYgDOgBSeCGcAG}@[kDSkCI_ACWOiAQ{AGg@O{@a@yB[wAk@gCYoAWeAWeAs@gCe@cBSq@g@{Ae@uAe@wAg@wAe@_BYu@Oa@[w@]u@[u@Yu@_@{@g@oA]w@]u@e@qA]y@k@sAe@mAc@uAa@{ASy@[}AO}@MeAQaBM_BMeBG_CCcC@mCBmA@i@?_A@WFmC@{AAe@KyBIcAE]I_AS}AEa@]_CKi@E]m@}D]uBOqAKyAEeA?k@?oBBwCF}BHwAJ{ATyBNqAN_APaAHk@ToBNaBBa@DeABaA@c@AgAE_CIgCIaBSgBYaCUgBSmBKk@Ic@_@uAM]Qm@Si@_@{@k@oA_AkBs@uAoAcCi@}@k@_A[e@U_@o@sA]w@i@wAY{@c@uAYoAq@oC_@oBOkAM}@MgAKcBKkBCsACoA?eA?u@BcB@eAH_CHgBLaBTyBR}Ab@gCf@oCtAqGbBqIBKp@}CPy@j@iCx@wDBOR}@F]FUHa@~@oE~CmO^yBL{@Hk@LgAHgALoBDyABkBA{AE_AEu@KgAMcA]qCc@mCo@uCk@eC_@oAm@_Bk@iAa@s@_@k@w@iA_AkAe@g@Y[Y[{BgC{A_By@}@{DkE{AaB{@y@{BsBu@u@}@eAs@}@cAyAu@kAaAkBa@{@Oa@Qa@i@yAm@mBOq@UeAm@eDOwAQ_BOmBMiCCcACaA?oA@eBD_BHcBFcAF{@v@eIFk@Fm@JcALmABUP_BPaB^yDRkBRiBNyAHs@JaAZ}C@C`@_EDe@@GFk@`@cE`@oDFo@JgAPoBJkALkADa@B]HcAFaAFcAD_ABaA@{@@}@?_@?}@A_@Ae@Aa@G_ACc@Eo@Eo@I}@Ea@Iy@KeAC]G_AGu@Cm@Cs@AUAa@Ai@?a@?[?i@?_@@c@?Y@[@a@@a@Ba@Be@BYBe@D_@B]BWD_@F]BUBODYD_@Hg@F_@FYF[Je@J]H]H_@J]H[HWLa@V{@Na@Vs@v@uB\\}@DKFOXy@L]J]J[H[Po@Lc@DSJc@DWFc@L{@@CHq@?AFe@@UB[B]@]Bu@@_@?_@?_@?]?]Aa@Aa@A_@A_@A_@C]C]E_@C]E_@G_@E[E[OaAKs@O{@Ge@Gg@C[E_@Ee@Ca@Ca@Cc@Ac@A]?g@@aBDsA@S@KB]Dk@BSH_AFw@BUFq@Fy@@W@M@[Bk@By@BiA@a@?[?g@?_@?_@?]Ao@AWAe@A_@Aa@A]Em@Ey@IeAEYAOQsACSIk@Kk@Kg@YuAMi@Mm@K[U}@K_@Ma@IYM_@K[M[M][w@O[]w@m@mA]m@a@o@SYQUc@k@kAwAQSw@u@YUQOSQi@_@UO}@g@k@YcAe@o@YcAc@iCgAWMUKYOWMSMWOSOSOSOWSSOSSSQc@c@WWQSSY_@i@SYa@q@[k@S]Wk@Sc@Um@Qg@KYI[KYK_@I]G]I[Ie@EUEYGc@G]E_@KaAC_@C[Gq@Ew@C}@Ac@?]AaA?aAAeA?aAA]?_@Ac@A_@A_@C_@C_@Cc@E]Ea@E_@E[Ga@G[Ic@G[G_@I]Ss@K_@K]KYKYM]Qi@Si@Uu@M]Ma@Uu@K]Qy@Ka@G_@G]I]E_@G]Ec@Iq@Ek@Ca@C_@Aa@A]CcA?c@Aa@?a@?YA_A?_BAc@A_A?c@A[Ac@A_@Ae@A_@C_@C_@Ca@E_@CYKeAGm@Iq@O}@Ga@Qy@Q_AS{@_@sAI[Ma@e@oAO]KWM[MWk@kAkA_Ck@kA[s@M[M]Ma@KYMc@K_@Uw@K]I]Ia@I]Ke@O}@G_@G_@Ga@G]E]Ec@E]KeAEq@Ek@Em@Cm@Ac@A{@Aa@?u@?w@@c@@_@Bo@Bk@DaABc@JyBBe@@Y@c@@]@]?e@?]?]Aa@Aa@C]Ca@Ce@Ca@E_@AGCWGa@E_@Ge@Ie@G_@G[Mi@Kc@I[Uy@U_A]yASaAO{@E]Ga@I{@Ca@AYCa@Ck@Ao@CkBC}AC}@C_@A]Cg@E_@A[K_AGe@CYG_@Ge@M{@Ia@M{@Kk@Q_AO_AKo@Gg@Ea@E_@C[Cc@C]Aa@A_@?a@?_@?cA@]@e@@a@@a@DaAB}@DaB@]?a@?c@?_@A]?[Cc@Aa@A_@C[Cc@E]C]Ea@G_@E_@G_@Ga@GYG_@Ka@G[GYs@yC[{AWyAMcAKgAGs@KaBGcBG}B]sKA_@A][{K]oLYkJAUAU?AAWQ{DMkBG}@E_@Gq@MiAOcASiAm@yCWaAESGQOm@Oe@Us@[}@Ys@Ys@i@qAoD}HuA_D{@mByBaFiBgEgC}FMYMY]y@_@cAWy@W{@K]Kc@I_@OY[cBMeAMmA?I?AIiAQsCA}@?kA@aABcAD}BDqAJ_CRoDLuBLcBr@_NHsAHcBH{D@mC@iA@uA@gBDwBHcFDiAFaADaABg@NmBRwBHw@Hk@Hk@Ji@Ns@Ls@ZeB^iBRcALo@Jk@Ny@VoBZ{B\\eDNwBNmCF{BByC?eAAcAE}AEmAEiAKwAMyAKmAUeBMu@Mu@g@kCk@gCo@cCYcAOe@Oe@Uq@Oe@Uk@eAiCq@yAq@eAoAeCeAsBo@uAc@_Aa@}@i@kAgAgCmAgCeA}Bg@kAg@kAg@sAm@_Bk@_BOa@Ss@YiAYuAIe@OcAIe@MaAKaAKeAIeACc@Cc@G{@EcBCiBAgB@kABcBFaBFiAF{@F{@JeAJ{@Ls@\\aB`@yBTcAPw@h@yBf@wBRy@^yAh@wBl@wCf@yBv@qD\\{A^{A\\{AfEoRVkAZ}AXeBZuBVwBb@kELqBBeALiDDqABgBBgB?eA?gAAe@CuBAg@CmAAmACs@AcAEiBEcBCeAAkBCiB@eABqBBy@DmADs@HqAHw@H{@L_AL}@F[Hg@F]P{@Ja@Ry@V}@J]Vw@Z{@Zw@j@qAP]Pa@j@cAb@s@T]JO`@m@b@k@b@i@b@e@TURUh@e@f@e@VQb@]^WLIj@]h@YXOn@Yj@WdAa@fFiBnBs@nDuAl@Ul@Wl@Yj@YZQd@YTQTOTOh@c@|@y@z@_Al@o@LUd@k@r@iAp@iA`@w@j@qAd@mAp@qBj@yB`@qBd@mCTuBNqBFqAF}BBsBCkBGkBGcAEi@SuBS_B_@_CeAwFe@}Bk@{CiAyFmAoGw@aE_A}EiAyFoAyG_AyEmAqGAKAEACCM[eBc@}BIe@AAAGMo@uAuHm@{Cm@sCGYAEI_@e@}B_@{A_@{Am@wB_@gAi@sAcAsBcAaBkAyAoAqAm@e@e@]m@c@qBkA}CgBw@i@q@i@AAo@o@i@m@g@q@MOUa@]m@q@{Ak@}A"
                     },
                     "start_location" : {
                        "lat" : 41.4287849,
                        "lng" : -8.3336598
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "45.6 km",
                        "value" : 45550
                     },
                     "duration" : {
                        "text" : "23 mins",
                        "value" : 1353
                     },
                     "end_location" : {
                        "lat" : 41.81444159999999,
                        "lng" : -7.4142337
                     },
                     "html_instructions" : "Take the exit on the \u003cb\u003eleft\u003c/b\u003e onto \u003cb\u003eA24\u003c/b\u003e toward \u003cb\u003eChaves\u003c/b\u003e/\u003cb\u003eEspanha\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003ePartial toll road\u003c/div\u003e\u003cdiv style=\"font-size:0.9em\"\u003eEntering Spain\u003c/div\u003e",
                     "maneuver" : "ramp-left",
                     "polyline" : {
                        "points" : "{zh|F~bwm@M]Um@]gA_AgCs@qBs@sBe@uAe@sAMa@Ok@Oi@Yw@M]Wi@[i@a@i@UYOOIIGIUQ_Ag@u@QcAKk@@W@K?K@MBK@WFWHWHWLULWPIF[Xa@f@IJILGLILGLILIJGLGHEJGHGHEHGHEHGHGHGHGHIHGHGHIFGHMHKJKHMHKHMFMHKFMFMFMDMFMDMDMBMDqBXI@c@TS@UBS@U@S@U?S@U@W?Y@W?W?W?YAW?WAUAUAUCUCUCSCUEUEWEWGWGWIWIWIWIUK]Q]Q[Q]S[S[S[U[UIIIGKIIGIIIIKIIGCEECCCECCEECCCEEGGGIGIGGGIEIGIGIEEEGCEEGEGCEEGCGGIQY_@q@MWO]GMKWKUQe@Si@Qi@So@U{@U}@S}@UgAWuAc@_Cc@_CMk@Os@Q_AI_@I]Me@Sw@Y_AY}@Oe@Uq@Yu@]w@]u@_@w@c@u@c@w@QWQUmA_BSWUWSUSUUSUSSQUQUQUMSMWO{Ay@SIWMq@[o@Ws@UeA]}Ai@g@QUI[Ks@SyBi@SG[IUKWIWKWMWMSKWMWOUQUOUOa@]WUSQg@e@g@k@QSc@k@QSa@m@OWa@w@_@w@a@{@[y@k@uA{@yBm@wAk@sAQc@Yk@]w@a@u@_@s@e@y@a@m@c@q@Ya@_@e@UWSWi@m@YWa@a@WQSSUOSOUOi@[WMUMk@WUKo@Wq@WgAa@o@U[Ks@Uq@SiBe@q@Qq@QYIYKc@MwAg@o@WYMUMWOYQm@_@k@c@WSsAcAy@w@}CwC_A_Ai@e@SOYWWQUOUOUOUMYMWMWMUIWKYKo@QYIYIq@O[GWG[Eq@I[Es@Gs@C[As@?Y@kAFs@DcBFoABo@@q@@s@?mAAWASA_@Cq@Es@GiAMo@IgB[oA[}Bm@}Bm@yCu@}B_@sCg@kAU}Ba@kBU}BQaAAqBHu@HiAN[Do@Lu@PgA\\m@Vq@ZgAj@wA|@k@b@_Az@q@t@_AlAw@jAg@t@k@hA_@z@o@|AiBxD_AhBaAjBgBpCw@dA{@dAi@f@_At@k@\\iAh@gA`@kA`@eA`@]Ni@Xk@d@m@f@q@p@o@v@kA~AoBnCe@h@i@h@SNYTk@Zu@`@e@POFe@LaB`@_B^k@NaAT{Af@UJs@Xs@\\aAh@kAt@w@l@kCtBaAz@_Av@{@l@cAp@m@XULULi@TgA`@kAZyAViALkBHcA?kAE{@GgAQcAQaAWw@Yw@YqBy@w@][Q}Aq@eBw@YMqAm@[O[OyCuAyAs@KG[S[S_BiAcE}Cw@y@mAiAaCeCeAcAoE}DmE}CaAo@sBiAaEsBwEsBaAa@mD}AsBiAcAm@g@[{CaCi@c@YY}C{CwA}AeBgBsAoAqAkAu@o@q@g@w@i@{@i@}Ay@{B}@]MyAe@gBe@iASiAQ{@K_BMkAEuAAaB@oADgAD_BH}BNeDV_BH}BH}AAiACQASAmAKUCSCe@KkAWaBi@qAk@g@Ww@e@uAaASQu@u@u@u@{AqB[k@i@_A{@iBq@iBWy@Su@WmA_@qBc@_DOwAK{AEm@EaAEuBA{BFgCFoAB{@B{ADqCD_EBiBB}C?qACmAIiBK{AUoB]oBk@uBg@uAQ_@Q_@c@y@eAwA}@cAwAoAiAcAs@k@cA{@kAaAeAaAu@w@y@aAUWu@cAw@mAe@y@q@sAg@kAe@cAc@_AQ_@a@y@]_AYo@[s@Yi@g@kAk@mA_AkBo@aAk@s@w@y@s@u@m@i@aAy@eAq@eAk@w@a@o@Wu@Wi@Oo@Qm@M}@Oa@GcAKoAI}@AaA?a@?sABg@@aADy@BcBHmADg@@o@@yA@eAEg@C_AGg@GUCoASk@Mg@MeA[_A[oAi@qAo@i@[s@a@m@]g@_@uAcAcE_D_CiBwAcAuA}@{DcCsBqAqBmAi@]c@[i@a@YUUU[WeAiAcAqA[c@Ya@k@w@c@u@q@gA}@eBw@_BaAmBYk@{@}Ay@{AS]e@u@OU]e@k@s@e@i@w@{@aAcAoA}@i@_@iAs@o@]]Qs@[k@Sw@Uk@O{EcAyBg@aAUe@MiAYy@QSE{AYsAYiB_@mAYkCm@iAY}A[s@O}@OkB]qAWcBa@wA_@]KMEa@O_@Ms@]y@c@i@_@[SUQi@c@y@w@OQ_AcA]c@[a@k@y@c@u@[g@mAuB{@_BqAyBe@q@U[k@u@W[UWSQm@k@cA{@{@k@_@S]Se@W_@Oe@Qw@YuAc@s@Qm@Qo@OiBe@o@Qw@Uy@Wq@UaA_@{@]s@Yg@SgAi@i@Ye@UUMiGeE{CqBuAy@aAi@aB{@m@Ys@]o@Uu@Wm@Om@Mq@Km@Gq@Ea@Eg@AcACy@AaA@o@Bq@BQ@QBcAJqCXy@Hm@Bq@@[?s@Co@Ck@Ee@Ga@Gg@I]Ig@M]K[IYK[O[Ma@Qy@c@a@W]U_@WAAi@a@_Aw@EC]]e@e@_@_@m@m@c@g@uAuAm@k@cB_B{AqAo@g@k@a@s@_@YMaAc@[Ma@SaAe@k@Wc@Sg@U[Qa@SWO_@W_@Yy@s@_AcA]a@W[g@q@OWU_@We@k@gAUc@Q]Q]}@mB]s@]u@e@}@]q@Ue@a@q@_@m@[e@_@e@]a@g@i@a@a@UWWUc@_@c@_@c@[g@]iAq@yAu@m@WYMy@Wm@Qk@Mi@Ma@K]Ko@Q_@Mi@Sa@Oq@[_@Ok@Y_@Su@a@o@]{@g@_Ai@aAk@w@g@iAu@kA{@cAw@eA}@g@c@GGUSA?e@e@o@k@e@a@_@]_@]c@_@u@s@SQiA}@s@i@e@_@k@a@e@[q@e@i@YeAk@y@a@s@[eAg@sAk@m@Wg@Qu@Yw@YqBo@u@Sw@QMEoFkAeGsA}Cq@c@Ke@KmCk@}EeAcBYm@Ks@GYCWCq@Cq@CiAA_BBu@Bq@DmAJiAH_Ed@q@HiAHs@DgABY?kAAq@EgAKo@KYEWGYKMEGCGAm@WcAc@o@[cAm@m@_@m@c@u@o@i@g@e@g@w@_Aq@aA_@o@o@kAa@{@Ug@m@yAi@wAmDwJg@qA]u@m@iAq@sAcA}As@eAmAwAi@o@w@y@qAqAiAiAm@m@sDuDeGeGgMaM_CsB_Au@cAq@{AaAuAw@eAi@gAg@kCcAgA_@qCu@cBa@}A]sFcAoO_D_E}@{@SgA[y@Y{@[eCiAoBeAq@a@sByAkAaA_DsCcBaB_IqHgAeA_B{AUSkAkA{B_CWYgAoAe@k@w@eAw@cAo@}@{A_CcAgBa@u@]m@{A{CUc@oAyC{@uBkCsH}C{IaCaHgCiH}AkE{A}DeE{Jk@qAwCsGmAmCc@_A_@y@KWCGiAcCoBaE_AoBqEsIaAiB_@q@_@q@_B}Cc@w@k@gAk@eAcAoB[o@cAwBc@eA_@_AGSi@uAk@gBg@gBOe@u@sCcA{Dc@eBe@aBm@sBkAiDi@sAq@}A{@_Be@w@QYU_@SYc@m@SWs@{@k@m@]_@k@i@g@e@USm@g@}@o@eAo@qAu@sAo@iB_AuE_C{Au@{Aw@sBgAwA{@gAs@kAy@cAy@{@w@u@u@iAqAMSOQi@u@cA}A_@o@g@}@Sa@MY_@y@Wm@q@gB_@iAYcAU{@UcAOq@Q_AIa@WaBi@wD]gDSoBMmAEk@I{@W_Do@sI{@qKE[I_AKy@Ku@Ig@My@WuAUiAMi@Ka@[kA[gAc@sA[y@Yw@u@kBm@uAg@oAa@cAa@gAKYSo@Sq@YaAYmAOk@Mm@Ki@Ki@QaA]sBIe@UyAEUy@aFq@iEoDsTMw@W}Ac@aCe@{BSaA_@yAc@aBg@cB{@iCk@yAs@gB}@oB_AiBMW_AeB}BmE_AeB]s@i@oAg@oAg@uAc@uAU{@Sw@_@}AQ_AO{@Ga@G_@E]S}AOuAEo@IaAGaAGqAEwACeACgCGoGAgBAeDCiCA_EAgAA{BAk@MiEEmAWcC_@aCcA_E}@gCkBsDa@u@a@m@c@k@y@_ASUg@e@SQg@a@m@a@u@a@iBaASIUIqAk@YKa@Os@UgAYe@QiD_AuA]MEKGm@Ik@IoEgAeCo@oAUaCe@k@OqDc@k@EYA[Am@Aq@A_@@k@?q@Bo@DiAJu@J{Bb@mBf@]LoAb@w@\\aAd@k@ZYP[Pc@X]V"
                     },
                     "start_location" : {
                        "lat" : 41.5020629,
                        "lng" : -7.660159600000001
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "11.6 km",
                        "value" : 11558
                     },
                     "duration" : {
                        "text" : "6 mins",
                        "value" : 350
                     },
                     "end_location" : {
                        "lat" : 41.9071443,
                        "lng" : -7.4308258
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eA-75\u003c/b\u003e",
                     "polyline" : {
                        "points" : "g{e~F|agl@_DzBcGdEeCxA_@TULGD_@T{Az@}Ax@eB|@}An@aCfAiBp@aC|@}C|@uAb@kA\\iAZiD|@{Cp@uBd@qGpAuNzBaBLoBNW@kBL_FVW?M?M?o@F_BAyAEiAIsDg@SCC?UCcEqAsCgA{Aq@_@Q_@Y}@e@cAk@y@k@{@s@c@]m@g@_Ay@}AaBSSeAqA]c@?AaB{Bs@cAq@mA{@cBi@_Ao@cAg@q@y@eAi@k@y@w@g@c@q@i@u@g@u@a@q@[UMy@Ys@UUGMCQGiBYa@C_@EkAEcA@{@@y@Hw@LmAR}A\\uAb@]L[Nc@Rq@\\kAr@aAr@aAv@y@v@c@f@[^o@|@]h@OX[j@Yl@Wl@[t@s@tBu@lCwA|Fg@`B_@lAW`Aa@`AmAfCo@nAeBnCuAlBcBpBi@n@WV]\\w@h@u@d@iCvAiBx@uClAoAj@kCjAiB~@iBx@cAf@oAn@aAj@i@\\i@^_CjBeA|@qAjA}@|@mBpBqAxAcAjA_AhA_AbAe@d@m@h@g@`@}@p@OHA@SLULeAj@u@\\i@Ro@R]HYHk@LaANu@Hy@FuAFmA@{@AiAEkAKa@Is@Ms@S_A]aAc@y@c@m@_@YSECGEy@m@WQOMMK]YgCqBs@i@y@i@oAs@uAq@}@[mBo@qA]yBe@sBe@gFuAqA_@eBc@SGQEIAqYaHyCu@oCu@_D}@mA[gH}AeD{@qBg@iB_@qB_@qASA?]EGASEcBOg@GkAC[@[@_B?uAB{@B{AJoDd@gBZcB\\_Ct@qAf@kBx@cD|AiBbAg@Zy@j@_Ar@wChCiE`D}@r@}ArAgErDeAv@m@`@i@ZoAn@YL"
                     },
                     "start_location" : {
                        "lat" : 41.81444159999999,
                        "lng" : -7.4142337
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "161 km",
                        "value" : 160801
                     },
                     "duration" : {
                        "text" : "1 hour 23 mins",
                        "value" : 4974
                     },
                     "end_location" : {
                        "lat" : 42.0649145,
                        "lng" : -5.6661739
                     },
                     "html_instructions" : "Take exit \u003cb\u003e1\u003c/b\u003e to merge onto \u003cb\u003eA-52\u003c/b\u003e toward \u003cb\u003eBenavente\u003c/b\u003e/\u003cb\u003eMadrid\u003c/b\u003e",
                     "maneuver" : "ramp-right",
                     "polyline" : {
                        "points" : "s~w~Ftijl@w@PgANSBI@O@W@O?_@?]?e@Ca@CMAQAIAe@ISEe@I_@Kc@MWK_@OQIKEWMSMQMMIk@c@i@e@_@a@KKY]e@q@QYU_@S]Sa@]w@e@oAa@gAUo@CCOQgAqCcAgCoAsCi@sAyBcFwPe`@MYMWa@cAaBwDiBgEq@{AmAkC_BsCs@eAo@}@e@m@[_@c@g@QQOQMM}@{@{@w@e@]k@c@y@q@_@Q_@SIEQKA?SKqAq@eAe@yBs@wA]wA[y@Qq@M{@QiAYaAYs@Yq@W_Aa@i@]iAq@eAu@{@s@w@u@k@m@cAmAo@{@y@oA[k@U_@e@y@iAeCcB{EK]a@iAiAyCy@iBm@kAc@s@i@}@U_@u@eAsBcCqAmAy@s@kA{@c@Ye@[k@]q@]{@_@_EeBu@[OGA?QIoH}CiFsBqAm@aAi@s@c@u@k@}@u@q@s@UYg@o@i@u@}@uAe@{@Ug@Ug@g@sAOa@Og@[kAOe@]}AUcAo@kDq@yD_A_Gu@eEEOYyAg@yCWaBa@iCs@sEKq@[oBYuAYkA[iA_@iAa@iA]u@Ug@[o@y@yA{@sAiAaBw@mA[i@_@q@OWa@u@]y@]u@c@eA]{@_@qAe@aB[mAc@wBWaBKo@QoAQ_BKkAEm@Ei@GuAGsAAy@A}@?cA@_DDgAFyADaANwBXcCp@{Eb@iCF[^}BXcCNiBL}AHsADiAB_BB_B?iBEoBEeBKqBWkEUiECg@A[CqAAq@?{@?kBDcBDcBPqCNgBXoCNyAVuBL_BHmA@MDcABsAByBAsBEuAE}@GaAIgAMqAOmASoASiAWoAq@sCc@qBc@sBWuAOcAUaBScBMoAMeBGaAGiAEsAE{@CyA?kAAkA@iA?ODwBFsCDcBB_B@kAAmACsAEgAIuAKqAIw@EY?EIg@QeASeAOq@U_AOg@Wu@Y}@c@eA_@y@i@aA]k@[g@a@m@_@e@k@o@o@q@o@m@m@c@i@_@g@[e@Ws@]m@Wq@Uw@Uy@Q}@Oy@I}@Gu@Aq@?}@@_ADo@H}@L}@NuAVgBZgBVaBVaBRuBVuANwBTsCTkCPcBHeBBqA?eBC_@Ck@C_AGoAOg@GeAQgAUa@Ka@KeA[kAa@oAg@w@_@}@c@oAu@mAu@u@k@g@_@}@w@u@o@m@m@k@o@u@{@SUQUa@i@m@}@s@eAe@u@e@{@i@aAw@aBw@eBwAmDmAcDu@qBgAyC_AqCiAmD}@uC_A_Do@aCk@qBm@_Cg@{Bc@yB_@sB]yBWsBUuBMuAOmBKyAK{BGuBEeBAyB?{A?uBBgBDmBFgDDqB@yA?mAAgAA}@EyAEcAGqAKqAKsAWuBYkBW}AUqAe@iC[oBYsBS_BMoAM}AIoACq@Ei@EwAEsBC{B?gA@sBDeBF{BFsBDyABqABmA?u@As@CoAC}@KwAIeAKaAKu@OeAWqAWmA_@qAe@yAa@eAa@cAg@_Ac@u@[g@c@o@Y_@Y]q@u@o@m@y@s@m@e@aAm@{@e@q@[y@a@eCqAg@[kAu@o@e@a@]e@c@u@u@i@m@{@iAi@w@u@mAi@aAa@y@i@iAe@kA]cA_@gA_@sAWeAYoASeA[mBU{AQwAQqBO_BKgBOuCGoBEaCAqB@gB?wADgBD_BJ}BLoBTyCTsBVoBRqAf@_D|@gFj@wClAeGb@uBp@yChA{EHWFWb@eB`AsDJg@Ji@ReAHg@Ju@JeAJiABq@DeABwA?s@Ac@CqAEiAEo@MsAMkAQiAMk@WsAW}@g@{AUo@Ui@u@{Ai@{@k@{@{AsB_AuAo@aAc@w@Ua@Sa@q@yAYy@q@sB]gAa@yA]uAq@mC[gAm@uBo@iBQg@a@gAs@kB_@{@_@{@_@s@y@yAm@gAi@y@k@{@w@aAoAwAaC_CcA}@cCuBmAcAqAeAcAw@}BeBiBsAuAcAo@i@a@]MOm@o@_@e@e@m@S]k@aAa@w@[q@Si@Yw@[aA[qAWoAQqAMqAImAEu@CeACqA?m@@a@BsAFeADy@NgBZiDRwBHoAHsADm@HwBBqA@}A@kBC{BCyAEkACm@S{CG_AGo@Gm@QyAIk@Im@QiAO{@WsAMk@UcAMm@]oA]kAs@wB[{@c@cAWi@_@{@e@aA_@q@Wg@iByCi@y@m@y@}@gAq@y@u@s@kAgAs@k@s@i@q@g@a@Wk@_@GCkAq@y@a@}@_@uAg@]MaA]qBm@YIOE_@Os@Yg@Qa@Q[O{@e@aBcAiAy@_Ay@iAgAsB_CqB{CkAuByAeDmAaDuAyDw@oBWq@a@_AYm@_@y@oAaCcBuCcCiDgBwBqBaCmB}Bs@{@OQkB}B_C}CaDoEYe@w@kAAA_BeCk@}@IQMQCG}@sAkAkBmAkBeA{Ak@s@_@e@oAwAgBgB_EiDWUs@g@iAw@q@a@_Ai@qAs@_@Qu@a@UK{@c@YO]U_@U{@o@CCi@e@[[WWY[c@g@y@kA[e@]i@Q[Yi@[o@Ym@Sg@M_@Qc@Oe@Mc@M]Mi@K_@Ki@YqAOeA[yBc@}CSmAWoAQ{@Uy@[kAOe@Qg@Uk@Qc@Wk@]m@We@o@eA]e@W]Y]s@{@cAiAq@u@m@s@Y]Ya@i@u@i@{@]g@a@w@e@}@_@}@c@cA[_A_@iAYaAMg@Oq@_@aBc@oBc@qBSw@Oo@Mc@Qk@Qi@_@eA_@aAc@aAQ_@S_@Sa@Uc@e@w@QYYa@i@s@SYW[W[Y[o@q@g@c@c@_@YUu@i@q@c@y@e@u@c@iAg@{@]mA_@}@UaASQCa@GCAk@IA?yDWwAK]E[G_@G_@IUE[K_@Mq@Y_@QYQe@Yg@_@a@[WUWSYWSSIKc@g@e@i@[_@U]U_@Yc@Wc@Sc@Q_@Se@O_@Oa@Oc@Oe@WaAYgAMm@Qw@WiAYmAYgAKW_@eA_@_A]u@i@eAUc@e@u@m@}@g@o@Y]]]YYg@a@w@k@YSs@i@[Uo@i@QQA?_@[WSMK[YSOMMUWA?YY]c@c@g@W]SWS[U_@S]Q]S_@O_@Se@a@aA]_A[_ASo@WeAOi@Os@O{@Kg@Im@Ga@K{@Is@Ek@Ei@Ce@Ck@Ce@Ao@Ci@?m@Aq@?}@@o@@y@Bw@Bo@FmAJuAHo@NwABW@C?ABQ@CL}@TiATgAZgAVcA\\eAPu@Ry@Ha@DMBOHa@BS@E@KF]D[B]Hw@Fs@Du@D_AD_A?u@?{@?o@CcACq@Eo@C_@KiAOkAM_AOs@SiAMq@SaAUiAKs@OaAM}@K_AKgAGeAE{@Eu@Cq@Ao@Au@AkA?aA@}@DsAFyABm@Fs@H{@Fm@JaAJs@F[Hq@Lo@F[DUVgA^gBV_AtAyEf@kBVeAXoAR_AVqARgAV_BL}@VwBXwCP}BPcDFcBB[R}FRgEJcBNcCNwANkAD]JeARwA`@mCZgBLk@R}@BOJc@VcAv@gDb@eBZ}A\\_Bj@{CJq@DUDWTwAFW^iC^wB\\qBVsAXqAVcAPm@h@iBv@{Bt@kBTi@h@eAvB_EtB}Dp@uAx@sBl@iBp@wB\\sAVkAFWP_AJk@`@eDRqBPcCJgBDiBBiC?gACgBEaCKyBQ_CSqB[kCUuAg@oCWqA]kBQwAE_@KmAIoAEqACuA?yAByBFwAHsAJmANmAPgAToARaARs@ZiA^gAz@}BXk@b@s@x@qAXc@Xa@n@_Ax@mAXa@f@w@f@}@v@}A^}@Tk@Pc@Z{@^oAZiAViAVkAr@_Ef@wCv@eEXsAT_AHUFWl@aCdA_EZoAPy@XuA\\qBPgA^eDLkAJuABi@HgAJeD@uB?q@?kCC{AAi@AYAYAy@G{BEuBAwB@sADwAJsBJoATkBTyAPcAVkAd@mBb@{A`AqCVw@x@eCNo@XgAJe@ToAZoBPoALgAJoALcCVuFJqAJeALiA`@uC|AoK^gCjAgI`AsG\\{BPsALeADc@J_BHuADgB@u@?gCAuAEuBGwAImAIoAKeAUcB_@{B_@mBc@gBGQCIACI[ACw@gC}AsEc@wAEOACGSg@oB]{AYwAIq@E[EYMy@?AS}BAO?CCQY_D_@{DE_@MiA[_COgAO{@c@yBiAgFk@iCScAKa@QeAKu@Ou@KcAMqAKqAKsBGoBAwBBuCHyEFs@HqAT_CVwBVsA`@iB^sAZgA|@}BfA}BlAyBrCsE|@wAj@}@d@y@Ve@BGr@uAb@_Ap@gBt@sBJ[J[Ts@Lc@VcALi@Jg@RiATsAF_@PoAPoALmALsAHoAHkAFoABk@BmAB{B?_CA{BAcC@uBDkEFoCDsADuAF{BFyB@wB@qAAmAC}BEcBoC}V_@eDw@mHw@oH{@oGAOo@eH[yD[aE_@gE[aEWaDOsBIaAIcAOoBIiAIwBCmAAyBDyBDyBDyB?cDE_DCmC@cDBsABi@L{BPsBXuBXsBPqA^yCHmAJqAHyBBwBByB@yBByB@{B@wBBwBB{BFwBFmALwBRyBJo@PoA\\oBd@wBh@kBj@iBr@iBhAaCx@_BLWZo@\\o@j@aATg@R_@Ra@`@cA`@eATq@Tw@ZkATkATiALy@RaBVmBX{BXuBl@sCt@oCn@qB\\y@fA_CjA}BtAoCv@aBRg@N_@Tk@La@l@_Bh@iBh@uBPu@Py@b@wB^}BVwBHo@LmADe@Dq@Fo@Do@Be@Bm@Bc@Bg@DqANsF@U?C?C@M?E@c@@[FkCDgABgAHqCD_BPoFF{Bn@sVVcJHsCXsGN_CLiBLyALwAHgADw@HcABk@Bk@DwA@y@?{@@gACoAE}AEeAKsBMsBQaCY{E[gGIgBKiDCiBI_EAyD@kM?oMA}@AqAEaAEy@IqAKcAS}AQgAOw@YmAOk@YaAa@gAa@eA}@yBa@aAiQkb@w@eBMYc@iA_@_ASq@Ma@Uy@Su@_@aBUgAKk@QeASsAi@wDYmCQgBMgBM}AKmBKiBGgBCiAGaDAoA?gAAcB?yB@u@DwCJoCHoBH_BJcBJiANgBR{Bf@gEh@{Df@wDDYPwAHk@HkAJ}AH{ABs@BsB@w@A{BCaFCiG?m@DkCDeAFcALoBRkBFo@NcATcBTkARaAb@gBT}@^qA`ByFh@iB`@{AR{@P}@P_ALq@L{@LaARaBNiBHcADeABc@D_BBiB@oA?qACyAEmAIiBK}AIiASwAMaAW{AQcAu@aDw@qCES_CaJuAgF}AsFQe@oCsIuJ_ZWy@cC}HqBgHg@gBi@wBI]CMWmAScACG[iBU}AU_BQuAMsAKcAMgBMoBIsBAm@Cq@CiACmA?k@?YAqB?}@@o@?s@BgAFwBBcAHcBR_Dd@yFTiDN{CFmABqA?k@@eAAeAEmBGkBQgCKoAMcAIo@OkA_@uBS_A[kA}A{FoAoE[mAWaAYqAYiAAIOw@ScAE[QeAMeAK{@Gu@KqAGaAGuACu@Ao@EyA@kD?q@@w@HiBJwBF{@LmA\\}CJw@Lw@TiA^iBf@sB\\iA\\gATo@Tq@fCmGhAgCh@iA|@uBfB_EhDkHpCsGl@}An@kBZcATw@f@iB`@gBXuARiAZiBNgALqAXeCHiAJoAFgABi@Bw@DoADwBBeBB_FBgC@aBB{@@m@FaB@[FqAJwAJaAD_@RwAJw@X_BBOh@qCb@wBd@{BXcBN}@L_ABSH}@HaAHeBBk@Bm@?iA?aAAuACw@Cy@Co@Gq@KeAGk@Ec@q@gFU}ASeBSqBGw@IiAEeACg@EaBAk@?SA]?[?eBBiCDuAH{AFyAHaBLgCLmB@a@NmE@oA@aC?gAEaCC{@C{@Ck@Ew@Es@OwBOyAYeC[uBg@qCWqAUcAiA}Du@wB{A{Dm@sA_D_HoAwCkAsC_@cAWo@Y}@]eAu@mCYkAQs@Q{@Qw@Ow@UoAGc@QgAOaAM{@SkBOsASwC[}Eg@gKWaESuCKmAI_AGi@MoAUoBc@_DSqA]qBSmAWmA_@kBe@oBeA{Do@wBe@yAs@{B[w@c@gAa@eA[u@_@y@mAsCs@}Aa@_ASg@s@}Au@cBs@aBa@aAg@mAq@}Aa@eAa@gAq@eB}@cCoAmD_@mA{@kC[eAOe@[gAg@cB]oAYgA[iAWeA[mAo@mC[oAWcAWgA[qAYmAa@iB_AyDs@wCWgA[gAk@mBYaAY{@e@uAq@gBaAcCs@aBa@aAe@cAqCeGc@aA}AiDo@_Bs@cBi@wAyAaEa@kAw@cCc@_Ba@yASw@Me@Sy@S}@Om@c@qB_@eBWyAO}@Kq@Oy@Ee@a@{CiAyHq@uEc@qCw@oEaBsHy@}C{@iC]kAyDuLISIWCGWq@IUOe@cBwF_AwDm@_Cm@kDUaBM}@ACMcAo@aHKyBQkEGyD@oGPuHj@sIBe@XwDf@mHTkELiGBoFIaHUqDcAeM{BeVoAuM[wDYiEEwAGkEAaCFwCHsCNaC\\}DX{BNcAV{A^sB^gBZqAlA}DtAoDHUlAkCv@uAlA{B\\m@Zk@`AeBlA{BlA{BN[Rc@`@w@pAsCn@cBp@kBJ[d@yAt@gCdBqHfAsGf@wE\\{CVyDF{AFwAFsBD}@FsEByAL_IXeQbAmt@HuE@g@@{@?IBgALqJZeSHoJ@y@@_CEeEGeDOkEs@mLs@cJQ_CGy@IoBIoCGoBAyC@uD@cBHkBF_BFqADw@HuAVsDT_CLiAHo@L{@TqA~BsKfAiFf@eC~@mEF_@RsAL{@XuBPwAHq@h@iFZwCNqBF_BD_AJcCJaDPaFR}E^yGRsDZsE`@wFJmAb@iEd@uE`@sD\\cCPuA\\}Bf@cDj@mDzAuIxByLv@kEp@gDn@gFh@yEh@oHb@{KF{BFyBf@gTHiAFkA~BqUz@sG|@{GhAmIx@cJZgJ@eEE{CAkA?QAq@CeDEyCC}BCiBGwBEcDAiDFcFFeERyFV_Ed@yGh@_HfBuR|@yJjAqRd@wK@OFyBNwFb@}SLqHR}KXsHRwDTiC\\eEz@sHx@aGp@yDpAqGvA_Hr@sDtBoL~@aI`A}IJcBjCeh@~AsUt@kIzA}Mj@oEtCoQdAeGtAuG|BkKpAeFt@wCj@sB`JyYVw@dAuDZeAfAsDx@{CFUlAiFh@kC`AwEf@sCl@oDHc@BQBKDa@~BuQfBcMN}@N}@z@aFvA{FjBmH|@wC\\cAbBiF~AmFbAuD\\oAPs@XmATy@Ja@BMb@sBbAgFh@eDFYDOBQBMBSBMz@iGh@aF^qEZ_FDi@VsGNoF@g@DqF@sA@w@@iD?oG?M@w@@}K?eA@gIBaJ?eD@yA@gD?oE?qBB{FAeA?cABgK@cHDcHJmGLqFJoDRwFJcCNuCLeCNeCJ_BJoANqB`@{EXaDZoCJcAVeCd@mEl@_G~@_J`@gDt@_HXaDlAmKNyAh@}ED[nCqWHu@Hs@P_BLoADe@X}CJoAJqANuBLyBLyBFqAHyBBqADqA@i@DyB@qA@sA?sA@qA?i@?i@Ak@AoAAqACuAAg@Am@Ae@Ag@Ck@I}BIqBGoAKwBEqAI{BIwBEsAIiCAcACsAAoAAk@?i@AoA?i@?qA?{B@qA@yABwAFmCJyCNwCReDXuDv@mIj@gFFi@Di@bC{TnA{KbAmJx@oH`@iDPuA\\gCXoB^iCr@wEBQz@wEb@cCnAwGj@mC|AiHnBkI|CmMdCqKr@eD\\eBh@uC|@iFXsBd@aDLeANgAPyARkB\\qDTgCReCZeF^}F@_@\\iF\\kEh@kGj@uFr@oGhDmXj@mE~@wHZoCRqBR{BJkALuBLmCHoBBgBB{A?{A?{@AwB?gAEsAEeBEq@ImBI{AQyBKqA]yCe@uD[qBQeA_@gBk@iCOm@aAqDu@cCmA{DYcAo@mBq@qBMg@Us@a@sA]kAi@wBc@qBq@cDq@sDQgA]iCWaC]qEQmCGmAEwAG{BAwAAkE@mABwBFuCPiEVyDFu@JoAh@gGr@sIh@_GXwDRqBZmDb@qE@I^aEb@cFd@aGBS@ODa@XqDf@mGl@}GX}CTcC^sEXeDVaEV}DPuCNaDNcEJ_G@mC?mCCoEGiFGoEIwEK{EGeFGoH?y@@_CDeDHuDD{AN}CLqBNwBDi@f@wEZkC`@cDf@iEd@gEb@iFVsEPuDHyDFcD@yDEwCKgEKeCMcDOcES_FMyDCyACcBAaCAkA@gBBqADaBBs@LkDD{@L_CNaDNoFHyDBiC?}BAwB?g@IwCEeBGkASyD]eFa@aFg@iHQoDK{BC}BC_EA_@?_DDeDDuBFaC?YJiCJeBFwABe@HwAFiBFsBFaCDkC?gD?iAAaAEeDI{BUmDYuCa@_E[{Bm@sDeAoF}@kEmAwFWmAYeB_@_C_@}Ce@{DSkCSuCMgCEkBE_CCsC@sDBgDFqCPuJL}JFwELgHJgIJkG?qFCuDCgCEeBCw@QkEQiDW}DOeBQgBSmBe@_E{@eHu@kGm@aG]oEMmBK{BKsCG_DEgE@gE@cCHkDH}CJaCVkF`@aITsEXyFZoGZyFTaF~@aR`@aIb@uIb@yIXcGZwFVoFR_DNgBNeBHcA\\sCTiB`@eCb@{Bh@mCt@wC\\mAr@aCz@eC|CcIf@sAXs@L_@Xu@To@Xy@^iAZgA`@uAZoAVcATaAZyABMNw@Lk@RkANaAHk@N_AJ_AL_AHs@J_AFu@H}@JqAF{@HiAFuADgABu@Bm@@k@@o@BsABoA@mB@yABkB@eB@aCBgB@wBBiDBeC@eB@yA@oBBoB@{B@{A@cBBoB@_B@yCB_CB}B@yB@_B@cB@wA@sA@qA@iA@kA@cA?aA@iA?{A?yA?eA?y@CkAAiACaAAu@Cq@Aw@E}@Cq@Ag@Cw@Cg@Cm@Cm@K_BIqAKqAIaAMuAMuASiBC[UoBOsA]}CUsBYiCMwAQyAMiAKgAAGEa@G_@EYGe@K{@CSG_@Io@Ea@_@}Ca@aDS{A[aD[}CMmAMmAEc@SqBQuBSyBCc@MsBIqAEkAEq@Ak@E_AGaDCaD?wG?SBkCB{C@u@@eABoB@{B@yBAwBC_DEyBEqAKwCMqBKkBKyAS}BMuAQ}AWiBIs@SqAW}AU}ASiAOy@UcAMo@UcAWmASu@I_@Ke@]qA_@{A]sAWgAc@iBYkAo@qCWkAa@oBUiAk@wCSkAQgAIe@Kq@Io@OcAUmBOmAUuBUwBOuAOiBQyBMcBIkAEu@IqAGqAI}AE_AA[SgGAg@A{AKcKKuMEyCG{CEiCAM?OCo@IyCIkBGaBKuBEq@ASAQG{@SsDOeBWiDW}COaBg@kFc@kE_@{DEa@g@oF[kD_@sDg@mFSoBSwBSsBWsCOwAa@{DUeCQcBS{BQoBKgAQwBOqBWkDKqBKqBGeAGeAGkBIsBGeCEgBG{CEyBCwBCoC?iCAqB@_C@uC?uB@yB@iBB_D@eB?oA@cC@qC@kB@_B@yC@oA@gD?oB@cD?sABgD@yD?oA@wB@uB@wBBkD@oCBuE@}C@qA@{B@wAB_F@iE@yB@cDBqD@eC?mAB{C?wABsC@}A@yC@uC@uA@eDBgD@_B?_B@uB?_C@yB?mA?aCAgBEeE?UAUEeCK_DGmAGmBIoCM{COuCWwEW}FAOKwBOaDIsBAICoACoACiAAwACkA?k@CsB?yB@uB?yBBuBBoADyBDuBHyBFuBJyBP{Ch@qIBQ@QXyDt@cKb@iGd@aHV_ER}DJyDFkCBiC?iB?qCEaDCsAE{AGqAIsBMgBMuBKwAMiAMsA[sC[yB_@gCi@cDc@uBg@{Bg@wBi@wBoA{EuDmN}A{FeBmG{BoIYiAWeAc@}Ae@aBmAuE[gAoAqEAGMc@AGCII[sBqHaBmG}AwFo@_CeA_EqCgKo@}BgBuGw@{CEOGOuBaIcAqDQo@Qq@q@cCwAmFEQGQ]wAYgAYmAWkAWkASmAIi@SkAIg@OoAQoAMqAMoAKqAIqAGqAGqAEsAAg@Ai@CsAAqA?_C?qABsABqAF}BJ{BhBi^hAkUn@qM@U?A@WXuF@SJ{BHwADgADqA@o@B{ABkA@}B?oACmCGyBGkACk@GmAMqBMqAMuAGi@Ec@OiAQqAIm@SkAIi@UiAOw@SaAWgAWgA]oA]iA]iA[}@Ww@k@{A]_A_@_AaA_Cc@iAc@cAq@eB_@}@qAaDq@eBo@}Aq@cBs@eBc@gA_@}@_@_Aa@cAq@cBu@kBw@mBOa@IO]{@{@wB_A}Bw@oBi@uAk@_Ba@iAk@aB]gAU{@_@qAi@oB_@cBS}@Om@a@sBUkASmASoASmAOiAKy@M{@Iw@MoAS}BEi@Ec@Eo@IkAGkAEgAEm@EkACq@EwACeACgACmAAwACeACsACoAGuBEkAI}BImAGuAMmBEm@KkAMoASqBQ_BK{@QoAUyAM_AQaAUmASeA_@iBWiA[qAU_Ac@}Ag@iBSq@_@iAMa@Oa@Y{@Us@k@aB_@iAs@uBKWIYSk@Y}@s@{BU{@Y}@YeAWgAc@mBYsA]gB]kBSiAQmAQqAMcAY{BMgAUcCMcBKsAIoAEw@Co@G_BGoBCcACsBAi@A{@AiAAq@?kA@gA@s@@_A@y@DsABaADoBBq@FiBFwBBa@@a@FwBBkAB}@@{A@w@?i@?k@?mAAyAAaAAu@Aw@Ck@Ay@EcAEcAEs@GoAIeA?OEe@KiAEk@OaBKy@Eg@Gc@UcBKy@OkAOaA]{B]yB[oBSqASyAGa@Gc@]sCMiAEc@OoAQmBUqCMgBIoAIsAKiCMsDGuCAwAC{BAiAAyA?iB@wB?gF?oCAaBC}CCmAEyBIaCEiAK_CQwCMuBQyBSsBSuBUwBWsB_@kCi@eD]sBSeAKg@a@mBWkAq@uCe@kBYeAu@oCeAmDeAqD}FuRU{@uDeM_BkFmKc^kDoLwA}EeDyKiBkGaBsFa@qAcAmDi@iByC_KQo@}CiKkA}DaEgNuAqEw@gC{@oCUo@_AkCs@mBe@kAa@_Ak@sAaAsBo@qAa@u@s@oA}@}AiAcBeA}ASYAAOUoAcB"
                     },
                     "start_location" : {
                        "lat" : 41.9071443,
                        "lng" : -7.4308258
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "51.5 km",
                        "value" : 51465
                     },
                     "duration" : {
                        "text" : "25 mins",
                        "value" : 1519
                     },
                     "end_location" : {
                        "lat" : 42.4994751,
                        "lng" : -5.5996698
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eA-66\u003c/b\u003e",
                     "polyline" : {
                        "points" : "uxv_Gptqa@oA_BcCmCuAsAmBaBuC}BsDiCMKwBwAYSkAu@UOUOkFiDwBsAoEwCkCaBoBsA_BaA[Sc@YwCeBcCsA{Aw@sBcAyCkA}Ak@kDeAmA[a@IQEC?UGs@M_@EiBWaD_@yBQsBKqCE_GByAFs@FuAJ[DK@gDb@m@JcDf@{Dr@wBb@y@PyE`AcL~BmRzDiAVa@HiQnDmEx@aC^}Er@gBRwALkEXg@@{@BuEFi@AyBAWAsAEeAGi@EsBQ{AOsCa@aAOYGYGyAWyBe@sA]g@Oy@Ua@Mg@Q{@YyB}@iAg@uAm@sAq@s@_@aDeB{@g@k@[e@[e@Y_Ak@SMiCcBmEyCqA}@yHiFgBmAgBmAgAu@oAy@UOsAaAmBqAuA_Ao@c@s@e@mAy@_DwBkAy@u@i@sIwF{AeAeAm@w@i@q@c@uA{@WQe@Y}AaAYOGEOKuAy@iBeAoAs@i@Y_@UiAm@iBcAc@UkBaAe@WmAk@a@SmB_Aw@_@mB{@u@]w@]mBy@aFsBsBu@mBu@_Bi@kDiAQGeEqAgEmAaCq@gBg@sBi@gGwAsCo@eASgB]{@Q{Bc@{@OuDo@oBYaC]s@Ia@EMASCe@Ge@EoGq@{@I}AMeAIqE[YA}@GyBI]Ca@AyAIeBImDMuIUwGQm@CyFKsCIsEOyBMiAIkAI}AOgAMm@G}@Ke@IKAOC_BWaCa@qAYyCq@mCs@uC{@{Bu@iEcB{CsAmDeBkCwAo@_@GGA?AAy@g@o@_@oAy@oA{@mA{@oAaA_CmBcA}@a@[yBqBo@o@uBwBmD_EmCgDyBuCaCmDmB{CuBoDcB}CkA}Bw@cBw@_Bs@_BiAmCs@cBqCeH}@_CqAeDk@{A}AuDAEEKACGM]y@s@}Aw@cBm@mAe@}@kA}Bi@}@q@oA[e@k@_Ac@q@cA{AcAyAEGAAGKkA}A}AmBaCsCeCiCwAwAcA_AoAgAIIoB}Aq@g@cDaCmBqAeC{AmAq@yAu@mB_AcDwAiBs@MEKEqC_AoA_@yA_@q@OaCg@qDo@_C]eBSsAOkCQoCQkDK{ACcB?iA?}BD_AB_ADqAHa@BcCPmBLkFZ]BqAHO?M@}ADi@BoABsA@kAAkA?_AAuBK{@CmAK_BMw@KqAMyASwAU}@QmBa@{A_@{Aa@ICKEm@QuAa@_Bk@wAi@uAk@SIsAm@sAo@aB}@_Ai@wCiB_@UKGMIeAq@yFqDsFeDsAs@yAu@_Ac@GCCAIEs@[mBw@q@WqBq@}Bu@{Cu@uA[sAWOEOCMAsAUuASc@Ec@Em@GcCQeDSs@CyCG}AAG?G?O@c@@q@Bg@B{DPw@Fo@DsBPaBRiAN_ALy@NaCb@cB`@I@A@KBI@oGbB}@VuEpA}@TsCp@s@P{AX}@NA@yATOBmBViBRoAHaBL[@oBHiEDuCAyCIQA[CC?WAeAGa@EmCW{AQy@MyAUwAWwA[uBe@iA[mA]kEyAoBs@yAk@mCeAqBy@iCcAuBs@uBo@eD{@cBa@WEYGaAQy@MwCa@{@KcBOs@GyCO{AGeDEqB@{BBwCJyBLcBNwD`@iNfBoEd@yBNuAHeBFW?Y?aAByC?yCEwDOcBK_AGgCYuEm@wF}@oC_@yASOA[E[Cc@EkCUwBKwAGsACiBCsC@eELqCPgCT}BVuATg@Hg@Jg@HqBd@aCl@uA^yAd@w@Vw@XuBv@oBz@sB`AoBbAcB~@a@XqBnAiBpAcCjBiA`AsAhA}GpG}@x@y@z@u@t@_OrNYXYXA?iEbE_EzDmCfC_CvBqD`DwCzBUNUP]ToD`CkBhAyBlAoB`A_Br@{@`@{Ap@kBr@qBr@uEvA_EbAsAV}A\\mAPe@HWDI@C@WDOBgAL_AL{@HoAL_AHaAFuAJqADsADyABsA@]?I?U?iBA}BCy@Cs@CyAGgAGy@G[Am@GiAMMAo@GKAcC]i@IyAUaCc@kAU}A_@qBi@oCy@{Ag@qAe@wAi@oBu@sB}@sB_AwC{A[OYOaDeBkJoEIEMGWKuB_AwGeD_FeCeDcBmBaAcD_B}Au@qGwCeDyAiDwAoCcAsBs@qAa@}@WcD}@AAsA[aFuAmIsBoEcAaGgAg@IwB_@oB[yCc@yAQmCYgBOsBS}@GwDWqFY}BIsFOuBCcCCeF?aGF_A@_A@sADkGNuDN}CLuBH{AF}Nv@qIf@{G`@uBJyCLyBJwBF{BH}CH{ABy@@_A@{A?wB?kBEgAAoDOg@AuAGsAKq@GuAM}Dc@mCa@yAU{B]yAYuBc@oBg@uBk@eHwByBo@wBw@oBy@k@WyCoAiEqBoDkBeB_A}DyBsBmAgDmB}@i@WOIE_@UyBkAiCsAWMa@Qa@QqAg@}Ai@eDcAqDmAcA[QE]I_AWyA[qCi@_Ca@cC[cCU_AGiBKwCIoCCeDI}DHsDH_CJuCLyBLsETsD\\qBJQ?Q?wBHqEFyC?_B?sCIeCIoCOyBQw@IGAcAKmBWoAQ{Ce@uBa@wA[{A_@}GiBSGSGkRoFsA_@uCy@sCs@]IkDy@wCm@uCg@wDq@qCa@qDc@kAK]CUCC?SAo@EkBKK?MA]Ak@C}AGwA?}EDsDF_DJsERsAFgAJ_BNqBV}BZkEt@{Cn@oDx@sCv@oDjAyAh@kC~@_GhC_ChAYNYNcB~@mC~AgC`BgCbBaCfBkBvAaCjBeBtAgBzAqAfAgA~@QNSNoCbCgFnEuD|CsCzBaE|CyCzB_EhCaDnBqC|A_EtBkEnBmBv@{Ah@eBp@cBl@sDhAsDbAuBf@yA\\}AZ{AVwAR{@L{@JaDXyBR_@B_@B}F`@S@UB{AH_AF{@DcADc@Bs@@{A?iCCkBC_AC}@C{AGyAKa@C{@GC?YCq@IcBQy@K"
                     },
                     "start_location" : {
                        "lat" : 42.0649145,
                        "lng" : -5.6661739
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.8 km",
                        "value" : 800
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 30
                     },
                     "end_location" : {
                        "lat" : 42.50445089999999,
                        "lng" : -5.5935424
                     },
                     "html_instructions" : "Take exit \u003cb\u003e152\u003c/b\u003e for \u003cb\u003eA-231\u003c/b\u003e toward \u003cb\u003eOnzonilla\u003c/b\u003e/\u003cb\u003eN-630\u003c/b\u003e/\u003cb\u003eLeón\u003c/b\u003e/\u003cb\u003eOviedo\u003c/b\u003e/\u003cb\u003eBurgos\u003c/b\u003e",
                     "maneuver" : "ramp-right",
                     "polyline" : {
                        "points" : "wtkbG|tda@QQAAQC}@Os@MUESGSEOEKCKEQEQGQIQGMGOGQKUMSKWQMIWQWSWUe@c@c@e@OQOSKMEESY_@i@OWQYQ_@OWQ_@]y@O_@Qc@Oa@IWI[Ka@Ka@GYGWGWEWG]G]Ge@Iw@C[Ea@AIES"
                     },
                     "start_location" : {
                        "lat" : 42.4994751,
                        "lng" : -5.5996698
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "160 km",
                        "value" : 160018
                     },
                     "duration" : {
                        "text" : "1 hour 19 mins",
                        "value" : 4747
                     },
                     "end_location" : {
                        "lat" : 42.3315298,
                        "lng" : -3.7777171
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eA-231\u003c/b\u003e",
                     "polyline" : {
                        "points" : "yslbGrnca@OyCAg@EcAAq@Ak@Aw@Ay@[}UAy@Ay@CsBI}FCiBCyBAk@?C?WAQA_AAu@?OC}ACyD?sA@a@@c@@q@Bg@Bi@D}@L{AJaAP}APgAVwATiA\\uARs@h@kBh@eBj@iBl@kBp@uBz@gCjAkDr@uBZy@`@gAj@yAXo@b@gAr@aBb@cAx@eBv@}ABEj@eAp@oAz@yAp@iAvA{Bb@m@x@kAn@{@`AoAjAuAj@s@h@k@fAkAn@q@n@q@~A{A~AyAn@i@x@q@RQfByArDuCpLeJhIoGnEgDjA{@jCiB~AeAr@c@lBiAlBgA`D_BjB{@rAk@tAk@lBs@bBm@LEtBq@rAc@|@UZIZIhE}@rCk@xAWnCg@vB[nB]rCe@vB_@tAY\\GtBc@v@Qz@UtA_@vAa@pAa@vAg@pAg@ZMv@]\\Qt@]ZQpAs@p@a@nA{@VQr@g@ZWlAcAhAaAj@k@XYl@q@XYbAmATYV[tAkBV_@f@u@T_@h@{@lAwBt@yAP_@b@aARe@dAgC|@cCn@iBPg@ZcANe@j@iBv@mCf@mBVcAf@qBd@iBJe@p@oC~@yDn@kC`CwJvCkKt@eCfAiDl@mB~GwRd@uA~@gC|AoEx@cCl@iBLa@l@mBz@mCh@iBf@iBJa@h@mBVcAh@yBv@mDp@aD\\kBRiAHc@He@Hg@VwA`@qCXsBVqB\\}CLoAXwCPuBJuADc@LyBFiAJ{AFmADiAFwA@QJqDBsA@mA@g@?KBmC?qA?{AAoBCsBAq@GsCCm@G{BEoAIgBEu@GqAEk@OqBWgDa@{DWuBg@gEc@sCIc@q@yDUoAKi@Ke@Y}AYoAMi@Kk@Kc@e@oBq@yCMg@Kg@[mAYiAeBeH{@qDs@wCUgAKa@c@sBk@sCUgA}@gFQiAYqBIi@WqBOmA]}CO{AKmAOiBO{BMaCQiDGuBG{BKgHAeD?mB?aEBqENgSTyV@cCJiMJyLPqVFyHAsEAiBCcDGsCa@{JCk@Eo@Eq@KuASiCq@yIc@mFQ}CIyAIcBC_AC_ACyB?o@AgA?m@?i@?qA@YBcBLwEN}Cr@oNLmC@k@JuCD{AD_CBaC?oA@}AAmAC}EEaDEqAEqAEsAGmAE{@EgAQyCQ}BQeC_@qE_@aFW{DI}AMiDAu@Am@CcC?wC?w@@s@DeCHgCLwCJ{AHgAH_AJaA\\}C\\iCd@{CJk@`@{B`@uB|AqHr@{Cf@yBnAgFf@iBl@}BZeA`A{Cb@qAVo@n@cBfAeCVk@\\s@`AgBtBqDnBsClCsDhBcCbDeEfAwAPUjCsD@CnAqBfAmBh@{@|@aBj@iAz@_Bn@sAjAmCp@}AZy@tAoDr@wBj@aBn@wBh@mBXiALk@Jc@Pu@Nq@b@_CN_ATwAXqB@OBMNqADa@Fq@@ADq@Hw@JyAJeBJaCFkBBcADcD@qDAeAC{BEqDIyDEyAGwCEoBA_@CkBCgB?eE?gABcB@c@DuBHwBDcALuBJuA@INcBFu@TsBPwALeAVcBNaAP{@X{A`@oBXoAXgA^wAp@_Cf@_Bv@aClCmIt@{BhAmD`G_Rh@_BdA_D`A{CFSL_@dAcDxAsERo@pA}DjByFfAgDdAcDxAsE\\cAlBcGrBmGh@aBj@kBl@oBp@eC^sAl@iC^gBd@cCVuAPiARsAPkAPoANqALmALmAPsBFq@NwBHqABk@HwBJsCBw@BaABaD?kC?qBCoD?A?k@AcCGkFC{CCuDA_ACgDGcHEuECgDAoACaDAsA?{@?kE?aAB{BFoCDyABqAD_AF_AF_AFgABa@Dg@Dq@F}@JcAJiAJ}@Fo@NmANeAHo@BUX}ATqAFa@Nw@VsATcALi@f@{B^uAZgAZgAf@aB^kA^iA\\aANa@`AmC`P_b@bIuSvC}HrAsDp@iB|@qCj@kBr@gC\\sAXiAVkAd@aCf@gCT{A`@iCL_AVmBRkBRwBL_BTaDJcBJeCF_CD}ADyB?mA@{BAmBAmCGoCCwAE{BEwBQ{GIkDC_Bg@iTSeIMoEIgE[aNw@a]KsFEoCGmDCaD?oDBmDFeDLkDJuCPcDNyBPgC^yDRsBVyBp@{E^{B\\uBj@cDfEoTbD{PbA{FdBiKhAkHBS\\gCl@mE~@sHn@}Fj@iGJgAHy@RuBRkCPsCNuB\\yF^cHTgENcDXgHBaAXiJPsFd@wPDuAX{GN{CFmADq@JuALwAJkA`@uDZwCPoAb@_Df@{C^qBX{Ab@mBh@cCf@wB`AsD`B{FrB_HrFyQjC}If@_BjDqLfB{F~CmKbCiI`AgDlAmEvB_I`B{FxA_FTy@nA{DzBcH|BgHtDuL~B{HrAqExE_PzCeKfCoIfB_GlBmG|AmFtAsEx@wCt@wC^cBTiAXuA^wB`@kCZiCNqAZyCNwBLyBJgBDoADcAFaC@mAB{E?mCGqGCcDAuB?gA?q@?uCJcFFmADiA?ADkALeCLmBR}BRgB`@eDp@{EfDaUfBuLbH}e@xBeOd@wC~@yGVsBLqAT_CXaDRwC^yGP}GPgMLqGFsCLuDDoA\\iI`@mHZsE^cFbBwRxAmQpBoVhA}MbAsLj@wGRsCNiCZmEh@sHh@}Gb@qFZiD~@uJPcBBUhAiLRyBl@yHfEog@@AN_BReC^yEJ}AHsARwDDaBDeBBaC?E?S?o@?{A?_AA_@CuBKkDA[IoAKiBG{@QsBQoBOuAUeBk@sDCQAKIm@QaAc@{Bw@kEa@sBOw@Kc@Ki@s@wDcB_JmAiHg@sCi@eDSoAWqBOkAIw@QuBKeAIkAEo@Gy@Co@Ck@Ey@Cu@CaACeAEeC?C?kB?]?i@@mAFmDDoAFcAFsADw@LgBDg@Fi@JgAHo@Fk@X_CNgAPcAZgBh@mC\\{A^yAZqARu@Pm@d@eBH[Ng@`@mAd@_Bx@gCHUHSl@oBJ[n@oBxAoEjAoDPm@xCmJhA{Db@yAt@uC^{AJg@FWn@iCh@cCPy@pAmGp@uDdAoGnCoRnBaNXiBbDgUF[N_AFa@Lo@@IJg@FYLq@Ps@XmAXkAZkAh@eBb@uAj@cBp@iBp@_Bn@uAd@cAlDsHdEwIR_@vCkG`@y@l@qA|DcIb@_ALWXs@Pe@l@{ARq@h@iBH]@GHe@R}@XcBJq@LcAJ_AFi@HqAFeAFyA@w@@qA?e@AyA?iAA_BCyCGkGAg@?e@CsAAmAA_A?}@EeDCcDEoFC_CAmAC}CCiDAeDCcDA_DA_D?iD@mE?uBBwB@aCBoB@y@@_BHiDD{A@m@BmADiA@Y@Q@]B{@LgENmDFoBBg@FqABk@FoA^sHR{C`@sGR_DViDPwCj@_IZkEJiB\\cFPoCJ_B\\kFXaFTuDRmETkEHgBJ_CNcEHoBJaEPaIH_GHaEFiKBoF?sD?G?sCAwDAaCCwBGuHG{ECiBOkHOgIMkFOaGQgIM{FKuDIaDSaJA[OoFOuFOaEKkCUqE]qGSsCy@gLi@qGSsBWyCa@aEWsCYaDWuCYyCUuCW_DY}DW}DSeDQwCO{COyCEiAEuAGqBEwBAw@AkBE_EC{A@sR?{@?M@k@LmId@{LBg@@a@?C@_@DeAp@eJDk@hAkNnBqPnAwIVkB|BiOrAeIV}AJo@@GHg@ZsBFg@BMBMv@wEnIaj@lA_Jl@mEnAuJ\\yCFk@@IHu@x@}HFo@Fq@`AeKPwBp@aJx@iMh@}JPoD`@sLRqHPeHH{DHeEF}F@mBFuI?ea@?uDAaDCaGE}FCuECqFAaFEeGEaG?sGAqG?sF@sGBiFBqEFwEHqFFgFLsEN_GRkGPqEPsER_EVuETsDZiERoDRmCNcCT}BNmBPkBX}B^aDZcDd@oD`@uDb@}Db@qDb@aDd@qDj@yD\\cCf@yDf@gDf@iD^yCf@}Cb@yCb@}Cb@yCZuBZsBb@}Cb@{Ch@eE`@{C`@_D`@{C^_D^}C\\aD\\_DX}CX_DXaDV}CVaDVmDTwCj@qJPeDZsGR}FFeAJaDJaDNmEHaDJcDJgDFaDHcDHcDHcDJcDHcDHcDV_JLmDp@uOn@uKdAmOnAsPdA{MbAeN|@}LjA}Qz@eOp@iNj@{PX_MZgSFoRKqT_@cWo@mRo@gOy@qOu@uLw@wKiGux@C_@CQASc@sFYoD_@gFo@oJk@yIWeFi@eKg@aOMaEOsGGmCIoGCuBC_CEgGAiH?a@?K?W?}@@mJ@iEPgUDsCHgH@U?S?GBiBL_LDgEH}I?]?K@i@@_BByE?e@?e@@aF?aA?yB?oACgCAwCGgEGqEIkE?[?AAYC}AEeBK_DKmC]}GUgDOwBOyBKiBKwAGoA[gEi@wGuDw`@i@oFy@wHi@oFo@{HYkEEs@WkEImBKqCYyJSgIEu@OqGU_II_CKuBMyBKcBUkCYqCKmAWsCSgBK}@CQ_@uC_@oC?EQgAKk@aAaG_BwHaA_Eq@oCsA{EeBuFg@{AiDqJKYCIQe@q@kBaJeVEOsAuDmL_[g@sAM]aAoC}FmQaAaDw@sCw@mCg@gBcEoPQw@Sw@S}@qBmJk@oCs@qD_@{BgCwNcDmTqBiMiAgHqAqIqAqIKk@Kk@w@oEuAwHc@}B[aB[}Ay@{Dg@cC_@eBw@oDq@wC[uA_DqMgCyJkD{LmE}NqBqGUq@Qe@i@qB}CmJWw@iIqWcC{I_BeGS{@Mi@w@uDqAwGm@uDq@eE}AyJqB{MyB_MmBcKgBwI}A{GiCkKaB{GWaAoDsMaCsIu@qCeA}EmAqF]cBQ}@y@}EeA{H]wCQcBo@wHMiAIiAUeDQoCIiCIaBOcFGqCCqCAsD?qFBwFL_KPeMDmJC_GEyECqDM{EOuE[kGYiEw@mIo@{FW{BqFca@wJcq@sAyI{AqKmBqNy@_Gy@kHq@cHi@yHQ}CYaFeBi`@]qF{@wMaAiJy@uGq@_Fq@yD_AeFkBuKqCoMuAcHyBcMO_BQ_Ba@qEw@mNI_FCiCAc@?iA?k@?eBHgEVkKFwAhC}l@B_@P_F\\cIb@aK@S?C@O?Gp@mPBs@PwD?WDsA@WPoEFkAFoADqA\\wJNeEZ}G^sHl@aKr@gLh@}If@kIHwAHyAH_DT_GVcINeJNyK@{F@wF?kGAeHA_EGaFIqEWmLOeFS}F[}GWiG_@cG_@yF[qEe@aGEc@iBiRcAqKUcCqBgSw@{HqBqOoAyIqC}Oo@eDoA{GmBsLu@sFk@wE_AuJs@{HWyCSkDQaEGoCSsHKiIGwRFuN?aEAeCCsDGkDK_DG_BIyBYyF_@{Eq@sGq@mFe@gDg@sCuBiLcAsFiAeHo@iEi@mEYoCSaC]cFO_CIaCIcDEqCCgDAgD?}B@iBDuCJyBBs@@EBm@NqCVkDTaDX}C\\uCr@uE`A}Fz@cEdAgEjBiGdBaFbAsC|AwDxCiHXo@Xo@dDaITk@hC_Hv@wBv@yBp@yBl@qBn@{Bl@cC|@uD~@}DZ{A`ByJ|@eGRyALgAPyAPaBh@yFL_BJuAVeEZsHLwFJaJB}C?iBCmBCaFIgEOqEYeGUqDQkC_@yEYiD_@aE_@oEc@uEe@{F[mDW_D]}Fa@_HYkHOcIK}JBgJLwJXgJ`@yHh@yHn@_IrAsMjAoKfA}J`AcKd@yF`@qFLgBLgBt@}Lh@uK`@wJTsH^mSTwR?qE?wECeIGeV?wMFsHTmPBiBb@wOz@oR\\yF?CZcFfAeNp@aIBYx@sHJu@rDsYpF}_@XsBt@qFzDiXbBmO`A}LDo@NaBDk@B_@XuFJsBh@eQJkO?wFG}K[oKm@wNSwDiAyQ}A{Vi@kI]_G_A}Ny@oMSuCMgDOuBc@yGe@_I]_Fg@cIm@qJy@uKW_De@cFs@yGu@kGu@gFy@sF_@cCqAaIkBgJgDoO{@_Es@eFSiBMwAO}AEi@EeAGyBE_D?sA?sA@sABcBBo@Bo@VaEH{@Fu@Fm@Jm@PkAF_@t@iEVoA^wAf@iB`AwCRo@pDsJbNk^`@kAb@qAj@gB~@cDb@eBVoAVqARkAN}@ZwBPuALkALwADo@HaAJcBD{@By@HcCBuC?wAAuBEcDSiHS{HEgDA_C@aC@wCL_ELwDJyB`@kFZaDVyB`@_Dl@uD^uBh@mCt@iD|@sDl@}B~BiIxAgFhBmGjAuEr@}C~@qE^oBnA}GRoATwAR_BLaAZiCZsC`@{DPmBPgBHm@RqBXuCRsB\\wCToBTqBXqBPkAZqBZsBf@uC^oB^mB`@mBb@iBd@iBf@mB~BmIv@cChAsDn@wBf@}Al@sBZcA^mAt@mCt@sCh@qBd@oBp@}C`@sBZaB\\kBb@gCL_AV}A\\iCRgBVyBTsBRwBRwBN_BFeAJoAHoAJsBJiBHmBJsBFqBBo@@o@Bw@BkA@_@?g@@i@?S@q@@o@?eB?{AAaB?mB?kBAiB?mB?gB?kCBcBBaCDmBFaCF}BHuBHoAHkAJqANoAHi@Fe@Fe@Hg@Hk@Lo@P{@Lk@Jc@Jc@Rq@Vy@d@uAX{@Pc@^_Af@iAb@cAf@kAb@_Ab@eAd@eAN_@N_@Vo@\\_ATm@Vu@^mAZcA\\iAZgAXeAVkAXuANw@Ly@Jq@D[?CF]RcBJ_AJgADe@JiAFiAFgA@]@a@FgB@g@@oB?qA?{BEaCCqAGiBE}@IgAImASwBOyAQsBSsBY_DI_AI}@MqASmBMuAMqAEm@Ce@Cc@GsACm@Eg@EkACs@EiAAg@EsAAg@?m@?c@?k@@e@@i@@k@Bg@Bk@Bi@Bi@HgAFq@Dg@LeAHs@PgAHk@F]Hg@Hi@Ha@Ls@TcALk@Pq@Ha@V}@FYRq@h@gBl@iBb@mAxCqHdAkC^cA`E}KnAmD|@kCvAuEj@kBj@sBLg@b@iBr@wCp@_D\\iBJm@RgAZyBNgAXyBRuBR}BV{CN_CLwBFqAHyBBq@BiAFcDB}B?}B?m@CoBCyA?g@EmAEwAGwBGqAOaDGsAGaAKcB[mEUyCU}BSuBSuB[eD[cDy@_Ii@oF}@gJi@qF[}C[aDGk@UyBQ_BI_AKuAG}@Gy@ImACo@IuAEkAGwBAu@CcA?_C@oABsABi@H_CPaDNyBPwBTyBVsBXuBRoATkAb@uBXiALk@f@gBZiAl@mBn@iB`@iA`@gA`@aA`AaCpCqGdAaCh@kAdA{BfA_C~@oBLUxAaDjAiCjAeCp@_Bb@cAr@aB|@_CTk@n@iB`@kANc@Lc@ZaA\\kAV_APi@Le@r@qCdA_ELi@Jc@ViAJc@Li@^sBRiARkARqAHi@NiAVyB\\{CLiAFw@JkAJqAN{BHmAFmABo@JwBBe@F_CHuBDkD?g@@e@?g@?}C@aCCuBAyAA{@C{@ImEGiD]cKSkGMmFIaDAqAAaB?mA?oA@kDDuBDoBHyBFsAJaBHqATkCPyAFi@Hi@Lw@NaARgA`@kBnAsEPm@`@mAhAcDv@kBxA_DR_@rAuBjBiCT[zCuCtAmAh@]pDcCrBwAx@i@`@WbIgFJGNKVObAk@xA{@bAk@dCwAjDqBpC_BTO~@e@t@a@`EwBzCcBz@c@|Aw@`BcA~AcAdBgAvA{@lHgEbCwAbC}AhCgBhBsAnAcAZYf@a@b@c@d@a@~@_A|B}BhAoAn@w@vCaEp@aAhAgBlBgDtAaCXk@jCcFFOBELSFO|EmJ`AkB"
                     },
                     "start_location" : {
                        "lat" : 42.50445089999999,
                        "lng" : -5.5935424
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "3.4 km",
                        "value" : 3430
                     },
                     "duration" : {
                        "text" : "2 mins",
                        "value" : 107
                     },
                     "end_location" : {
                        "lat" : 42.3060834,
                        "lng" : -3.7556165
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eBU-30\u003c/b\u003e",
                     "polyline" : {
                        "points" : "a{jaGvy`VDG\\o@|@aBnA{BdCgENWNWn@cA`AyA`AyAbAuAbA{Aj@y@dAsAbAqAl@w@lAqAfAoAfAoAhAmAr@u@hAkAjAiAjAgAdB}AhB}AzBkBPKNMtDqClCiBrA{@pAy@rBmAfC{A`DkBv@e@xAy@rAw@rA{@lBoAhA{@v@o@rBeB|@}@n@q@x@{@vBcCvBiC|AmBzAaBx@y@jAgAt@o@j@a@ZWx@i@^Ut@e@z@a@x@]z@Yz@S|@Ox@I\\CXAd@A`AA|AB^Bl@D"
                     },
                     "start_location" : {
                        "lat" : 42.3315298,
                        "lng" : -3.7777171
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "12.0 km",
                        "value" : 12047
                     },
                     "duration" : {
                        "text" : "7 mins",
                        "value" : 404
                     },
                     "end_location" : {
                        "lat" : 42.3341226,
                        "lng" : -3.6241091
                     },
                     "html_instructions" : "Keep \u003cb\u003eleft\u003c/b\u003e to stay on \u003cb\u003eBU-30\u003c/b\u003e, follow signs for \u003cb\u003eE-80\u003c/b\u003e/\u003cb\u003eE-5\u003c/b\u003e/\u003cb\u003eA-1\u003c/b\u003e/\u003cb\u003eN-234\u003c/b\u003e/\u003cb\u003eMadrid\u003c/b\u003e/\u003cb\u003eBU-11\u003c/b\u003e/\u003cb\u003eCentro ciudad\u003c/b\u003e/\u003cb\u003eBurgos\u003c/b\u003e/\u003cb\u003eAP-1\u003c/b\u003e/\u003cb\u003eN-120\u003c/b\u003e/\u003cb\u003eAeropuerto\u003c/b\u003e/\u003cb\u003eN-1\u003c/b\u003e",
                     "maneuver" : "keep-left",
                     "polyline" : {
                        "points" : "_|eaGro|UjAGv@Eb@Eb@It@SXK`@OZQ^UVOVSLMLKTYX[Za@Ve@NYVe@Xy@Pi@Lg@He@F_@BOD]Dc@Fs@Bi@@g@?kAAm@Cg@Cc@Ea@E_@O{@I[I]I_@Qg@Oe@g@qAk@yAQa@Qe@IUY{@Oo@Ka@a@iBIi@Im@Gi@AICIO]_@gE]cD?COkAUqBOsA_@_DMaAMaAWeBSoAiAkHSgASkAUoAy@wEYkBSmAQmAQmAQmAIe@Ge@QmAOmAOoAOmAOoAOqAMmASoBMqAGk@MoAKmAWqCKyAEk@OsBEm@?AIuAIqAIqAIoAIqAIoAEi@Eg@IqAMoAKqAKoAKoAMqAMoAMqAMmAOsAE]UgBE_@OgAUeB[_CQoAQmASoAQmASmAIe@QeAKi@Ik@Ic@Kg@SkAg@kCOu@Ie@Kg@Ie@Ke@Ke@Ke@Ke@Ke@Ke@Kc@Ke@Ke@Ke@YkAYiAMc@Ok@Ma@Oc@Oc@Oa@Qa@Qa@Qa@Q_@S_@Q[ACS]Q[g@u@SYU]MOe@o@W[WYWYUYW[WYU[U]U]S]S_@Qa@Oa@Oc@I_@Sy@Ig@E]G_@C[GeAAYA[CoA?cA?Y?[?Y?o@Ao@AsAGiCEiAEi@Ce@Eq@Ek@McAO{@Q_Aq@aDOo@Me@Sk@IWYu@o@sAc@}@e@y@q@gAw@eA]e@Y]iAqAMMa@_@q@o@s@m@e@]uAeAg@]eBmAgBsAoAcAi@i@g@g@cAkA]a@s@_Am@}@a@q@{@yAcA{BsAqDi@aBa@aB_@{AYoA_@mBk@gD_@}BGg@Ge@QuAMwASuBKaAEs@OgBOcCIqBGkBGyBEuBAuBAmCBmC?]?C?[BwAHuBJiC?ABW@YHgBD_AHcALaBJoALmARoBLeAHo@Hm@TuB\\uCLoAFu@Ba@J}ABg@@Y@[DsBBcBAeBAuBCcAE{@MqBKwAGm@Gm@MiAU}AUuAMq@UcAYmA[mAW_AWw@[}@e@mA_@}@q@cBm@wA{@wBUo@Yu@]}@a@iAUo@Y}@[aAMe@[gAQk@U}@Mk@U_AMk@Mg@Ow@Q{@Ie@Ic@Ie@Ga@Ie@My@M{@Gc@Kw@MgAEk@Ea@Gk@Ee@Eg@Ec@Ek@Cc@Ee@Cc@Ci@Cg@E}@Cg@A_@Cs@CcAAe@Ak@Aq@CeAAs@?]?IAe@KoJA_BCeAAsAC{@AqACkACqAE}ASsGEcAGeAE{@GeAEy@G{@KuAGaAGs@Is@Iw@KeAGo@Iu@QqAK}@QiAQmAOeAQiAMo@QeAO}@Kg@Mq@Oy@Ms@Kc@Ii@Ka@Ke@Ke@I]Kc@Mc@Mg@Mc@Oi@Ok@_@uAgB_GcA_D"
                     },
                     "start_location" : {
                        "lat" : 42.3060834,
                        "lng" : -3.7556165
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "56.2 km",
                        "value" : 56225
                     },
                     "duration" : {
                        "text" : "28 mins",
                        "value" : 1692
                     },
                     "end_location" : {
                        "lat" : 42.6220203,
                        "lng" : -3.1173367
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eAP-1\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "polyline" : {
                        "points" : "gkkaGtybUeBwFOi@Ok@{BoJm@yBqAyEg@iB]yASy@iByHkAwEw@wCW{@a@oAk@iB_@aAq@iBKUy@kBw@eBy@}Ac@y@Yi@w@qAkAgBm@y@k@y@]a@aAmAo@s@aBeBiAaAiAcAw@q@USWQWS[UoA{@eAq@}@k@y@a@u@_@qAm@uAk@sAe@uAc@]IgAYmAY{@Qi@K{@Og@Gg@Gw@K}@Ku@IYCeBOyD_@kBUy@Qs@OcAYkAa@cAa@]OuAq@s@c@iAw@s@i@g@c@{@u@g@g@q@w@Y[SWq@}@{@oAg@y@qCuEqAyBmAoBi@}@k@w@_AwAaAsAi@s@m@s@o@w@eAkAgAiA[]wAoAWYQMCCwAmAaBmAmA{@qAy@uAy@o@_@uAs@sAo@oAi@a@QoAc@y@Y{@Yy@U{@U{@UoAW{@Q[GyAUuBY{@K{@KwAQ{@I{BWuAOwAW}@SuAa@yAg@iBw@qAu@s@c@OKk@a@WSq@k@USo@m@gAgAiAiAgAgAsAsA{@s@oAcAw@g@q@c@mAq@}@a@u@[]MaCs@q@Om@Mc@GWEg@Gw@ISCy@EyAGuCK}@Eg@Aa@Eq@Ea@Ew@M]Es@M]Iy@Sa@Mw@W[KSIcAc@g@Wg@W]SGC_Am@_@YOKMIeA{@o@m@q@s@k@o@c@g@a@g@U[k@y@U_@e@u@g@}@Wi@[q@Se@Se@]{@a@eA]cAkAoD}@sC]cA]cA]aA_@cASe@Uk@Ys@c@aASa@O]g@_ASa@S_@e@y@U_@QYk@{@]g@c@o@i@s@o@y@cAmAaAkA}DmE_BgBm@q@o@u@_AiAm@s@m@u@yAmBi@u@m@y@i@w@Yc@e@s@i@}@e@w@i@}@c@u@We@c@{@g@aAw@aBc@_Aa@cAa@_As@iBKY_AoCm@kBk@oBe@cB]qAU_AIc@gAgF]qBQgAc@}CYwBUuBMmAQsBKwAKoBOyBGoACo@IoBK_CIuBO_DIqAEw@Gg@WoCQkAGc@QeASkAMk@UcA[kAm@sBMa@EKKWM]Qe@_@_Ai@iAs@wAw@sAm@aA_AwAqDmFKQKO}AaCQYk@_Ai@}@y@uAy@}Ae@}@i@iA_AwBs@cB}@_CSg@}@aCa@gAqAmDaAiCeAmCo@aBm@}AeBgEc@eAmBoEaBmDwBqEs@sAi@_Ak@aA{@qAaAwAk@w@gAqAo@w@o@q@y@w@g@e@{@w@o@k@cBsAqDyCqAiAYWWU[]}@aAm@s@k@u@k@y@i@{@w@uAy@aBa@_Ae@iA_@aA_@kAg@iBYiAS_AOu@UkAWmBOiAIq@YsDs@gJa@oEMw@Kw@Ig@UmAUeAYiAYiAUy@_@oAa@eAsAcDe@aA_@q@y@sAs@eAgAuAu@_A_AaAk@k@}@w@o@g@[UqAy@kAq@a@SUKWKu@WsAe@oAY}@UcASg@IyB]_@GgDk@k@K[Ie@Mm@SwAi@q@Y{@a@s@_@w@g@q@c@m@e@g@c@e@_@m@m@o@s@s@y@[_@SWYa@Yc@s@cAiAgBc@u@mByCk@}@gAaBw@iAOSkAeBw@kAkAeBkCuDmAaBeAwA{AoBgA}AcAwAy@gAo@{@k@y@Yc@[k@e@y@Ue@Ue@c@{@_AeCc@uAs@cCi@sB[sAYkAi@qB_@mAa@qAi@uA_@aAa@{@a@{@a@y@c@w@yAcCa@u@S[QYsAyBw@_BSa@Ug@o@_B_@cAk@iBa@wAS}@c@qBa@cC_@mCSmBIaAIsBGwBAk@Am@?uB?mA@gDDqC@aA?qA?kACgDGqBKyBQmBMyASyA[qBa@wBc@iB[mA_@iAUu@]}@q@iBu@kBy@mBiA_CoAcCaAeBuA{BcA{AaByBqBcCiBqB{ByByBgB_C}AeAm@mBcAwCuAqBu@aAWe@Mg@OoCs@wBi@i@MkD_A_@Mq@Uo@UsAg@WMwAq@gB}@qCiBg@]iB{Aq@i@gAcAaAaA{AcB{@cAW_@u@aAkAiB}@_Bw@wAkAaCiAeCgA{C}@gCaAcD}@aDaAgDo@mB_@kAUo@w@gBYg@_AkBmAqBeCuDmAkBu@qAu@yAi@oA[u@]}@k@cBy@wC]uAa@sBSqAIq@OuAOgAIaAKsAe@kGU_DQsBSmBOqAWsBSyAaAiFOs@YgAo@sCEQSs@G[sByHg@oBm@kCk@wC[qBQeAUeBQ}AWyBQwBIcBM}BGyCGkDCyBEeEGwHGuEOsHMmEGwBIuBQoC[_FUoC]iDYuCo@cF_@yCe@iDs@sEeAoG{AoIkB{IqA{FmCmKaAkDsAsEgAqDgBkFkAiDqAgDaAiCcBeEiCkGyAmDiDuH{A}CyEuJeBeDw@iBu@cBOa@e@kAiAmDi@gBe@cB[oAc@iBUkAYsAW}Aw@{Eg@iEm@mFYmBc@qCYeB_@oB]aBu@aDe@_Bm@{Bs@sBaBaEiByDeB_D}@wAS]U[i@w@k@u@y@aAq@w@gAmAs@y@cAcAo@q@gAeAeAgAc@[_@Yg@]}AiA}AoAy@i@{B_BwA_Ay@e@SMoAu@y@c@[SsBeAu@_@qBcAkCoAw@a@uB_AmCqAiCmAyAs@cBy@iCoAwAu@qAq@mAo@]Su@a@qBeAwBmAmAw@qBoAoAw@iAs@}@m@iBmAw@m@sA}@uAeAoFkEcEgD{A{AgCmCq@o@EGc@c@WWEESSuA{AkBwBoCiDeCqDgBmCyAiCQYiAmBmAoCq@}AiAwCGWCGK_@uAkEaAsDo@iD]iBO_AWeBm@oFWgDc@eJGcA?EG}@MsD[sHKiAU_CG[[wBUeAUiA]_BKg@Ss@k@gBQg@Oe@Ys@i@mA}A_DCIMS]m@mHgL{EmI{@iBa@{@sAqC]w@mAaDaBwEwCmJ{@uCmBaHqAoEuD_K_D}HqDyHkC_FeDoFqB}CyAuBOQAAOUwAmByAiBqB_CmAqAiBiBiBeBaCaC][}BqByBoB{BuB}BsBeBeBsCaDaBuBu@eAeBgCsAwBcAgBw@wAe@_AIQ?AKSgDeH_HqOcD_HsAmCgBgDqAuB_A}AqAsBKMkAaBgA}A_BuB}AoBaBkBoBuBeCgCqBmB_A{@mB_B}BiByBeB_As@{AmAgM}JuBgBqHqGc@_@e@a@oBkBsDqDiCkCcBkByBgCiC}Cg@o@cBwBcAqAm@y@yAsBU[cAwAcCsDaA}AeBqCqC_FWe@y@{A_@u@}BkE]q@g@_AEIKUoBaEqBkEqAsCkAkCcCsFy@iBWi@c@cAWk@a@}@e@gAc@_AgBcEaAsBu@}Aw@cBc@}@i@gAo@qAq@sAg@}@y@aB}AmCi@_Ai@_A{@yAaA{Ai@{@cAwAc@k@g@q@[a@{@cAm@s@]_@_@a@yAyAiAgA_Ay@eA{@gA{@}@s@mA}@yEcDsBwAk@e@s@i@{@q@}@{@aA}@c@c@]_@_@c@o@u@c@e@c@k@a@i@a@g@c@o@i@w@_@m@We@g@{@i@cA{@eBe@cAWk@_@{@[{@c@oAWu@Sq@Uu@YaA[oAe@mBYkAa@oBo@yCw@kDg@wBc@cB[mA[gAYgAY}@e@_Bm@kB}@eC_@aA_@{@k@sAm@sA}@kBcAoBkAuBc@s@[g@g@w@k@w@OWW]m@y@_@g@g@o@{@eAeAiAa@e@a@a@aAeA_BaBu@u@uAwAiAkAk@m@m@q@_@c@c@k@a@g@s@cA_@k@_@m@]m@k@cAS_@e@}@Yk@Wm@e@iAUk@]}@[aASo@Uu@Qq@c@cB[{AOw@QcASgAW_BYsBe@iDe@_DU_BOcA]oBOy@Kq@U}AIe@Ia@[{AYsA[uAa@wAqAkEuBiGy@}B_BsEo@kBk@gBa@qA]gA_@sAYcA[kAWiAQw@Ou@[aBYwAY}AOeAOeAKo@a@_DQmAUwBMuAKoAW_DEu@AO?MCg@Ca@Eq@G{AEsAQwKEeIAuHI}FEkBG}AQsCGoAEq@IiAKyAU_CSqBi@cEe@{CAGQqA{@iFaAaG[uBQqAOsAKmAC_@Cs@I{ACcACk@CsA?_C@}ADqA@c@Do@@[@UDe@NwBXcCHm@V{Aj@_DXgBz@wFVqBPiBFcANiCDeC@g@?_CAsAA}@CmACcAEqAMeEE{AG_D?_A?_ABcBDiADiAFqADk@J}@VsBZkBx@eFV{AFa@NkANmAFg@p@kFn@iF^mDRyBPcBBc@FcABc@Dg@H_CDiB@{@BsA@eA?uAAcAAuCCyBCmAIeCSkFGcAo@qIO{A"
                     },
                     "start_location" : {
                        "lat" : 42.3341226,
                        "lng" : -3.6241091
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.7 km",
                        "value" : 672
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 46
                     },
                     "end_location" : {
                        "lat" : 42.6244318,
                        "lng" : -3.1135684
                     },
                     "html_instructions" : "Take exit \u003cb\u003e4\u003c/b\u003e toward \u003cb\u003ePancorbo\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "maneuver" : "ramp-right",
                     "polyline" : {
                        "points" : "srccGjz_R?g@?KAMAUQiBMeAE]Ms@Ic@QeAKu@ESAU?SAQ@MBk@Jk@Li@J]DUDQBO@M?K?K?KAMAMCICMCIEIEMGIIICCECIGKEICKAG?I?G?I@KBIFEBGDOLKHi@h@aAbAQRGHONIHGDGDGFGDGBG@I@SA"
                     },
                     "start_location" : {
                        "lat" : 42.6220203,
                        "lng" : -3.1173367
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.3 km",
                        "value" : 277
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 63
                     },
                     "end_location" : {
                        "lat" : 42.62646669999999,
                        "lng" : -3.1117303
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eN-232\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "polyline" : {
                        "points" : "uadcGxb_RI?I?MCICGCGC[QQOc@_@UUYYk@i@]YyAsAU[OYO_@"
                     },
                     "start_location" : {
                        "lat" : 42.6244318,
                        "lng" : -3.1135684
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "20.5 km",
                        "value" : 20481
                     },
                     "duration" : {
                        "text" : "13 mins",
                        "value" : 789
                     },
                     "end_location" : {
                        "lat" : 42.5569928,
                        "lng" : -2.900962
                     },
                     "html_instructions" : "At the roundabout, take the \u003cb\u003e1st\u003c/b\u003e exit and stay on \u003cb\u003eN-232\u003c/b\u003e",
                     "maneuver" : "roundabout-right",
                     "polyline" : {
                        "points" : "mndcGhw~Q?E?CAE?CACACAECCMyAIqA@i@BY@U@O@KHc@FWJa@VeADOBGLi@@AT{@X}@Xm@Pa@JWp@mAJOjc@yt@lAmBlAkBrAsBjCcEhBqCnByCjDmFjAgBdAaBl@_AVa@N[LWn@{AL[V{@Ne@Ni@Pu@VeAXwAt@gDdA{E\\yANs@XiAZqALg@Jc@Pq@HUTu@Rg@n@cBl@yA|@}BVo@Vq@Pe@Ne@b@oA`@qAj@cBh@gBh@_B`AyCjAqDt@cCNi@Nc@Rg@`@uAr@yBdAcDv@_CnBiGt@_Cj@iB^iAr@}BVu@DOf@yAzCsJh@cB|@qCpA}Dl@kBj@kBv@aC`A{CrA_Ep@wB\\eA`@oAj@oBJYRk@^iAd@yA~AeF~@qCt@{Bf@{An@oBrAeEj@gBj@eBr@aCn@qBb@yAVw@p@uBj@cBd@wAp@yBpBgGfAeDl@kBjAuDTs@`@iAv@eCz@mCNe@Tq@\\eAb@qAb@wA\\cAHYFUFUFYH_@Fc@Fc@Js@LcAJ}@Da@Fk@He@D_@F[F[FWFUFYJ[Nc@La@`AqCf@qARm@Xw@Tq@Z{@L[N[NYPYJQPSPQHIPOTSt@o@RO\\[n@g@h@e@p@i@`Ay@z@q@f@a@p@k@bCuBbBuA\\Y~AsABEvAiArAiAJIVUdCsBvAmA`Aw@\\[v@o@\\[f@g@TYr@eAr@gAf@{@b@q@LUh@{@^k@NONQRSVSVSd@_@TSTOXW`@]DCn@g@v@o@d@]l@e@\\YXYX_@PUJUP]La@Rs@VcAVaARq@\\qALa@JWTa@R[Xa@FGT[RWNSNUJSJUL_@DQFUHg@PuAHq@J{@DYBOBKDMFOLWT]Z_@Z_@TY\\c@Z_@^e@\\a@PSPYP[JUL]XeAd@cBh@gBXgARq@\\oABGJYPe@f@mA^{@|@sBv@kBTk@Ti@Pa@JSHQHOHMDKLQPUX[NOh@i@f@e@XYPQLKFIHKDGFIDIDMDKDKBKBK@K@E@O@O@K?M?O?]?q@Am@?K?S@Q?O@WB]D_@BYHe@FUNg@Pe@`AsCNc@HUj@_BjAcD|BuGt@uBdB}EpByFZw@x@gCBIb@qApDmKzC{IrCqIHGDMj@{Ab@kAVy@X{@jAcDh@wAj@eBv@yBtAyDt@aCf@{ANc@@IBO`DaJxAgE|ByGtBaG\\eAvIyVvIyVPe@Pg@dCkHz@gC^kA|@qCZgALg@Nm@Nm@Pw@Ny@Ny@PkADa@RsAJ_ADc@Ds@@KB_@Bi@@c@@c@@c@@c@@kA?kBCqBCaAGyAK_BCa@CUIcASoBKw@QqAOgAOiAQqAMq@M{@QeAKo@CQYiBGa@G_@QmAQmAMiAG_@E_@Iq@Ii@M_AI}@I{@Eq@Eu@Gs@AY?M?OAO?IA_@As@?g@?y@Bu@@q@BeABi@D{@Do@HoABk@Dm@D_@Hs@Hq@@KFa@RyADWDa@PmAHg@BONq@Ni@Ru@V}@Le@@CLc@HQXy@Xw@d@kAd@kAZw@f@mA@A`@gAL_@Re@Ne@j@{Ah@_BPi@ZcAXeAd@iBT}@d@yB^mBJi@Lu@Jk@NaALaAJ{@Hy@Fq@HaAXiDPiE@UB]?c@@e@@w@?o@?wBAyACaACu@GkAKgBO{AMgAK{@OgAa@{Be@oBw@aD]kAm@aB}BaGSe@m@sAgAcCo@_Ba@}@_@}@sD_J[w@O]IQ?Ai@mAAEIQCEKWg@oA}@sBiA{Ck@_BQc@k@_B}@yBYu@k@uAgAyCw@mCCIAGKe@e@qBk@kD_@{C[oDSiFKqEFuEJcEXiDf@wEp@}D~@}D`AuCBKRo@@C`AiCXq@\\aA"
                     },
                     "start_location" : {
                        "lat" : 42.62646669999999,
                        "lng" : -3.1117303
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.3 km",
                        "value" : 310
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 20
                     },
                     "end_location" : {
                        "lat" : 42.5547506,
                        "lng" : -2.8988347
                     },
                     "html_instructions" : "Slight \u003cb\u003eright\u003c/b\u003e onto the \u003cb\u003eN-126\u003c/b\u003e ramp to \u003cb\u003eAP-68\u003c/b\u003e/\u003cb\u003eLogroño\u003c/b\u003e/\u003cb\u003eBilbao\u003c/b\u003e/\u003cb\u003eHaro\u003c/b\u003e/\u003cb\u003eCasalarreina\u003c/b\u003e",
                     "polyline" : {
                        "points" : "e|vbG~quPRIJSJUzAcCd@m@`@c@`@_@d@_@f@[VMXKNGZMNGPK"
                     },
                     "start_location" : {
                        "lat" : 42.5569928,
                        "lng" : -2.900962
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "2.1 km",
                        "value" : 2117
                     },
                     "duration" : {
                        "text" : "2 mins",
                        "value" : 117
                     },
                     "end_location" : {
                        "lat" : 42.5622681,
                        "lng" : -2.877078
                     },
                     "html_instructions" : "At the roundabout, take the \u003cb\u003e3rd\u003c/b\u003e exit onto \u003cb\u003eN-126\u003c/b\u003e",
                     "maneuver" : "roundabout-right",
                     "polyline" : {
                        "points" : "envbGtduPD@F?DAF?D?DAB?@AB?@?BA@?BA@A@?@?@A@?@A@A@?@?DCBCBCBCBCBCBEBEDGDIBIBIBIBK@IBK@e@Ai@Ig@M_@AAS]UU[QYKO@I?G?GEMGIEKGGGMMOY]k@Ws@k@_BsAcEW_AcAwDg@}Am@aBo@eBYy@]aAq@iBg@}AQi@Ka@{@{CKY[eAUu@y@mCg@{As@qBa@yAa@sAWs@q@}Ag@cAm@qAg@uAe@qAISEOESEWESWgBQoAKaAQwAEe@KiAC[Aq@E{AEsACc@E]M_AO}@Ga@Ic@G[G]EOAIA]AGW_B"
                     },
                     "start_location" : {
                        "lat" : 42.5547506,
                        "lng" : -2.8988347
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "1.0 km",
                        "value" : 1025
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 88
                     },
                     "end_location" : {
                        "lat" : 42.5589125,
                        "lng" : -2.8741259
                     },
                     "html_instructions" : "Take the ramp to \u003cb\u003eLogroño\u003c/b\u003e/\u003cb\u003eZaragoza\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "polyline" : {
                        "points" : "e}wbGv|pP@kA@[BKBMT_@HQx@aAnAeBn@{@r@c@TIlAYhAAf@Jj@TXTVZ^n@Ln@Hd@Bf@@R?ZEn@Kr@Md@Ob@OXWZ[N[BS?IEIEEEKGMSGOGSEWAY@[FUFQd@gAZk@Va@b@w@d@{@Ri@`@aALe@Hq@"
                     },
                     "start_location" : {
                        "lat" : 42.5622681,
                        "lng" : -2.877078
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "205 km",
                        "value" : 205362
                     },
                     "duration" : {
                        "text" : "1 hour 42 mins",
                        "value" : 6135
                     },
                     "end_location" : {
                        "lat" : 41.6777282,
                        "lng" : -0.9340364000000001
                     },
                     "html_instructions" : "Merge onto \u003cb\u003eAP-68\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003ePartial toll road\u003c/div\u003e",
                     "maneuver" : "merge",
                     "polyline" : {
                        "points" : "ehwbGhjpPpBiFj@wAtCqG|A_DhBkDrBwD~BgEHOd@{@xAqCbBcEXw@`AqCfAqDRw@T_AJg@VkAj@oCl@uD^sCf@wDn@kF|@uGt@_Fj@iDz@iEt@kD\\sAf@qBp@aC`AwCZ}@v@oBNa@b@aAb@}@z@eBj@gAx@oAn@cA~@uAt@cA`@g@h@o@\\_@dAiAx@y@VWp@m@TSTQJIJIFG~BcBhD}B~AeA|BaBz@o@VUTO~A{ApAuA`AeAd@k@b@m@bAuAp@_Af@w@^q@l@eAtAmCb@_A`AyB|@_Cz@iCf@cB^uADK?C@Ev@iDd@cCRkARwAXaCX}CHaBBg@Be@JuCBaB@eCAaBCsAC}AGyAKqBIeAAMMwAGa@CSASIo@k@}D]kBa@kBWeASy@U{@GWs@mCcA}Dg@uBSy@g@eCg@gC[iB[uBOaA[mC]wCIcAOuB?AOuCGsB?OA_@?MAo@@}B@k@@o@@u@JaCFaAHkARkB\\gCLu@TuATeAHYPu@Ps@h@sB|@aD\\uAVeA\\kBF_@PeA@IPqAJ}@X_DX{EPaDHkALmBL{ALsAFm@Fi@Fi@PqANeAToABQb@_CPw@Jg@Ja@j@_Cp@aCtA_Ef@gAd@cA`AiB|@}Al@}@Zc@hAqARUpAqAjAcAbAw@`Am@TOVMZQXMzAs@dC}@pBs@t@Yz@_@\\Ov@a@j@YtA}@^WlAaAdA_Ar@s@nAyArAgB~@{AnA_CfA_Cn@cBL_@Z_AT{@Tu@`@gB\\}AJi@DYN_AL{@Jw@JcAH{@BWDi@Di@Ba@Dw@HuB@i@@eA@kBC{CE}BEiCAsB?s@@mBDiBHyBHmATmC\\sCTuAJo@ViAVmAZkAZiAVy@Z}@\\}@^cAd@aAj@gAf@_Ah@{@j@}@t@eAvAmB|@gAlBsBn@q@n@o@v@w@|A}ALOPQnDmD`BiBnAuAzAgBbAkAhAwAXa@Z_@b@o@rByCvAyBtA{BbAeBx@{Az@cB~@iBx@cBTi@dAaCz@uBbBkEp@iBTq@z@cCjAkDf@}A`@iA\\cA`@mA`@kAbAsCl@aBp@iBr@mBv@mB^aAx@mBb@cAd@gAt@cBh@kAr@{A~@oBfAwBfBkDl@gAnAcClCcFbBaDdBiDh@iAZq@Zq@bA_CrA}ClA_DhA}CnAwDhAyD`AoDb@cB~@}D^gBn@yCb@{BfC_OrBkLfAwFd@}BZuAf@sBf@mBb@aBZ_Ab@uAf@yA|AeEdAaCx@iBt@yAr@uAxAkClBoD|@yA|@_BxBuDfAoBvCiFdCcFdA{BVo@Xm@xAmDhB_FxAkElA_En@uBhBiHZqAt@aDp@oDd@cCn@uDtAqIb@oCn@gE`@aCNs@PeAP}@\\yAXuAXoA\\uA^qAZcAl@mBNe@L[FUrAuDxC{H`BoEr@uBl@eBr@}BXeA^}AVeAPw@h@mCh@oDLu@NmANkAHu@TcCBYJmAHsABe@LyCFiBBg@@k@@eB@mCA}AC_CAm@Ac@Cy@EiAGwAE}@MeBKwAOkB]}DEc@Ec@KgASaCEg@Ca@KyAG}@G}@EaAEk@IoDA[?e@Aq@@aB?a@@a@@e@@e@Bi@FmAHsAB_@Fo@Da@Hy@Hq@Fg@BQD[VwAP{@Ny@VeAVgAL_@^oA\\cAp@gB`@_AVk@v@}AXi@fAmB~@{AHOnAgBbB{BdAsAtA_BrAwAv@u@Z]b@_@fA}@dAs@lAu@VMpAo@hAe@n@U`AYfAWxDq@vB_@v@M`@K^I^KXId@O@ApAe@x@]JGvAs@fAq@bAs@JKLIRQz@u@|@}@d@e@v@aAj@s@j@{@j@{@n@iAv@{AVk@v@gB\\aAr@{BNg@Le@XeAXoAHa@Jk@VyAJm@NkAD[DY\\cDDe@JqALeBFcANiC@_@F_AHsBF_BJcCD}AHiDBo@BeBBqB@mABaC@kBAiA?c@?q@GqDUmEEm@Go@Ei@CWCWGi@Gg@Ik@Mw@Ga@Ig@UcAa@iBMi@WeAQk@i@cBo@eBwEuLOc@Qg@Qe@Og@Si@Oi@Ok@Ok@Ok@WgAMk@Ie@Mm@SsAIi@Im@Is@Ee@MqAGq@Cc@Ci@Em@Ci@Ag@Ci@Ao@?qA?w@?mA@e@@k@@i@Bo@Bg@Bi@Dk@Bi@Dq@De@Fm@Fg@Fg@Hq@Fc@Jm@Hg@Ha@Ji@No@TcANq@J_@Ng@Ng@Ng@\\eATk@Nc@b@aAN]n@qAv@uAj@aAj@y@t@cA`AsA@CdAuAfA}AR]xAaCf@}@b@y@Zq@Zw@n@{A^cA^eA`@oARu@Ru@Nk@Ns@ViATmAHa@PaABQR}APqALoAFo@JgADu@HwAFqABoADyABeB?eAA{AAq@CwAEqAG{BKcCGkDAyA?sA@yA?i@Bg@DyABm@HmADg@L{APwAFg@PoAHk@H_@VwAJe@No@VcAPm@Ja@Ne@Pe@Pi@\\_APc@Pc@Vg@`@_Ah@aAVc@f@y@V_@T]T[V]V[X[V[XWV[XWZYXWXWXU\\W`@[jAy@zAcAt@k@fA{@b@_@ZYVU^a@l@s@\\_@V[T]Zc@h@u@Xe@h@_A^q@\\s@f@aAd@kA\\}@Rk@j@eBNe@Nm@t@qC~AaHl@iCHa@Ja@Nk@ZiAdA_EZeAPk@Nc@`@kALa@Ri@j@{Ah@mAf@kAb@}@j@iAP]j@gAj@eAXc@\\k@Xe@j@}@Zc@PWn@{@p@{@`BoBj@o@`CeChD_DbB{AhBaBpBaBdB{AjAgAhBgBbAaAx@{@x@{@p@u@v@_Ad@k@dAwAdA{Al@_Ap@iAb@s@x@{AbAqBh@iAh@oA`@_Ap@gBj@{Aj@cB^mA\\gA\\oAZkAXmAXkAXwARcATuATsAZ{BZ_CRaBN}AH}@Fc@NgBV_DX_DP}AV_CVmBDW?CDSN_AP_Ad@}BZoAVgAj@uBv@wCj@wB~@mDx@sDNw@DUTsAZiCRuBDm@Di@DaADgADqABu@?u@?gBAaBEcBGuAGeAOuBQmBOkAM}@QgAm@wCUgAQw@i@yBScA_@cBKk@Ie@WgBOmACYCOCYI_AEi@Em@Cm@Cg@Ew@GuBCmACuAEuAEsAEw@GkAGq@Gk@MsAIs@Gi@_@yCWoBGo@MiAAMEm@Ec@G_AE_AEy@Cs@AkACw@?i@?mA@q@@}@DuA?WBk@FgAJ{AB]Fi@D_@L}@XwBXaBN{@^mBX{AZuBNgAD]J_AHy@HkADg@FeABs@@U@[DoA@{@?o@?A@qACqBAc@Ag@Cg@GyAEi@Ce@Gm@MqAMeAIq@Im@E[W{AUiAWmAo@yCk@eCSiASgAOgAG]Ky@Gk@Ec@CUCWEi@Ei@Eo@EcAEiACaAAsAAk@@kA?e@@q@?c@DqADqAPwDJaCDsABmAJsF?kBCaAK_FEiAI_BGuAEi@McBKuAOeBC]E]Gw@KkA_@qEWqDIgAEu@IiAMeCKyBCs@Cg@Cg@A[AOOcG?]Ae@ASAqA?k@AyAC_H?wE?}@?_@AoH?Y?iB?M?C?Q?a@IkRKqJIwGGaDEsBGcCE}AMsEIsBM_EOeEMuCKuB_@aIGcAEu@WuEYwEa@cG]kEQ_CM{AY_DSaC]qD]iDU}BWcCOsAk@qEWwB[}B[wB_@cC_@}B_@{Ba@uBMq@Ic@c@qBg@wB[oAQq@Sy@q@_Cm@qBq@qBQc@Oc@u@oBe@mAo@yAWk@a@{@o@qAg@aAYg@y@{Am@eAi@y@Wa@m@}@qBqCs@_Am@s@Y]kAoAi@i@aAaAgBaBuAkAkAaAgDkCiCuBeCsBqAiAo@k@_@]mAmAqBsBm@u@gAuA}@iAa@m@]g@u@gA[i@w@qAUa@e@_Ay@cBUe@a@}@q@{Aw@mBYy@a@eAa@mAi@aBW_Ae@iBa@_Ba@gB]cBa@yBYcB]_CI_@E]QuAIy@MeAUiCM_BOsBGgACo@KyBCq@C{@Cy@Ao@?g@AS?_@?YAI@mBBmBBcABcAD{@JuAJcAJeAL_AHm@VsANaA`@qBPeAN_AHq@Hs@Fu@H}@Bu@D}@BsB?uACqAEw@Cs@Ek@Gk@Ei@U}B[_DIuAI{AEeAAg@CgBAuA?w@BkABwADmAFcAD{@F{@RqBFo@PqAPoAHk@Fc@TuAJm@TgAJi@XwA`@qBb@sB|@gEb@sBXyAb@}B`@}B\\uB\\{BHk@Hi@PuAb@iDViCLiADg@TcCJsAVoD\\aF@QFsAB_@PeDHaBNiCLoBFs@HqARiCPeBLiAHo@RwAFg@RsANeAJu@r@gFPmALcAVyBJmALcBVmEJaCDuAJaCF{AJ_CJ_BN_CT}B\\yCZ{Bh@eD`@cC^_CLcAHo@BWL_BDs@Bk@D{A@sA?e@?k@?_A?AEqAIuAKuAGi@Gk@E[Is@UuAKi@G]Oq@e@iB]kAk@sBi@uBQs@a@kBa@oBe@kCSmAQqAYcCOcBI_AC]Ce@EoAAu@Aw@?eB@mBFsBDcBFaCD}C@mBAaBKcGG{CC{@?k@Aq@?{A?q@ByABqAD}@Fw@Dm@Bc@Dm@Hy@Fo@He@Fg@Hm@Fe@VyAPy@No@Nc@FUDMpB_G\\_Af@qAdAeCj@oA|@cB|@cB~@}Az@uAfAgBjBgDz@gBjAmCr@cBb@kAb@qA^gA^sAd@iB^yAXoAVqATsAToAT}AJy@Hu@NoA^eDHu@?AFk@@GDi@d@iFZaDVuBPeAJk@F]FYPy@Nu@@AFYH]R{@Tw@Ts@Ng@L[Tq@JYt@eBXo@Zq@JWNWLUd@}@BENYf@}@HMPULU`@k@NUNUPULSd@m@NUBCFG?AV[V[DE`AgAv@w@x@y@`A{@^_@HGRSRQ\\]tAyAbBwBpBsCb@s@Va@bAoBN[Xo@JWLYL[JYn@_B`AqCzBkHbDyKv@kCX}@Tq@bBmFJYTs@JYTu@JWJYJ[HYXw@Pe@Nc@HYVq@j@}Ah@wA`@kAp@eBb@gAVs@n@eBt@gBb@gAVo@pAaD|AkD`AuBZo@Tg@LULWLWNW\\o@Zk@NWf@{@\\k@l@aAnAiBt@eAPQ`@i@NSNQr@{@JMlAsAfAiAt@s@\\[~@{@HIPOPOf@a@PMROROPO`BiAnCeBTMRMxAw@f@YNGv@e@b@U|@e@tBiApBgAVM@AB??APIz@e@fBcAjEkCpBqAlAu@bBkAlBsAxAiAp@i@jA_Aj@g@lC_CjAcAjBkBdAeAlAmAjAqAp@s@v@y@t@y@n@u@hAmAnAuAfAkA`@c@hAkAn@q@r@q@nAkAr@q@x@q@t@m@nAeAv@o@jByAzBgBf@a@x@s@r@m@p@m@d@c@l@m@t@w@hAiAh@o@t@_Ap@{@v@iAjAkBl@cAh@cAvAaDXu@\\}@Z{@n@oBRm@`BwF`BcGh@gBh@gBt@wBh@{Al@yA`@cAf@eAx@iBj@iA`AkBnAgC~@iBn@wAz@mB\\y@Xs@Tk@Vo@Ne@Pi@Pi@b@wAJYRu@d@gBPq@H[Jg@Jg@Jc@Jo@VmANy@@KJg@LaAPkA\\uCX_DJsAJsAJ{B@i@Ds@FcAB}@BgAB{B@_BAeCAeCI{BGqBMoBMcBYoDa@{D{BsT[qDUiDWgDMqBMmDOyEKkGBwGJkFLiDVuF\\sEd@_Fh@_Fp@kFp@}Ev@yE^{BzCoPpAwGxAqHbAmFjCgOxAuIzBsNNaAv@aGh@sEVsCHkADu@Du@BkABoABkBAuBE{BMaDKqBKmAScB]}BSkAa@uBc@_B{@qCeBuEeA{BwBsDgBaDcAuB_A_Cq@qBc@yAa@_B]_BUqAQcA]oCUeCWwC[{E_@wGQmDIgCUuGKuDEmCGsFAeCGoL@oGH_JB_CFyCN{FDoAh@iMNsCD_ALeB\\gE^_ENuAj@{Ef@iDf@iDz@yEl@iDd@}BdAkEr@sCjB}G`BqFfDkKrD}K`CsHlBgGbAeDnBaHnAqEtAmFhAwErAeG`AgF|@uEvA_I`AgGxGec@r@qEr@uEvA{I`AqFx@sEx@aEv@sDF[FY`@gB~@cEbAcEnAwE`AkDXeAp@yBvAsE`A{Cv@{B`BoEvAwD`B}DbCwF~BeFvAuCpAkCt@wAtAeCrBkDrAyBZi@lDuFvB_DHM~CqE\\g@^e@tDyEzAoBzBqCn@u@fEiFjHwIpBeCzFkH`CyCfBaCnCuDnAgBbCmD|EgHJMBG@?^m@^k@pB_DfAeB|AeC~@{A`AaBnDeGb@w@fCoErAmChDiG~CgGzBoEdDgH~BcFrC}G|BcGnBmFlCcIjBiGhA}Dp@cCdBkGpB}H`AaEpDaObCeKLi@H[tDwNl@yBXiAT{@BIL_@FWLc@FSl@qBd@aB`@oA`@qA^iAX_AZ{@Vw@Xu@X}@x@aCpCiHnAwCdBcEvAaDzCkGjCcF~@eBzAmClAsBDEzCoFjBcDxD_HvAoCjCkF|AiDhDyHpCaHfDiJbBcF`BgFtEgOlAcEnAyDbB}EfAqCx@iBf@eA|@cBz@yAn@cAdAyAbAoAv@{@vAqAlAeAr@k@t@g@nAy@ZQpAs@\\Ov@]tAi@lCy@`@IXG~@QtAUz@K|@K|@M\\EvBY~@K\\G~@Mz@O`AO|@QvA[|A]z@U~Aa@lEsAzBy@`@Q~@_@pAg@v@]xAq@xAu@v@a@zA}@v@c@vAy@bBgALIZSb@]LIx@s@jCoBr@m@nAeAnAiAnAmAhBkB`BgBbBoBp@y@l@w@`@e@j@s@fB_CPUBC?ANOx@gAr@}@NQt@y@n@q@h@g@~@w@ZU`@[`@Y`@Wv@g@fBcA~CsBbBoAf@e@|@{@|@cAjAwAJOpAoBhAoBn@oA^}@`AmCJUVw@l@qBb@kBVqAVoAb@wCPsALsAF}@L_CJ_CV{GNgDLiCNsBJoA?CZyC^kD`AyGbAkFv@qDh@wBtAiFpAsEd@yAL]To@Ng@j@eB|@_ClAaDx@uB~A{D~@qBz@gBhA_CjA{BfAqBnA{BhAiBlAkB`A{Ar@eAfBiCfByBpFmGfDqD`CaCzCmC`B{AtFoEpHqFvA}@hGeEhC}AvBsAvF_DlGcDbG}CjCmAhHiDpHmDd@Ux@_@zEaCnBkAzAaAx@g@ZUXSj@_@VSn@c@|@s@h@e@`@]BC~@y@~@}@zAaBPSRWv@}@p@{@j@u@^e@v@gAv@iAj@}@h@}@|@{A~@cBl@kAt@{AbAsBv@iBf@oAlA{CTq@`@eAp@oBj@gBRq@^mAfAwDJ_@v@qCPw@^sAfAeEf@oB^qA^qAn@kB`@kA`@aAVk@d@cA|@eBT_@`AyAX_@~@oA\\a@NOd@g@p@o@TSx@u@t@i@^Yv@g@z@c@ZQ^Q^QVK`@Oz@Y^Mx@S|@UbAO`@G\\EFAXCPCNA|@EZA^Az@AbA@^B`ABj@Bv@Dx@BdDPzAFb@@`AB`B@bBBzAA|@A`@?`@Ax@C~@CfAGz@E`@E|@Eb@EvD_@n@IxAQ`C_@z@Q~A[~A_@|@S`@KbBc@pC}@|Bu@tAg@^Q~Aq@|Aq@fB}@\\QbCqAdAm@PKb@Wf@[b@Y~@k@nA}@j@a@dAw@dAy@|AoAjAcAt@q@p@o@t@u@p@o@lCsCrA}AhByBxAiBf@s@f@s@tAqBpAqBpBaDfAmBtAiChCkFdA}BjAyCtAiDfBiFt@sBnBsG|BiIjBcHpEoPzAmFrDcMrAiEjCkIlDkKXs@dB{Eb@mAlDkJhFoMjBoE~F_NxD_I~A_DbB{CrCgF`EsGbDyEl@{@bDgE`BqBp@w@@CdCsCxC}CnEkEfD{C~DcDpDoCnBwAtE_DbDqBjBgA|CeBhEyB|Au@h@Wb@SxAs@fFuBtCgAhIwCzDoAhE{AjBq@pEkBbCcA~FuCxDqB|BqAfBcAjCaBjBmA`BiAhCuBrDaD`DiCbA{@t@o@dB{AbB_B|@{@j@m@f@e@f@g@VYlAmAdBkB~AiBlAuArBgClA{AdAsAfAyAdAwAdAyA~@sAfAaBnByCTa@j@{@Zi@PWbAcBx@yAXg@z@}Ah@cAdAmBr@wA`AmBhAaCl@mAt@cBb@aAlAoClAuCpAcD`DwIp@sBpA{Dl@kBd@{AfAsDPm@v@sC\\mAt@uCdAkEf@sBp@{CrAwGl@wC|@aF^{B^yBh@_D\\}B`@aCh@aDTmAb@aC^qBXuAViAf@yBZoALg@\\oAh@iBb@sA^iAb@oA^aAt@kBd@iAt@cBh@kAj@gAd@_Aj@aAn@gAb@u@p@eAxAuBp@}@dAsAp@{@r@w@\\_@fAiA^a@`BqAVQlGoFhEaEfAkAjAqAz@gAfB_CtAsBtAwBhAoBvB}DbD{GJW`A{BnAmCl@qAr@eBbCgGpC{HnB{FBGhDiLjDuLpAuEhB_GzAiEjAcDfAcC`AqBlA}Bp@eA|BgDdAsAnDqExBaD`A}AtAyBlAcCx@iBb@iAHSFQh@yAHUHUh@gBl@uBXkA`@}Al@wCd@{B\\_Bp@{CTgAZmAv@uCVu@^mAb@iAn@aBLYLY~@sBVi@Tg@v@wA\\m@`@o@`A{Av@gAd@o@`@g@h@k@\\a@|@_AJMt@w@nBcB`BoA|@o@n@e@|@m@f@Yt@e@v@e@`Ao@`@Yv@g@d@]\\W|@q@bA{@j@g@xAyAjAoA~@cApDcEvB{BnAoAvCuCzBsBzAuAhAaA~BmBxC_CfBqArAaAzB}AtBwArAy@hDuBjAo@~@i@rAs@|Aw@nCuAnJuE|BcAhB}@\\QbBy@pBcAbCmA|BmAfCwAbDkB~BuApA{@hBqAvCaClA_Av@k@XUp@i@~@u@v@m@lAgAvAqAx@w@`B{AlBiBhAiAt@y@zA{AxBgCt@}@f@o@lBaCtBoC`B{Bt@eA@CZa@`AwAb@o@dA}Ad@s@r@eAl@cAb@s@n@cAlAoBhAkBbAcBf@}@Xk@~@eBbAcBt@yAr@uAn@qAd@cAb@_AXm@N]p@_Bd@kATm@^w@Zw@d@mAt@gBz@sBb@eAv@iBt@eBh@qAz@kBb@}@nAiCbAkBr@qAlAsBrAuB|@kAr@{@`AcA|@y@nAeA|@q@x@m@\\OHC`@UXMr@]rCeAlAa@xAa@hEcArDq@jDg@hBUfAK~@IzAMhAGv@El@Cl@CpDK`CG~@CjAEhAGtAI|@Gz@GbAOv@O~@SbAYr@Uz@[|@a@x@_@\\Sp@_@j@]l@c@v@m@r@m@v@u@dAiAn@u@n@{@l@y@~@{A~@eBh@iAp@}Ad@sA\\aA\\mAZgAZsAVkAVqAV{Ad@{Ct@kFt@uFn@kEV}APaAZkBb@{Bt@mDp@{Ct@wCz@}Cz@oCj@gBb@mAx@wBd@oAd@gAj@uAp@{A@CZq@bAqBb@{@l@iAj@eAz@}A|AuChAqBz@_BpAaCv@wAZo@v@aB@AFM^w@^y@Xq@Ti@Xo@d@qAXu@HSTo@Pk@Pi@Vw@Tu@Pk@Ja@f@cBTeA`@}AJe@`@mBdAqF@EBMRiAl@iDn@gD`@sB`@cBXoA\\qA^wAf@yAd@sA|@_Cb@gAj@qAp@wA`@u@Te@x@wAx@sAp@cA^k@DGBEJMBEHOBCt@iAbA{AhAcBdBqClAuBp@iAfAyBx@aBx@kBt@gBb@iAl@cBPg@^iA^iABIVw@J]La@V}@@CfAsDXcA@ABK?ABKZkAb@aBj@mB`A{Cp@qBnAgDd@iAHQBGDI?AFOZu@xA_D@ADKTc@n@oAfB{CbAaBJOBCFI@C`@k@v@eADGz@eAvAeBjB_ClB}BhAsApA{AhByB@ADGHIr@}@~@kAbAqAlAeBn@{@vAqBfAcBrAuBdAcBv@sAp@kAp@iA|AuCdAwBzBuE`BsD|@uBdAcCh@{AnAcDf@yAL_@hAaD`AyCX_A|@yCRu@XaA\\qAt@sCP{@x@gD^cBTcAt@uD|@{Ef@mCf@}CjA{Gr@qEpAaHj@{ClA{EZkAV}@Tu@b@oAZ}@\\aA\\}@l@aBRi@Ti@`@_Ap@wAfAyBxAqCz@uAhAgBbAuAf@s@p@{@xAiBj@o@v@{@z@{@n@m@nAkAn@m@t@m@p@k@h@c@pAeArAeA|@o@n@e@lA{@v@g@x@i@hDwBnCaBjCyAfAm@zCgBdDmB~BuApAy@lD{BhEyClA_A~@w@rAiA`FoElFgFlBsBnCaD~@iApDwE~DuF|@sAT[`A}ABCjAmBbBuChBeDjBkDn@mAd@aApB}DnBaEjB_EhAgCXm@@C?ABEnAuCzAoD\\y@fAiC`BcEtAoDxAsDTk@fAuC~A_EvAcDBItA{C\\q@@CFK^w@x@_Bt@sAVc@Xg@\\m@^q@\\i@^o@\\i@`@m@h@{@l@{@l@y@l@{@r@}@d@o@x@aAt@}@TYh@k@Z[Z_@Z]^a@VWPQVWZ]TUZYZYXYVW\\Y\\YZYXW^Y^]JIJGZWZW\\YXSZSZW`@Yb@YZU\\UZSZSp@a@d@Y`@Wb@U^UZO^SXQVMZOZQXMd@Uf@U`@Q^Oh@Wb@QLERId@Sh@Sd@QVIXKTI^M`@MVIXIRIXITGPGTETIXIb@Mv@QZIZIVG\\Kd@Kn@Qd@IZIXGLEXId@Kr@Qp@QfAYd@Ov@Un@Ul@WLEBANGj@URIhCsAvBoAh@_@BCvAaA`A{@rAmAtBqB`CeCnCsChBkB`C{B~BuB`BuA^[hCkBpCmBtA{@nAw@zCcBxCcBpC_BjAo@NKJEp@c@bDuB~AoAxAiA|BwBlAiAp@s@XYx@cAj@q@p@w@n@y@JOj@{@~@qAt@oAv@oAn@gA~@eBf@aAj@mAb@aAv@kBp@}Ab@mAd@qAh@cBXaAFSZgAl@yBj@}B\\wAPu@BIBOPy@VwAPaANcA@EVyATcBHu@LeAJ}@Hy@J}@HgABc@Hu@HmAF}@D}@FiAHgA@KF}@DeAFu@FaAF}@HcANqBHo@Hs@TkBL_AN{@NaATmAHc@VoAXsAXgAPo@HWLc@T{@^mAVw@Xu@Xw@^aAXm@^{@Zs@Xg@Xo@`@u@^s@Ze@d@y@l@}@bAuAf@m@f@m@d@k@j@s@h@k@f@m@j@i@h@o@^a@d@e@hAoA\\a@XYJKBEp@y@t@{@PSd@i@j@u@x@aAl@w@n@}@t@aAr@eAj@{@h@u@nAsBp@eAh@cAj@cAf@{@h@iAx@_B\\o@d@aAh@gAn@oAr@aBd@aAv@aB|@oBZk@N[BEJSd@eADIvLsTtGmKlAsBXe@NYDGJS~@}A^m@lAcCzA}C~@iBd@eAjBaEtAgD^cALYJYZy@`AkC~AiE~AqElEcNHQ@AFUz@qCtAqErFqR`@uAv@iCbAmDtAkFxAyEfAkDlAuDv@_CDKp@sBdAqCTo@nA_DvAkDl@yAfBaEtBmEjCmFd@_Al@kAjC}EtByD`A_BtC_FnCqE`C}D\\k@Zk@NWrDiGvC_FdAiBbAeBxAgCp@kAt@wAj@gArAgCh@eAZq@\\o@rBeEnBaEbDaHhCaG|AuD~@_CtAkDn@aBHU~@cC|@eCvAeEvCyHVw@Vu@`FmObBuF|FuQz@iCp@qB|D_LbBoEPa@Na@x@qBrAeDdBwDhByDfCkFjAyB\\q@bDwFpA{BlAoB|BsD`AuApBsCZe@xAqBj@w@pBkCzAiBLSlAwA`BiBr@{@jCqC`@c@xAwA`C_CbB_BvDgDvD}CzAiAxC{Bx@m@hBoApA}@pA{@nBmA`C}AjC}AbDkB|EsC~EsCpAu@fC{AzDgCv@i@rAaAbBqAdDoCzAuAZYr@q@p@q@l@o@n@s@dAiAfAmAr@y@vAgBr@{@vAkBbCgDnBuCjBsC`BmCpBiDXi@bAmBx@_BhA}BrBiEbB{Dt@kBn@}AjAyCtAuD~A{Er@aCNc@Le@z@sCv@qCLe@d@kBf@oBXmAn@oCVkAn@{Cp@kDb@_CbA{F|@gFn@_E|AkJr@iEn@sDv@mEnAgHx@gE~@sEp@}Cf@_Cd@sBx@eD~AgGfBoG~AsFfGsStE_PhBcH|@mDrAoFhA_FzBuKjB{J|@iFn@eErA_JnAsJ\\kCr@gGZqCh@qEb@wDf@iE^aDn@}FReB^{C~@_IVcBn@wEp@gEn@_E~@cFdAiFdAaFdA{ErEaS~@wDjAqE|@_DhB{Fh@_BbBoE^aAxAmD|BaFfC}E|AqCvAaCxBoDzAaCrBaDhDwFpAyBj@gAv@yAz@aBx@aBt@aBhAgC~@}Br@kB`CqG~@mC|@iCn@kBl@iBz@kC|@qCx@mCz@oCz@sCx@kCz@iCn@kB^gAn@gBbAiCvAcDv@eBhAgCt@wApAaCnAsB`A{A|@sAl@{@^g@x@gA`AmAbAqAfAoAhAmAfAeAjAmAr@q@n@m@hAaAdBwAv@k@nA{@pAy@rA{@pAu@pAs@pAq@lAk@vAm@ZO|@[dCy@LEdAYrBk@vDcAxA_@~Bm@tFqAdBe@lBg@nA_@vBs@lBq@fBu@pAk@@?j@Yr@_@z@e@x@a@nAw@t@g@p@e@^WTSjA_Av@o@fA_Az@w@|@}@n@q@t@y@`AkAfAsA~@mAx@gA`AuAjBuCv@oA^m@`@w@h@aAn@oAz@gBtAwC`@{@Vk@r@}AlAsCpA_Dp@cBbAkCrAiDfBmEpAkDr@cBv@iBdAcCp@uAh@iAbAgBv@sAzAqCv@sAz@wAxA_Cl@_AdAyAn@}@z@mA`ByB|AqB|AsB~AyB^g@^g@xBuCnCwDvAuBl@}@nB}CdAcB~@_Bt@oA|@{AR]h@aAtAgC~@eBd@{@v@{AbBmDh@mAx@gBp@_Bj@oAj@yAv@oBl@}Af@qAb@mAp@oBl@sBj@mBn@cC|@sDb@uBVqALs@P}@\\}Bd@qDTuBLkANwA\\cFJgBH{AF}ADwAB{ABqA@yA?eACyAA{AAwAAiAAo@E}AEkACs@GwAMwBQcCQ}BUyBW}BMaASyAU}AQaAO{@WoAWqAm@iCo@kC]mA_@aBMa@Ka@U}@[kA_@sAYeA_@uAi@qBiAiEu@uCeBuGeAyD_@yAeAaEw@{Cu@{Cu@aD_AeEe@{Bw@cEY{ASoA]qB]}B[{BYgCW{BUwBS_CKuAEq@Ci@I{AM{BCs@Ai@GeCCeBAmA?cA?}A?_CBgCDyCBkABw@HcCDuAHeCBs@NcELiDBu@DaBJcCDkBFqDDcE?gC?qB?_AC_EGsCGkCImDIeBGaAKqBMuBQmCSeCa@sEOqA[iDe@mEYyB_@{CS}AY{Bi@gEi@oE[_CMoAQoASaBK}@Gi@Gk@u@}GSgBQgB[sDQ}BWaDEu@QeCSkDS{DGgAE}@Ck@Co@QeGMoEKgFEqDAiBE{G@yE@aEB}A@gBLsEL{DH}BNgCDu@^{E`@oEHaAb@_Er@sFXeBvAyHLo@Pu@No@XqAx@}CJ]Nk@b@qA`AuC^aAb@iAhAoCJStAyCp@qAb@s@d@_AnBeD|CuENQjDaFT[zDqFfFmHn@aAfCyDzA}BvA}BlBaDrAeCh@aAtAiCvBgEx@gBNYj@oAv@eBz@mBhAiCxAsDf@qAzDeKvA{Dp@oB^kAHUfBmF|A}Ep@wBdFwPjBqGl@qB|@{ClA{DnA}DX}@b@qAf@wAl@gBb@kAp@gBn@cBf@qAr@gBRg@d@eAb@eAVi@Pc@f@eAb@cAh@kARa@b@_Af@cAh@eAf@eAdAsBdAqBb@u@l@kAh@aAj@eAZk@j@cAhDmGj@gAx@{Av@{AjA{BbAqBrAoCp@yAz@mBd@eAXo@pByEn@{A`@cAhAoCnCaHtEmLbAaC|CeHrAqCfCaFzAyChA}BfAsBnCmFvAuCt@yAfCqF`BoD~ByFxAqD`@kAd@oAp@iBrAwDpA}DlAyDj@mB|AiFz@_Dt@qCfAeEv@uCpAyE`AkDl@yBv@oCr@}Bf@aBp@qBfBoFfAyCjA}ChB}EjByEbCiGhAqClBaF`AkCv@mBr@qB|@gCz@kCjAsDx@kCz@{Cn@{Bl@_C~@uDz@mDl@oCn@yC~A}Hz@yEjAkHj@wDVcBl@oEdAoIl@_FReBPyAJy@Hm@Da@\\aCHw@Z{BPiAJs@^yBd@sC~@sEh@iCt@{C~@uDZgAXcA^mAx@iCp@mB`@gATi@l@yAv@eBr@uA`@y@j@cAh@_Af@w@t@gAn@}@l@u@t@_Ap@w@r@u@t@u@`A}@dA}@~@s@l@a@p@e@bAo@t@a@lAo@j@Wz@a@^On@Wn@Wv@Y^MLEJC\\M`@Mb@O~@WbDaAjBg@zBq@|Ag@dA]b@Q|Ak@|Am@n@YrAm@xBiAtAu@~@i@lCcBnBwAz@o@lAaAnAgAp@o@VW`@_@l@o@r@w@x@_Af@k@Za@j@s@x@eAFKHKJOJOr@eAZe@bBoCvC_Fn@eAj@eA|@}AlCyEb@y@x@}Aj@eAz@aBrCsFzBqEnAiCrBkEvBuE|BkFd@eAvAeDp@cBr@eBn@cBlBeFr@oBzAoE|AqERk@t@eCX{@Pm@Tw@XeAz@_DrAaF~@kDt@sCt@sCj@sBt@sCx@_DxAgFl@sBr@wBd@_BPk@n@qB`@iAn@iBv@wBn@_Bz@uB\\{@f@mAhAkCjAoC|@uBj@wAXu@Tm@j@eBLa@Ni@b@aBRy@P}@Nu@Ny@ZcB"
                     },
                     "start_location" : {
                        "lat" : 42.5589125,
                        "lng" : -2.8741259
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "21.8 km",
                        "value" : 21824
                     },
                     "duration" : {
                        "text" : "12 mins",
                        "value" : 726
                     },
                     "end_location" : {
                        "lat" : 41.6148528,
                        "lng" : -0.6992187999999999
                     },
                     "html_instructions" : "Take exit \u003cb\u003e244A\u003c/b\u003e on the \u003cb\u003eleft\u003c/b\u003e to merge onto \u003cb\u003eA-2\u003c/b\u003e/\u003cb\u003eZ-40\u003c/b\u003e toward \u003cb\u003eA-23\u003c/b\u003e/\u003cb\u003eHuesca\u003c/b\u003e/\u003cb\u003eBarcelona\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eContinue to follow A-2\u003c/div\u003e",
                     "maneuver" : "ramp-left",
                     "polyline" : {
                        "points" : "ydk}FvluD?a@?C@E@SFg@Hq@Dg@Dg@Dk@Dk@Bq@Be@Bo@Bm@?[Bu@?a@@y@Ae@AgAAq@Am@Cg@Cm@Cm@Eu@Em@AWGk@Go@Gk@Gm@Q}AG[Ie@Kk@EQEWSw@Qy@WgAQo@Ma@Mc@M[Qi@Qc@a@cAi@uAUi@c@eAWk@?AACC]]u@M[MYK]Yu@Sq@Oi@Qk@[qAMi@EOMq@GWOy@G]Ko@SsAk@kF]sDIgAGw@Ek@GmAEk@Co@GqBAg@CgAA{@?_A?o@Aq@@y@@aBBeAB_BDeABu@F{AJcBFqAz@wNXyELaCT{Db@gHHeAHcAj@cLHmABa@@YD}@RaDj@uMFsEJcN?cI?m@?A?Y?A?o@?sI?u@?aA?C?m@?c@@sI@_G?G?q@?kA@eC?iH@m@?]?q@?C?Q?QAU@}D?uB?k@@kA@mA?}@@{D@wA?Y?gBBcW?[@k@?k@@wO@{@?y@BqLBsN?eA@eABiBBuEPcGL_DVeFDg@Dg@F{@N_BhAuLFk@Fk@@M@Kj@eGd@_Fr@uHVkCPiC|@gKb@sE\\_Ep@cHJcAHcAhD{^XwCRwBLyALwAVoC^cERqBB_@D]v@wIJ_ANaB?Ed@oEX}B\\gCTqAP}@^mBTeAb@mBH[Li@Ru@DOV{@l@qB`@qAv@sBp@iB`A{BDIr@{AdAsBh@cAd@w@dAgB~@{A`CwD~@_Bh@_Ar@yAb@cA^}@r@oBZaAX}@d@kBXiAf@{BhAmFZaBTaAZqAf@_C`@kB`DyOb@eCf@kDp@gFLiAr@iHj@cGt@oHj@wE^eC\\aCbAaGbAkFr@{C|AuGxAcGvE{Q|CyLTcAn@eC|A_GbCqJ^uA\\{Af@qBlDcNfAeErAqFr@mClCmKb@{AV{@\\gA\\_AfAiCr@yAn@mAp@eAz@mAn@{@n@u@z@{@p@o@hAaAt@k@|@m@pBqAzCiBpD{BhD{BlA}@jA}@bA{@vBiBv@s@n@o@p@q@rByB~AmBrAaBbAuAjAcBlBuC~AkCdAoBp@mAdBoD|@oBd@eAbAiCt@kB`DeJj@aBXy@n@mBp@mBbAuCp@iBv@sBt@kBhAoC`AwBx@iB|@qBv@cBN[N[dAyBhAcCdAcCVk@Tm@d@oAbAmCTu@h@gB^kA\\sA`@{AT}@Lg@Lg@vBeJR}@Nm@f@oBRw@Tw@V_ATs@X}@Rk@Ri@\\}@f@kAXq@`@{@h@eAr@uAZi@LSHO~GwKlAqBn@gAZo@R_@N[Ra@P_@n@{Ax@sBp@qB^gA\\mAh@oBl@cCXuA`@wB`@cC`@aCRqA^wB^}Bf@mCb@uBZwA`@cBXgAh@mBb@uA|@oCd@uAfAsCf@mAh@mAz@aBVk@Tc@Rc@zAsCzEyInAaCx@mBt@iBv@uBTs@X}@\\gAd@aBp@qCT}@P{@l@gDtBwNNu@Py@Ns@Pk@XaAPe@Pg@Vi@Xk@T_@Va@h@u@DGb@e@r@s@"
                     },
                     "start_location" : {
                        "lat" : 41.6777282,
                        "lng" : -0.9340364000000001
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "214 km",
                        "value" : 214262
                     },
                     "duration" : {
                        "text" : "1 hour 47 mins",
                        "value" : 6436
                     },
                     "end_location" : {
                        "lat" : 41.2686837,
                        "lng" : 1.5357647
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eAP-2\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003ePartial toll road\u003c/div\u003e",
                     "polyline" : {
                        "points" : "y{~|FbqgCZYPMhBiAfAm@nBeAjDeBrDgB|@c@fDaBhB}@~DmB~C{AbAg@FCXOhAi@nCkApCmAjCeAfDqA|Bu@~CaA~Bo@lAYrBc@`Eu@jEs@f@I|@M|@O~@Mv@Mx@OzA[zAY~Ac@xAe@rAg@z@]r@[n@[`Ai@t@c@ZQZS\\Sd@[r@i@f@]r@k@jAcAhAgAbAiAn@s@`@i@f@m@`AqAZe@\\e@^m@b@s@b@w@Ta@Tc@R]N]Tc@Ra@\\w@Ti@N]N_@N_@n@eB^kAX{@J_@Ro@\\mAVcAViAXqA`@sBZoBZqBJw@\\iCZ{Bl@wEv@oF\\sB^sBXsAR}@b@oBZiAf@mBh@iBj@iBn@iBp@gBp@eBr@cBv@aBjA_Cd@}@h@}@f@}@l@_Af@w@h@w@Va@j@w@\\e@\\a@\\c@h@o@tB_Cr@q@`B}Ap@m@z@s@`Aw@jBwA|C_CROTQzB_B~BkBZY`A_Ar@q@t@y@dAiAj@s@v@cALOtAsB|@wA~AwCjAcCf@gAn@aBn@gBjAsDxA_Gl@qCf@_DZuBTkBB[D[LgAPyBHqAJuBDiADyADuABsB?{BCyBGaDCo@EiASgFSsFIaCGoE?Q?a@?a@@kBDiB@q@Bq@Bs@PaCNoBTmBHo@Hk@f@}CP_Ax@qDHYZmAn@mBVu@Tq@|AkDd@aAnBkDhBqCdFkHtAiBhF}Hn@aAx@uAf@{@~@}Aj@aAfAoB`CuEvBuE|AkDJW~ByF|@{B`AkCp@mBlAsDrAoEj@kBXcAb@_Bn@cCZoAp@uCz@uDNy@f@cCbAkFtAmHpAoH`AkFh@sCn@wC\\wAPs@d@gBn@_Cf@cBPi@|@oCL_@\\}@h@{AfAoCTi@l@sAVm@d@aAx@cBv@{Az@}At@sAVe@|@_BzAiCXi@~@cBP]d@cAt@{Ax@iBb@iAl@eBFMp@uBb@{Aj@_CPo@l@mCj@mCb@wBRgAPu@l@uCVkATaALg@\\mAZgA\\gAX{@r@mB`@eAj@wAXq@t@gB~@sBd@iAr@eB\\y@j@{A`AsCZcARo@\\oAXgAJg@XkAb@wBZmBTuA`@wCXiCFy@H}@Dw@JwAFqADmAFcBD_B@eB?{@?}AAmAGiDEgAGeBG{@ImAKqAKiAEi@WwB[cCYcBUuAWsAWiAUaA[oAW}@g@mBo@sBq@_Ce@aBe@}A[iA[mAUaAc@iBYuA[cBUmAKs@[eCWuBW{COoBEs@E{@GoAC]?UEiBAqAAiA?{A@{A@uABsABg@D{AHqAFkAHkAFk@Fu@N{AVsBPsAZkBVuAZ}AZ{APq@d@kBVw@p@}Br@sBr@iBd@eA@Et@{Av@{Ax@aBn@oA~CuGpBuEhAoCx@{Br@{B\\kAf@mBx@mDDOfAmFt@yDXeBD]BM@K@IPeAXmBFg@~@mGF_@b@oChA_Hx@aEh@eCh@sB~@yD`CgIvAeETo@Ro@bBgEhCeGN]Vi@Te@JQDKVi@Vg@v@}Ah@_A|AmCZi@NU|C}E`E_GjEmGNQNWd@q@~CeFlCiFdBwDr@mBl@{Af@{Af@}Aj@gBV_An@_C`@eBh@aCZwA^uBVuARqA^mCNmATsBN_BRwBLuBHoAJwBDmBHkBF}D?U@w@@y@A}BAeBCcBImDK}BQ_DW{DWuCe@aEm@gEm@sDg@iCsBuJy@oDs@aDg@gCQcAUoAGg@G]Ik@Ig@m@iFIiAKmACi@AWAaA?w@DiBHcC@_@@_CBwB@_A?eA?yAEkBCe@?i@?{@@s@Dw@Ba@F{@NyABIJ_ABQHk@|@iGf@yCdAgGl@yDj@_Eb@_EVcCFw@Fs@B[PcDLeCD{AHmBAo@?iADwH@{DAwE?cA?cA?cABkDD_CBkAHuCFcAH_BJ}AHuALqAZaDFk@`@{DZuCTmBRwBJeALcBJuALmBH{ADkA@o@B}@@aA?}@@g@A}DAqACsAE{@Cs@GiAGqAGy@ScCMyAOsASuAUcB[mBIa@YuAOs@Q{@S{@g@gBe@aBi@aBk@aB]}@w@oB_@}@qBwEeAcCi@oAk@wA_@aA_@eAe@wA[eAYaA[gAWiAQq@WmAWoAWwAIg@WcBKs@QwAO_BGk@IcAM}AGoAGcAEqAGwACuAAqB?sA?_BBcBBuADiAF{AF_AJ_BDq@LyADc@NsAZiCHi@L_AVuA^oB^cBb@mBRy@j@}Bh@uBh@qBr@sCXkAXmATiA\\kBZiB\\}BRaBLoAHm@LyAHcAHcALgBRcCRmCPwBN{A\\_DL_ANeAXaBVwAVoA^iBFQLm@b@cBf@kBj@uB^uA^sAf@kBZkAf@uBb@qB^qBVyANaAPgAP_BPeBVkCH{ANcCJqDDqB@yDAiBEsCEuAEeAEu@IuAEo@Ei@I_AAC?KMoAQiBm@kEW_Bg@kC[}AsAkF{BkIaCaJ]mA_BsGMm@Uw@aBuHYuAm@}Cm@gDqAaIK{@m@oEe@sDm@}F{@wJg@wH]aHUqGKoDM}HAcACiC?uA?{@?E?g@?aF@_CJ}FDeBBmALcEDoADs@P_EHgBXwEd@uGH{@TmC~@_Jl@uE~@{Gr@mERiAHi@`BmIZ{Ap@}CXmAf@sBdAcEn@}B~AoFnBaGz@gC`AkCd@qAbEcLrBuFlAuDl@sB^yAJ_@Ro@t@aD`@mBp@oDd@uC^qCHq@Fq@Js@`@uEZyEXeGDiB@g@BmC@_EA{BGeFCaAEmB?[MoCGyBKuDE}CCkC?iDBsEF_Cd@aMNqDRkFNcHByD?}BCsEKmEG{Be@kHu@gI_@gD[uC_@aD_@uDYkDU{DWiEMuECgB?cCA_DFcDFyCJgDTuEP}DFgALsCL_CJqBH_CFwBBoAB{C?gD?sAE{DA_AEkBMkDMyCQeDQaEWgGUkFQyEMqEGgFGiFAyE@uDDaGDgEH_D@YReGPkELgCTuDTyCV_DTyCTqCVuCR}BRwBZaEV{CT_DPwCHoADq@DeALyC?KFuBDqBB}BBgFCmHKaDCeAAa@Cm@Ci@EaACo@E_AEq@GaAEu@Eo@Gs@IkAGs@KeAIu@KcAOqAScBS{AOgAU{AUyAQcAWsASiA[wAUeAWmA[sAU_A_@sA[mAa@sAa@sAg@cBi@}A{@aC]_Aa@cAkDqIQ]O]IQ_ByDm@yAeAuCeAyCiAmD[iA[kAe@kBYmAYmAa@qB]sBSiASqAQqAQqAUsBUwBQyBUeDQcDA_@EqAGwBE_CA{AAkA?iC@i@?]?_@@_@DqBH{CDoAJmBJeBJeBNqBDu@NqBPyB^}EHeAf@_INwCTqFBu@?ODuADeCHwE@w@?oC@aDJcQ?E@K?M?M?a@?OV{JNyDLeCh@cId@_FHq@`@yDPwA`@aDV_BFc@`@eCZmBNaAZ{APu@dAsEx@mD|@eDjAeETy@tFcQTu@Rk@b@{AZ}@ZiAZiAZgAh@oBd@mBd@kBn@uCViANu@Lu@Z{AV}A\\uBZsBN}@ZcCPuAPuAPeBRmBLuALwALmBP_CF{@Dy@D{@Dm@F{AFwAF}ABy@DyBB_CBqC@}A@oCA_DA{BCmBE_CEsACu@IiCGqACm@E{@C_@A]ImAOuBKyAOgBSyBSsBUuBWwBQmAYsBKq@]aCW{AYgB]sB]mBi@yCUuASiASkA[kBYoBQmAWoBW{BMkASwBOuBMsBGqAEgAEqAEuACyACcAAuAAi@?{A?cB@yB@q@DsAFkBDqAHgBD}@JsAHqAJiALsAL{APuALkARyANmAPsAPmA^iC^kCVmBTmBTmBLoALsADo@De@JeBHyADqADwABwA@gB?wA?{AAgAAg@Ac@Ak@EmAKuCQmDO}B?AA[C[MaCI_BEwAEqAEoACsAAy@?_A?_B@qABgAH{BJwBJqAJmANkAFq@NeATuAToATmAb@oBZqAd@}ATu@b@oAPc@Vs@JWXq@hAeC|AcDfA}BTk@`@{@Pc@Ti@Pc@b@kAXw@j@iB^oAv@aC^yA\\{ATuAV}Ah@aELmAFq@HmABk@JqBDwAHaD?o@@e@?q@@}BEkBGeCIkBGsAO{BI_AUyBScBO}@eBuKCWEWQgA_@}BU{A]gCQyAWeCIy@OuBOuCGaBCy@As@CqB?iB?oA@cADeCBkAJmCJuANkBVkDZ{Cl@mEz@cHd@iEb@aF^gFHkBJ_CLsEBiC@wFE_CE_Ca@eKMkCc@gJCg@WqGI_CGaB?i@EaD?_A?e@Ac@B_FBeABkAHcCHwAT{Db@gFFu@TiBD_@f@oDd@{C|@yEb@sBP{@x@yD~BgKh@mC`@yB\\cCXyBJaANwAD]LeBJaBB_@JsCJqF?cBAwBGuDGeBMoCe@qGU{B_@iCk@eD[gBkAyFs@eD{@mEW_BU{AS}A_@oCKoAGw@CYOuBSmDMeEA{AAqA@mCDwCFmBN_DVkDRqB\\_D\\gC\\oB`@{BbBiInAyGh@eDZ}BReBVqCRmCL}BDmADqAB_C@sB?}BGeDGqCMgCO}BKyAUuBWwBKw@UwAW{A]cBUeAc@eBa@aBe@_Bg@_Bm@gBEKs@kBe@iAs@}Ao@qAu@uAq@gAMU}AcC}@oAcBwBuAgBgI}JeCcDeB_CkAiBi@y@_BoCYi@]k@sAcC{@_BaAsBy@gBeAgC_BmDsAsDeAyCw@eCk@sBs@iCcAiEcAiEg@_Cg@mDu@qFe@{Ee@}F[yEMgGEgGAqCBqCBiBJuERgDf@sGt@oGr@uEh@wCx@aEjC}Jr@}B~@qCRc@nCwG~@uB`@s@LWx@yAd@y@l@eAhAaBDIrBuC~AoBjAuA`BgBfAgAl@i@vAqAzAkAnA}@pFwDf@]JERQb@YdBqAtAkA~A_BhAoA`BwBzBoDfBgDxAsDhA_Df@iB`AoDVmAb@oB|@oEz@wDr@gCbA{C\\}@d@eAz@cB|CaGJShB}Dt@yB\\eANi@^sAj@mC`AiGRoBH_AV{ELaCJkCHyABk@Hw@ZmCVwB`AaFFU@EDQFUDMZiAPg@z@}BVo@`@eArAiCPYlAoB|B{CzA}AtCwCpAwAzBoCx@gAl@_AZg@r@iAh@_Ar@yAJUx@kB`@kA|@mCr@iCPs@Nm@XwALq@\\sBRsANoALeAJwAJiALyBFkBBmABoA@mAAm@AmCEwACgAE{@Eo@E}@Ec@KyAMsAOyAMmAYaC[wCe@yEMeBMaCGeBAe@?UAI?OAS?U@{EBgBByALkCPgCNoALgAPuA\\cCVmAb@uBp@iC^iA`AuCx@wBP[bAwBt@wAx@yAh@u@Xe@nAqBhBmCZg@hBaDp@qAf@iAn@gAPa@r@cB^eA`@kA\\eA\\gAZiAd@iBXsAZwARcATmARqAPmAPqALgAHu@LqAJoAHgAD_AFoADiADsADcA@y@BiA?q@BaA@yA@oABuA@aA@}@@o@BkA@wA@s@BsADkADyADkAFmAFqAFeAJcBHuAJsAHoAJuALkAJiALuANoANoAPsAR{ATcBTyARoARiAToARmATkAVoAVmAViAVgAR{@T_AXeALi@XcAPm@Tw@Tw@j@iB\\eAVw@h@}A^cA\\aAd@oAb@cA\\}@b@cAb@cAd@cAf@kAr@{Ad@aAh@kAd@aAh@gAp@uAx@iBb@aAf@gA^}@Vm@\\}@`@eAJ[Vs@Z}@^kANi@XaARo@Ry@Po@VcALk@T_AP{@R{@`@{BVsARoARqAPoANsANgANyALuALmANyBDi@T_DLeBFcAFs@LqBN}BNyBNiBTaCN{ARkBX}BPuAXoBZ{BRiAVwA\\qB`@sBLq@Pu@\\}AXmAVgAZuAb@aBXcAj@oB^sAb@uAd@yAZ_ARq@j@eBn@mBt@wBZ_Ap@oBj@gBj@cBb@wAj@gBn@oB\\mA^mAh@oBh@mBTaA^yAb@eBPw@R{@\\{AViAVmAZyAToA^sBb@aCRuAPaA\\uBt@kFFo@^yCf@aEVoC^_EZ}DLgBX_FRqD\\_JLwEJgGB_IAgJCoCKkHOuECm@U{FU{Eu@mLw@aL]wESiCEk@IuAGqAGyAIcCGyBEmBAaE@cBBuCJoED{AHeBJqBJyAFy@HaAN{Af@uENgANgAHe@x@{E@EReAb@oBf@uBt@uCd@_BxB}G`ByErA{Dh@aBn@eCTgATmA@IRmAJo@\\wCLgBNoBDgAFwC@kBC}CEsAMgCKsAMwA[eCESY{BYgB_@qC]oCGs@MyAEs@Es@G{BCqAAyBAaB@uA@mB?wGAg@CaAGaBA]O{BQoBOmAa@yCMo@sA}HWmB[}BE]O}AUaDG_ASyCUyBAOOgAQqAW_BMm@g@eCc@aBOk@Ss@M_@Yy@i@wAc@eAYk@Ue@k@gAc@s@{@qAU[U[}@gAeAqAaAqAu@eAoAuBk@aA_AoBe@cA]o@iA}Bk@gAq@eAm@aAy@iAu@_AaAeA{@{@mAeAc@[c@[y@k@e@Yq@]s@_@_Bw@qAk@mAk@w@_@}@g@aAk@c@[s@g@c@_@aA}@[[k@k@gAoA[a@[c@oAiB[m@a@u@i@eAe@cAM[Si@Qg@Qg@Ws@Uy@_@sASy@SeASaAS_AUsAUiA[eBa@kBc@sBSq@U{@Qk@Sm@Us@]{@Qc@Wk@i@gA[o@Yi@w@qAq@gAw@iA{A{B]g@U]q@cAg@{@Yk@S]c@}@Wi@Uk@a@cAQe@Oe@Ww@Y}@Mg@K]i@oBOi@Mk@Oo@Ka@Mg@ESEQo@mCeA{EYuAOu@Mq@Ou@Kk@YkBIa@Ga@QiA{@cGGo@UcCQiBKeA[gEIaBUwEKyCAe@E{BC{AAcA?iA?MAwA@kA@}@Bw@?C?k@Bu@D}A@s@BqABy@@{@Bo@?UHwCBu@?C@]DsB@[EyK?eA?cAGy@EqAG{@KsAMyAKgAOgA[qBQiASgAQaAScAUmAQs@YoAOw@YyA_@eBSkAGa@My@OqAM_AKgAI{@Gs@G{@G_ACcAEo@Cu@C{@CwAAiB?sA?cAA{A?gAAq@EgECeAAk@Am@GiBGmAIyAKyAKuAKaAa@_DGg@Im@Ik@UqAQiAScA_@mBUiAUgAQaAMo@YwAYuAI_@E]Ia@Km@Ik@Kq@Ku@OgAQuASuBGw@IcAGw@IaBEkACm@C}@CmACoAAyAAgCAyD?_@A}AAgC?e@EeCEuAIeCCs@Cc@ImAM_BS}BOwAIk@QyAWeBQ}@WuAMk@Qy@S{@Kc@Oo@Ok@YgASm@Og@Ma@]aAc@oAk@yA_@{@{@mBi@eAUe@k@cAUa@g@y@OW]k@m@{@y@iAi@s@W]eAsAqBeCuAiBs@}@m@{@m@y@S[w@gAaA}A_AcBWc@Uc@Q]Ua@e@aAUe@i@iAQa@Qc@{@qBa@iAaAmCg@_BIYc@wAq@iCQs@[qA[wAEQi@oCSiA[iBM_AUyAQwAOsAIu@KkAMoAKuAK}AIqAI_BIyAEsAIuACo@MkDGmAOuCGaAG}@KkAUcCGo@OiA?GEWACK}@Kk@Km@Ii@Km@WmAWoASaAQs@Om@Ok@W}@g@eBw@sC_@qA[gASs@K_@Mc@CKg@qB[mAWiAQy@]mBM{@SoAWqBQuAUyBOcBu@uIM{AMuAIq@MmAMeAIo@M{@Ga@QcAKo@Q{@c@wBUgAa@}AQm@Oe@]oAYy@Uq@O_@gAuCCIMYKW}AeDcBgDe@}@y@}Ae@_A]o@g@eA}@kBe@eA]y@]}@Yw@Qk@AAQe@GUIUOe@Og@Me@I[U{@_@}Am@qCc@}BSqAUyAc@eDIk@[aC[kCYuB]wBIk@_@wB[{AYoAMm@Qo@I_@y@yCIWIUQg@Ma@Sk@_@cAs@gBo@uAm@mAYo@i@aAS_@Wc@q@iAa@m@m@}@[c@aAqAq@w@m@q@UY]]Y[i@i@OOu@q@o@k@[Ye@]a@[k@c@{AiAu@g@u@e@q@c@k@_@w@e@o@a@iAq@}@g@_Ag@kAq@iAm@eAk@eAk@iAo@y@e@w@c@}@i@o@]eAq@{@i@y@i@m@c@_CaBiA{@MKg@a@_@_@SOKKQOg@e@{@y@e@e@qAuAMOoAyA{@gAaAqA}@qAeA_Bo@eAoA}BaAiBq@sASe@u@eBcAgC[}@Yu@e@uA]cAe@}AYeA[mAu@wCUaAUaA[cBQcAa@}BG_@Iq@Km@Is@Mw@SgBCUGi@Go@Eg@Gg@IiAKwAIoAGcAGmAKmCEyBAi@Au@C}@AeA?}@?}@?{@?oB@aCBaCBaA@{@FwBDoAJcDN_DTqEHoAHwAT_DVcDRaCVuCX_DZaDN{AjCuVd@gE|@iIB[BYJ_ANaBFm@f@yF\\kEJuAPaC@W@QNeCHgAJoBBk@DgAJ_CPqENuGFcCBsA@sA@qABkEBoB?wA?qAA{BCcDAyACyBEwBE}BGsBGaCMgDOcDGkAIwAQ}CUgDO{BW{CKoAS_CY_DSyBi@uFw@iIYyC[cDQuBOkBOiBEc@YyD]aGMuBMuCGwAIgCGaBIsCE}BIiECaCAiACeCCcEE_GCgEEmECyBCqAE}BEoAEs@EcAGuAIqAAQAYIkAEs@CWGu@QoBSyBOsAUsBIk@a@yC]yBSkA_@uBa@sBSeA[sAUgAq@mCc@cBo@yB]iAk@iBa@kA_@iAOa@Yw@y@sBq@aBiAgCs@{Ac@_Aw@_Be@_A{@aBc@y@e@_Ag@_AWi@u@wAc@y@y@}Ai@iAACMW[k@GOc@{@e@cAcA}Bs@cBg@kA[}@]{@w@uBm@kBm@kBk@kBi@mBg@iB[mAo@mCq@wCi@qCWqASiA[mBe@{Ck@cEOsAMeAQcBOaBW}CQyBMwBM{BEoAKsBEsAKqEA}AAYAuCAmB?uC@qA@oABqABoABqABoADqAFoADqAHoAFoAHoAHqAHkAJoALmAJiALoAFo@LkAPoANoANcARwANgARmAHg@Ji@PcA^qBViAVqAViAb@mBXgAXmAZgAJa@ZeAZgAZgABGJ[To@Ni@Pg@Pe@Nc@\\aAXs@Vq@\\_ARc@d@gAb@eAL[d@cAXo@lAeCZm@P]Tc@b@y@LSFOR[Ta@P]T_@P[p@kA^o@d@y@z@{Az@wAP]T_@Ve@f@{@d@}@Ra@`@u@Xi@b@}@Xm@JS`@_ARc@\\y@Tk@Zy@Ri@^cAPe@X_APi@X}@`@qAf@mBPq@FUNk@TeAb@qBj@uCh@yCh@yCf@yCb@oCLy@Lw@d@cCP{@XmAd@oBh@oBb@wAf@yAr@qBTk@Xs@\\{@Vm@d@oATk@\\{@d@mA`@eA`@iAd@{A`@qAVcAZoAZyATiAToARsANmAJaANaBHiAHaAFgADyADiBBqA?m@@wAAcAAkAAu@GoAMqCIuAIaAE_@QiBQ{AOeAM{@SwAUsA[iBe@qCSkAM{@[yBMkAOwAIcAGo@ImAKkBKyBCs@Ck@CkAC}AC}CGcJCuBCmBIsBEwAK_BKcBOiBKgAQyASqAUcBUyAGYI]Mw@ScAQu@YsAI_@YiAYuAQu@Q}@S{@o@aDWmA_@oBSoA]qBQsAQmAOoAUoBMsAEc@Gu@OmBSiDCc@GsAEkACo@CgACsAAo@AkAAoA?sA?mA@qA@k@@oADoADwABgAH}AFkAReDJoADg@LoAJaAHs@Fe@Fe@Fi@Fe@NeAJm@PgAHk@Jg@He@Je@Hc@TkAJe@TeANk@T}@XkA\\oANe@ZcALa@ZcA\\eA^cA`@eAb@gA`@_Ad@eA`@}@d@aAd@}@d@}@z@}A|@{AV_@d@u@`AwA|@oAfAuAz@eAnAyAzAeBzBeCzBgCf@k@l@s@l@s@Z_@TYj@q@V]j@s@X_@d@m@Zc@T[f@u@T[j@{@b@q@p@cAZg@r@kATa@\\m@\\o@\\o@\\o@x@_Bv@_Bv@aBt@aBp@_Bp@cB\\y@Vq@`@cA\\eA`@eA\\eA\\gAZcAj@mBh@kBr@gCt@sCpAeFf@mBBO^yANi@VcAPo@DUPk@XcAZiANi@XeAZcA\\eANc@\\eA\\eA`@gA\\aAr@iBr@gB`@}@^}@Tg@P_@b@aAb@_A~@mBr@sAv@yAPYXg@nAyBrAsBx@qADGf@u@TYxAsBX]|@mARU@Ar@{@xCqDpCaDfAoAnDgENQPQv@aAhAyAlBiCbAwArAsBrAwBbBuCx@yAlA_Ct@{AnBiEv@iBp@_Bd@kAhBgFd@uAt@_C^mAf@eB`@uA`@_BZkAZwAT{@d@wBViA^mBToAp@aEZsBXcBRyAd@_E\\aDT}BVwCZqEFeAJiBH_BLgDH{CDuADwB@}B@wB@{BAcDAsAAmAAk@EsBAi@Am@Ci@CwAEkAMcCGwAK_BEo@GcAUgDMwA[iD[_DIk@Ee@SwAYwBQkAUyA_@}B]qBKg@Mm@UkAc@sBYuAcAeEw@yCi@oBm@uB_AsCqBaGgAuCmBcFg@qAa@gAUo@a@eAu@yBWu@Og@q@{BOi@Kc@g@wB}@eEKg@SmAKk@SqAGg@SqAGk@K}@McASwBScCMyBGqACq@EuACm@CoAAg@AwAAgC?qABkDBsAF}BBo@PoDJ{ATwCT_Cz@{JXoDPaDFwADo@@i@DuABi@D{B@wA@qABqD?aA?wG@}B@cCBeGFoEJcDJoDPoDL{BRgDReCX}CLsAXcCPwAZcCb@yCd@{CVuANw@\\mBj@oCH]XuAPu@ZoAZqAv@sCXgAV{@t@cCZaA`@oA|@gCx@{BL[f@oAz@qBN[f@mAf@gATg@Ve@P_@h@gAr@sAr@qAZm@T_@HMhAsBfBoCb@o@b@o@lAcBJOfAwAfAsAzAkB`@c@^e@`@c@j@m@~@cAtB_C~AeBtAyAz@aAl@o@PSPSVYTWb@i@l@u@dAsAl@w@f@o@tAkBh@y@fAaBtBmDbAeBNUlA}Bv@yAlAeCdAwBfAcCfB{DfAaChAaCjAeCdAqBP]Xk@f@}@NWhAqBz@yA~@wAt@kA`AsAt@gANQZc@\\g@FGlB_ChC}C`AcAn@o@j@m@~@}@x@u@r@q@vBmBdCyBjB}Ar@o@ZWhC_CvAwADCvA}Ax@}@|@gARUfAyAv@gAfA_B`A_BnAyBpAaCl@sAP]`AyBf@oAh@wAj@{Af@uAt@sB|@gC~@iC|@yBn@wAZq@rAgCpAsBp@aA\\e@d@m@X]l@s@n@o@VWVYt@q@jA_ApAaAx@i@t@c@t@a@bBw@fBs@hBo@l@QvAc@pAc@d@MtAg@vAg@z@]\\Ov@]t@_@r@]\\S|@i@nAy@~@q@XSvAcAhEqDfA}@x@o@tAgAvAcArA{@zA}@vAq@XO~@a@z@[\\MxBu@pBg@b@KpAYdCe@jCe@fBa@x@UtAa@d@Qr@[l@Wv@c@f@W~@m@t@i@VQt@o@VUrAqAr@y@bAoAf@o@j@{@bA{AdBgCjAeBh@u@DEZa@v@aAPSl@q@f@g@VWfAcAd@a@x@o@p@g@lA}@lA{@f@[~@q@r@i@hAy@r@k@Z[p@q@f@g@h@m@d@k@`AuAf@u@z@yA|@eBLY^y@Ri@\\}@Na@DMX{@b@{AT_ARy@TeALq@He@F_@Js@TeBD]PoBLgBNeCFsALkDFuAB_@HgBFu@De@HeAFk@@IFg@Fc@D_@RoALo@RcADUR{@Po@H]Ty@La@Pi@Ne@b@iARg@P_@Pc@Xi@`@{@^o@Ta@FK^k@Va@r@aAp@y@Z]h@k@b@c@r@o@VUZWPOd@_@VOVQlAw@hBcAzBiAjAo@\\SXOtAw@x@g@p@a@b@[ZUv@k@t@k@ZWt@o@ZYVWXYVYXWX[X[VYV[V[V[n@y@V_@T[T]h@y@V]LS^o@R_@Ta@Ra@h@aAP_@Ra@Rc@Re@N[Pc@HSHQPc@Pe@^eA`@kAJ_@Ne@^oANg@Jc@BGTcAXiAJg@Lk@`@uBJk@RmAHk@PoANmARyA\\_DV_CX}BV{BJq@NcAVwAJi@TiALe@Jg@XiALg@t@aCVw@Rg@Pe@BEJYN]Tk@R_@P_@Vi@N[Tc@R_@R]Ta@j@aArAyBj@aAj@_AR_@R_@Ta@Tc@d@_AR_@Re@Rc@N_@Pc@Pg@Na@Pg@N_@Ne@Ne@Lc@Ni@XiALi@Li@Je@Jg@Lm@RkAHg@Hi@PqAFi@@MJy@VwCBODo@Dc@Be@?IBc@Bk@Ba@Bo@Bg@FsABu@Be@Do@FqAJuADc@Dk@Fk@De@Di@Hg@Fi@Hi@Fg@Hi@He@Hg@Ji@Lk@Li@Ha@Li@J_@Nk@Lc@Le@Ne@Nc@Ne@Ne@Pe@Pe@^aAPe@Pe@Pa@Pc@Pe@Ri@^cAPe@Pc@Nc@Nc@Lc@Na@Ng@Le@Le@Lg@XmALk@Je@He@He@Fa@Hi@Hm@Fg@Fi@Dg@Da@Fk@Dm@Bk@Di@@a@Bk@Bi@@g@@o@?m@@oAAwAAmAEaBEyAGeBG}ACqAAk@CkA?m@?a@?q@?m@@g@@i@@g@@m@Be@Bg@Bg@Dg@Dg@Dg@Di@Fk@Ho@Fe@Fe@Fa@He@Hi@Ji@Je@Jg@Lg@\\oAVaAPm@Z_A`@oA^iA\\gAZcAPi@Ja@Pi@Le@Le@Ja@Le@Je@Ji@Je@H_@Je@Fa@Ls@He@Fi@Hg@Dc@Fc@?AHo@Dg@Dg@NkCF}@Be@Bo@D}A@k@@i@@s@?wAA_BAm@C{AG{BIuBEo@Eq@Ey@Eq@Eo@K{AI{AIuAEi@GwAEy@Ck@Cm@Co@Ag@Ci@Au@AuAAk@?m@@}AByA@m@Bm@Bo@Bm@Dk@Dm@Bk@Di@Fm@Fm@Fo@Hg@Fm@BMVwA\\sBZaBFYLk@Lo@Le@ZwALg@Ha@^yAf@wBJe@No@Lm@Li@VsALi@Ji@Jm@Jk@Hg@Fg@Jo@Hm@Fg@Ho@Fm@Fe@Fm@Fq@Di@Do@Dm@HsADs@HqADu@Bm@Dm@Bm@L_CLeCLoBJeBDi@Fu@JsADe@Fk@Fk@LqANuANoAPuAPoAPmA\\aC^mCTuAb@aDRyANkAHu@NwAFk@LqADm@Hu@Dm@HkAH_BDu@Be@Ds@@i@Bo@Bg@@e@?O@g@@k@B{@?U?k@@q@?kA?w@?g@Am@?aAAo@Ao@Au@Ak@EkAG_BCa@Cu@IsAIsAIsAMuAMwAMsAOoAIm@MkASuAIm@QkAIo@QiAQmAUuAKu@My@QuAE_@Ga@K_AEa@Is@Gk@Ek@Ek@Ei@Eg@Cg@Eu@Cg@Cg@Cm@Ck@Am@Ak@Ao@CuA?k@?Y?Q?m@?m@?i@@c@@q@@m@@m@Bg@@W@YBg@Bm@Bi@Do@Di@Dk@Dm@Dg@Fm@Dg@Fg@Fk@Fg@RwARoAJm@Jm@Hi@Je@Hc@Lq@Je@Ja@Lo@ZoAZwAZmAXsAZqATkALi@TqAJm@RmAJo@Fg@Fk@Hk@Fk@Dg@Fm@B_@HiAD{@Bc@By@FcB@oA@s@?m@?yAAe@?q@EsAAs@Ck@Ck@Ek@Ck@Ek@KuAOuAC]Kw@OgAKs@UyASmAEUEWSiAOw@_@wBUyASuAQoAQqAGs@MmAIkAImAG}AEyACuAAoA?cB?{@@aAD{AFyABm@Dm@HmALyAN{ALcANiAFa@RmAXsALo@DUDSJe@TgAZsAZsAXsAXsAJi@Jm@RoARsAP{ALsAJyADi@Bm@DwA@k@@m@BsA?{AAuACyAAyAC{ACuAAyA?aC@}ABqADaBFqAH{AJoAL_BNyAV_CNwATwBTkCJoADg@HuABs@Bg@@Y@W@o@ByA@sAAqAAaBC}AEuAEyAEsAGwAKeCGcCC}AAaA?c@?W?W@oBD}A@k@FyAHmABk@TiCNwAHw@PiAZuBRgAFYD[d@cCd@cCb@{BPsAPmAHq@Fg@JiAFw@JyABw@DkABoA@wA?}AAkAA{@EeAE_AIyAKwAUaCs@cHSaCEo@IyACc@Cu@Cg@Ao@?i@Ac@?s@?o@@o@@m@@g@@o@Bk@Be@Dq@Be@Dm@Fm@Fm@Dk@Hi@Fe@Ho@RoAVsALm@XkAZqA\\sAZkAZmAPo@ZoAh@yBf@uB`@uBNw@RcA^}B^_CZgCV}BLsALsANyBBOBi@HwAHcBJeCFqAHiCDuABy@FiBHuCDuBH_CHkBFkAFwADw@HqAJsAL}AJoAPeBLuATgBPuANiANiAPcATwATqARkAZ_BXqAZwANs@VgA`@_Bb@_B^oAZgAV{@`@mAh@_Bl@_B\\_Ah@uAh@qAn@wAt@aBZo@n@qAXi@f@aAf@_A`BoCv@mAb@s@j@y@h@w@f@o@n@{@f@q@~@kAh@o@X]Z[\\_@b@e@z@_AjBoBhBmB~A}AZ]|@}@X[X[r@w@p@u@t@y@jAwAZ_@T]X[n@}@j@{@^i@NUXe@FK`@q@|@aBFMd@}@f@eAz@kBPe@b@eA`@iAPg@`@kA\\kA^oAVcAZoAZqAHc@XsAJo@FYJk@RiAHm@Hi@PoANsALkAFi@Dk@Dg@Dk@De@Dk@@c@LaC@UDaA@k@@s@@_@@iA@u@?c@@m@?i@Am@?g@?i@Ak@Ae@Ao@Ai@Cg@Cm@Ag@Ci@Ci@Eg@Ck@OqBEq@I_AIw@IaAQeBOyAc@eEg@wFS_CMwAIoAIgAIaBCe@Cg@Cm@EmAGuACyACi@CsAAk@AgAAuA?s@?oB?c@BiC?g@@iA@s@FiCBwABmAD{ADsABuADuADuC@{A@qB@qAAiA?eBAaBEoBGkBIuB?AC_@E}@KiBO{BQgBU{BI_AOkAi@sDi@iDa@uBk@qCYsAgAwEk@iCQs@w@yDu@aEUsAYoBOgAIq@Ky@W}BOcB]kEIoBGoAMaEGwDCuD@wB@q@BsCJaDLeEHmBHoCLwEHsDBqDAcEGoFImCMmCQ_Di@cHm@aH]wEQuCM}BI{CEwACqBCcC?gC@wCBcBDcBRgFZcG`@{GFgABg@FyANmDDyAFwB@yA@sA?}BA{A?uAE}BE_BK_DWwEWgD[aDCUk@eF[wB_@}Bk@gDe@gCeAyF_@_C[aCQ{AKsAGs@K{BE}C?e@Ae@?g@@y@@{@BgAHkBHsAH{@Fq@P{ARyAL_ANw@Jg@Ji@T}@Ps@Nm@Ni@Pg@L_@b@oAPc@b@gAVi@Ve@d@cAt@qAb@w@lBcDl@gArAeCpAkCRc@d@gAPa@Tk@`@cARm@^gAp@uBV_ALa@Jc@h@oBhA{Er@wCl@_Cx@wC\\mAL_@Nc@Pe@^eAf@mAHSPa@Pa@P_@R_@P]P_@T_@P[R]R]T]R]T]T[T[TYT[VYTYVYRWXYVYVWVWXWVWXUXWVSXUXUXQXUXQZSXQ\\SXOZQXOXM\\QZMZOZMZMZK\\M\\KZKZK\\K\\Ib@MVIZI\\I\\I\\IXG\\I\\I^IZI\\I\\IZG\\K\\I\\I\\IZIZK\\I\\I\\KZK\\K\\KZKZK\\KZK\\MZK\\MZOZM\\MZMXM\\O\\OXMZO\\OZQZO\\QXO\\QXQZQ^UVOZQXSZSZSXSXQ\\UVSZUXSXSZWb@]POXUVUXUXWVUZWVUXYVWXWVWTWZYVYVYXYRUp@s@l@q@X[VYX[TWX[V[TWVYV[TWXYV[VYVWTYXYVYVYVYVWVYTWXYVWf@e@b@c@b@a@PQRSXWXUXWXWXUXWXUVSZWXSZUVU\\UXSXSXUXSZSZSXSZQXSZQZSXQZSZQXO\\SZOXO\\Q\\OZQZOZOZO\\QVMVM^S\\OZO\\OZOZO\\OZM\\O\\OXMZO`Ac@r@[XMXO\\OZQ\\OZOZOZQZQXO\\QZOZQZQ\\S^URMvA{@t@e@lBoAlA}@nA_AfBwAbAy@|@w@vCgCVUp@m@hB}A`CqBlAaAfBuAfCeBnA{@pAw@v@a@v@c@v@a@tAo@v@_@vAm@rAi@vAg@rBo@~@YZIXIzA_@z@Sz@Qz@QzAWvASzAS|AOzAMxAKvAGvBIxBIvBG|BGxBGrCO~BQvBS|AQxAUvAUxAYtAYzA_@vBk@z@WpBq@tAg@tAk@rAm@l@WdAg@dAi@~A{@nAu@rA{@hBoAfBsAdBuAlAeA^[zAwA`BcB~AaBfAkAfAkAfAkAdAkAfAgAjAkAzBuBlAcAfBsAnA}@jBkAjC{AjBeAlBcAlBgAlBiAlAw@j@_@PMhAy@r@k@r@k@jAaAhAeAhAiAfAiAdAoAdAoAbAsA~@qAj@w@j@}@~@wAh@{@~@wA~@wAj@y@h@w@|@kAn@{@p@w@n@s@fAiAhAgAjAcAfBuAlBoAn@_@x@e@v@_@p@[vAo@|@]tAe@z@YvAa@vAa@vAc@x@UvAe@tAi@rAm@pAu@nAy@jA}@fAaAn@m@\\a@l@q@l@u@`@g@Xc@Xa@r@kAHQf@}@v@_B`@cA`@aA^eA\\gAd@cBb@oBViAJm@He@Fg@Hg@LcANmAJkAHgAHsADkAD{A@iA?k@?sAAiACqAGyAMsBMkBGc@Ec@SaBG_@Ik@I_@UqAMk@WgAWeA[cA_@mA[}@c@gAQc@Qa@Q_@e@aAiA_Cw@}Aa@u@We@Se@Qa@Qa@KWO_@c@mAOg@[cAQo@EMGWOm@Os@UmAIg@SoAIg@Ky@Ky@Gk@IaAEy@Ci@Eq@Cw@Ag@C}AAw@?mA@cABoAB{@DaAFoADi@Fi@Dg@J}@Ju@D[Hm@NaALs@Nq@RaAXkAXeAFURm@Pi@L]Vs@\\w@v@eBh@gAp@kAnGgKtA_CjAwBtAuCdAcCh@sAXs@h@yAbA}C\\gARu@V}@XkAV_AR_AJm@`@mBJk@RmARmARwAPqAD]Hi@RsBPkBDm@NkB@O@Y?CBUL{BHkAFsAHaBL_CJwBF_AF}@JoAJgANkANkANiAJk@`@{B^cB|@eE`@kBl@sCLq@PcAPeATqAFa@Fi@PmALmALkAJoAFs@HcAFgAFyABg@DoABqA@gA@uA@aCCkACoBEuAGuBGkAIoAEg@Ek@UgCK}@McAIq@Im@OaACKE[I_@G[G]SeAKi@S_AWkAc@gBSy@uAqFa@eBMg@SaAc@{BO_AUsAGi@Gg@Gi@Ge@QoBEk@IkAKyBAc@Ci@AcACwA?o@?oA@sA@k@@c@BmAFmADm@HmAHmALiAFq@Fc@Fg@NcAPkARmATiALo@Ja@Lc@XgALc@La@Po@Vw@Pe@L_@Pg@t@sBh@wA^_Al@cB`@gA~@kCj@cB^oAXaAf@iBRw@`@eBTcAP}@n@cDZwBPkAVqBNmAPmBJeAPaCNwBHuBH{BDwB@qABcD?sBGmECsAIeEKmEEwBEsBAiAAeC?eCDsCF{BP{CHsAVgDXuCbAkJRuBPwBN{BHqALaCDmCBu@@iA?m@?kA?kA?_A?ACm@AaAAi@EoAGoAMuBKwAW}Ck@gGgA}K[yDC_@?AEe@CYOwBIsAGiASoEGsBG{BCoAAm@AqAAyBAcD?kA@sA?m@@}@FoDHsDHmBL_DHuANkCNmBNkBLwAPcBFq@`@gD\\oCBIRsA\\cCv@mEPy@d@_CVkAxAiGXeAZmAx@oCjAsD|@kCv@qBz@}BfBkExAiDVm@Pa@Pa@~@yBdAeClA}Cb@kAl@cBb@qA^gAh@iB\\kAf@gBZmAH[ZqAb@mBl@wCp@qDl@oDNeAjAyHPkA`@mC~@aGf@sC\\kBP{@b@yBZsAf@{BXmA`@_BT{@b@{At@iClAyD^gA~AuE`AsCn@gBz@gChAgDh@}Aj@iBX}@nAuEPo@BITeAjAqEt@qDb@yBp@oD"
                     },
                     "start_location" : {
                        "lat" : 41.6148528,
                        "lng" : -0.6992187999999999
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "50.3 km",
                        "value" : 50297
                     },
                     "duration" : {
                        "text" : "27 mins",
                        "value" : 1649
                     },
                     "end_location" : {
                        "lat" : 41.4478487,
                        "lng" : 1.9911039
                     },
                     "html_instructions" : "Take the exit on the \u003cb\u003eleft\u003c/b\u003e onto \u003cb\u003eAP-7\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eToll road\u003c/div\u003e",
                     "maneuver" : "ramp-left",
                     "polyline" : {
                        "points" : "gh{zFo}jH@ODUD[~@kGPqA\\wCHm@RuBLqAJmAJmADo@JmBFyADsABeA@k@?i@@o@?Y?y@Ak@Aq@C{@EmACu@KoAKoAOoAQkAKk@SeAWkAWeASo@Y{@m@cB[w@q@_B_@y@Oa@Ys@Oe@Y_AY_Ag@gBU}@Oe@K_@Qg@IWMY?AYQOe@]_A[{@_@{@_@{@c@}@Ua@_DmFEIg@q@i@s@}@eAiAqAMMwAyAiAkAcBgBkAqAeAoAgAuAcAsAm@{@_AuAEG_@m@c@s@y@sAu@uAq@qAe@}@a@{@q@wAo@yAm@{Aa@aAi@yAi@_Bg@{AYaA[cAYeAWaAc@aBUcAWiA]aBScAUiAY{AKi@Ii@]iBSiAUmAUsAWkAWkAYkA]oA]gA[aA]_AUm@Um@a@_Ag@gAg@aAc@y@k@_Ag@{@i@{@k@}@k@}@S]u@qA_@s@k@iACGYg@Ui@c@eA_@gAYw@Uq@_@kA[mA[kAWmAUkAUmASmAQsAK{@I{@MmAKuAOyBQyCMqBEe@KeAMuAOiASwAUyAQcAKe@WmAYkA[mA]eA]iAQg@_@cAQe@Sg@a@aAg@cAo@mAu@qAm@aAi@y@CGc@k@Y_@Y_@OSOQQSSUSSy@y@iAeAo@i@c@]m@c@cAq@q@a@_Ag@oAm@{@a@_Aa@sAk@aAa@mAi@{BiAqAu@y@k@{@m@o@a@qAeAo@k@_B}AiAmAq@y@aAkAyAqBS[g@w@w@sAOYMUOWIOYk@u@aBsA{Ca@iAWs@Sm@u@eCe@gBQm@i@}BY{Ae@gCSqA_@sC[mCQ}ASiB_@_DYiB]qB_@iBc@gBSw@[eAK]c@sAe@sAc@gAi@kAa@}@q@sAq@iAu@mAk@{@u@cAq@y@o@s@o@s@wA{AmAqAw@{@u@{@o@{@w@cAm@aA{@wAs@uAc@}@a@{@e@iAo@eBYy@g@_Bg@gBc@mB]eBWyAk@eDk@mDQy@e@wB[uA_@wA[iAm@gB{@}Bk@qAsAoCw@uAk@}@mAeBcAsAaAgAk@m@wAuAa@_@}AkA{AcA}A}@mBeAkBaAy@e@y@e@k@a@mA{@y@q@y@s@o@m@}@aA}@cAoAcBo@}@u@oAk@cAa@u@m@oAi@iAm@uAq@{AcAgCwAyD_AkCk@aBw@cC}@uCaAiDu@kCy@eDk@}Bk@cCa@eBk@qCu@}Dw@qEYeB]yBw@oFs@aGc@wDEa@Ea@e@gF[gEQgCEy@IuBE{BCoA?sBD{CDyBPyCRgCPiBJ}@^_C^uBVmAd@uBV{@ZkAzAyEf@cBPk@Pk@VaAh@oB^mBTmAXkBLw@J}@RgBJmAJsAH_BFmABaABu@@}@@_A?}@Ac@CqC?e@EqAAGEgAMwBQwBQ{AIy@Ku@M{@Mu@i@qCeBcI_@qBUkAYmB]uCSuBKsAGgAIwBCiACgAA{A?wA@aA@}@BmAFyADiAl@{JHuBHaDByB?qACwBIeDI_BMiBQuBWwBOmA]qBSmAWoA_@wAa@}Ak@mBUq@Yu@_@cAWq@[s@o@qA[o@{@}Ai@{@{@sAaAwAk@w@m@u@kAwAQU_@c@c@c@cBcBAAaB{AaBsA_BkAoAy@eBeAeB_A}Au@y@]sAi@cA_@{@Y{@Uw@U_AUg@M}@Sy@O]Gq@Km@Im@G{@I_@CYCy@G_AG]AWAk@A{@Aa@A{@?{@@{AB_ABwBDmCF}@?a@?g@A_@A{@Co@Eg@Ei@Gw@IiASgASeAUKCWI_Bi@gCeA_B{@qAy@UM[Se@[iB{Ay@u@cAeAm@q@Y[?ASUKKw@}@gAkAa@c@o@s@WUu@q@c@]g@_@_DwBkAs@}@c@a@Si@Wi@SUKUIaA]gAWkAYc@Ie@Iq@Ke@E]EiAKoAGwAEwC?{BD{@@}@@}@AaAA{AGu@GyAOmASaB[c@Ke@MYIeA]]Ka@QUISI]Oe@Us@]q@a@c@Ym@_@q@e@c@[UQ[W[[c@_@c@a@e@e@[]SUUW]a@e@m@W]W][c@a@o@[i@[e@_@q@Ye@c@{@e@w@]m@o@gAUa@[e@]g@_@k@Y_@SWEGg@m@_@c@_@a@MOYWa@a@e@c@QOu@o@UQu@g@y@k@a@W_@UeAi@q@]g@Uy@]]MwAe@{@W_DaAkAa@wAk@qAi@{@e@w@c@q@a@u@g@o@e@]Y[W]YSQWWi@i@OOWWc@g@a@e@e@k@_@e@_@k@k@y@Ye@Wc@_@q@S_@g@_As@}Ac@eAo@_Bc@qAY_AYcAg@qBWkAk@{C]{BWwBSoBMcBKkBEiAGyBE{BEyBE_BGiBGqAI_BKuAQ_BEa@QuAGc@SqASmAIa@UiAYoAg@qB_@qAg@}Aa@eAg@uAo@wAi@iAu@uAk@cAa@q@[a@_AuAi@u@q@y@s@{@i@q@iAsAu@_Am@{@c@m@e@q@g@y@k@aAU_@Uc@OYw@{Ae@cAs@cBUk@]}@Yw@_@gAW{@a@uA[gAYmA[qA[qAI[AGOs@S{@YiAWgA[iAi@cBa@kAc@kA_@}@Sg@_@w@c@}@Yk@g@{@S]c@q@[i@k@y@k@s@oBaCo@q@s@o@}@w@s@k@e@_@qA{@uAw@aAg@eAe@eAc@iA_@eA[_AUy@QuAUkAQmAM}@KeAKoAM{@M_AO]Go@OiA[w@S_@Mw@YYMw@[SKe@S}@i@_@Sk@]s@e@w@k@o@g@u@q@kAgAm@q@eAkAk@u@i@s@Ya@y@qAc@w@_@q@s@sAu@_Bg@oAoB}EgAgCWk@IOIQ_@s@IQKQoAmB]i@g@s@wAkBgAwAw@aAgAuA}@oAU]Ua@g@y@OYWc@S_@c@}@Ug@q@}ASi@M]k@cB]gA[gA[gAQy@WiAY}A]wB]iCSmBOcBKkBKsBEyAAe@G{CCeAA]IqDGgBQoDM{BKmAQ}BMkASmBQqAOmASsAc@uC[sBQeASuA]uBOgAUqAKw@CUQmA_@{CMqAOwAOwAKqAIkAMiBI}AOgCA_@EcAAQ?CAWKmBEoAGkAI}BKwBIsAGmAIsAOqBKqAOaBOeB]{CKy@QqAU_BUuAOaASgAMk@UgAMk@Os@_@yAYiA]iA_@mA]cAa@kAo@aB]{@kAiCg@eAg@aAgAoBcAeBg@y@e@u@k@}@i@y@o@_A}@qA_AqAsAgB_@i@_@c@i@q@g@m@_@e@i@m@_AgAw@y@s@u@o@i@s@o@q@i@gAw@]U_@Wq@_@k@[{@a@o@Yq@W}@[u@S{@S{@Qg@Kk@Ks@Oq@M_@Is@QYI_@KWI[K_@Ma@Oe@Si@W_@Qc@WYQ[QYSg@[_@Ys@k@]Y[Y_@_@_@a@[][[W[_@e@[c@Ya@U_@Wa@Ye@[g@S]Sa@Yi@Yo@Ui@Uk@Wu@Qg@Y{@Qq@Qw@Oo@Oo@Mi@Ki@Mq@O{@Kq@Gk@Im@Is@Gi@Gu@IcAGw@Gk@Cg@Ee@OkCMeBM_BCYC[I_AIy@I}@Gi@Ec@Ko@OgAOgAIg@Kq@ScAoA_GYsAYmAWoAUeASiAQgAM}@Ie@ScBO}AI_AIoACg@IcBEcAAeAAu@AgA?e@?g@?kC?[?Y?y@?{@A{@?s@AaAEkAEkAAg@IsAIkAIgAGu@K}@Is@Ku@Ku@My@Mq@Mu@WmA[sA[oAYaAK_@Qk@Qi@Sk@Ws@[w@a@aAYo@c@}@g@}@e@}@e@u@k@}@o@_Ao@y@}@iAw@}@k@m@i@m@k@o@m@s@m@s@i@s@c@k@c@m@e@w@o@eAg@}@]s@m@qAg@kAi@qAa@eA[cAe@oAUq@c@iAc@iA[u@_@}@[q@a@w@Wg@]m@_@o@g@w@iA_Bm@w@i@q@Y]e@k@q@{@y@cAu@eAi@u@o@aAi@}@c@y@e@}@Yi@O_@k@sACE_@aA]aAUq@Uw@_@mAYiAOs@U}@UiAQcASiAo@wDW{AY_BUsAScAWkAi@cCWgACOOg@c@eBU}@Sq@ES]oA]oAWeAU}@S}@]cBKm@Ie@Im@Ga@CWUaBMiAMoAWsCUwBGk@Gm@MgAQmA]oBUsAQs@Oo@CIq@}COq@Qy@QaAKa@U}@AAOk@Sq@Oc@Sa@i@mAEK_AkBOc@M]EKGUGYI[SaAa@uBUuA{@kFMy@]wBi@sCe@mBQm@k@iBe@wAe@iAwA{CkAsBwAyBa@k@o@{@_BcC{AgCmAkCO]O_@Oc@Wu@y@yCo@kCIi@[oBUoBQoBSuCWqCY}BYiBYqAUkAa@qBUmAQkAO{@Im@QsAQ}AOwBIcBAEC{@A{AAiA@mA@m@Dw@@Y@ULsB@U@IB]Ho@@I@K?EHi@@QBMFa@BKRkAPy@ZwAPm@Vy@BIRq@Pi@To@Na@\\gAd@wAfAaENe@VmAF_@ReAJu@NeAN_BNqBJ}AD}B@mB?C?}@?UAm@G}B?GCYE_AG{@Cc@CQIw@QiBIq@YeBw@_EcAqEYkBa@wBWmBM}AM{BEsBC}ABaB@i@@[J}BDe@LmAB]NcAZkBNu@b@gBd@cBf@yAd@sAr@oBj@iBf@oBXsA@GVyABMBWJu@BW@QBOFo@@KF}@FiADiADiB?C@]?]?yACuD?U?G@kAA_D@UBu@Dy@Be@Dy@D]LsAVuBHe@RcABQFSFY?CRy@\\qA\\eAj@{ADKj@qAt@wAr@kA|@qATWZ_@r@w@`@a@l@i@f@a@VQfAs@\\Qj@[JEn@]h@Ud@Ox@Yt@QNEVGp@Q|AYlASv@MtAShAMr@KhAOr@I`AOx@Kr@MfAQz@O~@QnAYdAUvBi@xAc@bA]lAc@x@[x@[zAq@tBaA|@c@rCaB~B{A`Aq@BAPMBCNMHEb@]nC}BbBeB~AeBbAsABEHI`AyA|@wAHO`AiBr@aBh@oAd@mAj@eBZ}@n@mC"
                     },
                     "start_location" : {
                        "lat" : 41.2686837,
                        "lng" : 1.5357647
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "8.3 km",
                        "value" : 8253
                     },
                     "duration" : {
                        "text" : "5 mins",
                        "value" : 304
                     },
                     "end_location" : {
                        "lat" : 41.3812113,
                        "lng" : 2.0296903
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eB-23\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003ePartial toll road\u003c/div\u003e",
                     "polyline" : {
                        "points" : "ah~{Fk{cKt@mDxAeH\\wAXaA\\gAJY`@mAXw@@CJYJYVi@BGHM\\o@@CNYXe@NST[NQFKl@s@n@q@`AeAr@o@t@q@bBmAf@[ZOr@a@z@_@t@Yx@WTGbA[pA[xCu@bA]xAg@xAm@n@YpBgAv@c@|A}@rAu@pAu@|CmBrBeArAs@h@Yn@Yx@]tAk@nDqAPGLEbCq@bBc@~A[vAYz@OvAUxBYpPgBbC[p@I~Ba@~@S~@Sh@Oh@MlBc@r@QVC`@El@Oj@Qd@Mv@UnCaApBs@nAe@JEbDqAlHcDt@]dF_CrBcAtCsAzCwAJIZO^U`@SlHmDpJqEtFkCHEPIZOvFmCnCmAfEqBtDeBpAo@nFiCx@c@pAu@t@e@lBsAp@i@\\WTQjAaAbGcFl@i@t@m@n@g@v@k@v@m@n@a@|@i@nBeA`Ac@jAg@tAg@tAc@~@WbAU|@UzCo@vBg@j@OfA[~Ak@pAi@|@a@ZMXOj@[zA{@j@[~@i@bAm@`@U`@WRKPKJG`@UJEn@[p@[ZOXMb@Q~@[lBm@rCs@pCu@rCs@tDaA~Bo@VGjA[B?tAa@d@Ox@Yl@ULGb@St@c@"
                     },
                     "start_location" : {
                        "lat" : 41.4478487,
                        "lng" : 1.9911039
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "6.6 km",
                        "value" : 6602
                     },
                     "duration" : {
                        "text" : "4 mins",
                        "value" : 261
                     },
                     "end_location" : {
                        "lat" : 41.3450011,
                        "lng" : 2.0840791
                     },
                     "html_instructions" : "Take exit \u003cb\u003e5\u003c/b\u003e to merge onto \u003cb\u003eE-90\u003c/b\u003e/\u003cb\u003eA-2\u003c/b\u003e toward \u003cb\u003eBarcelona\u003c/b\u003e/\u003cb\u003eRonda Litoral\u003c/b\u003e/\u003cb\u003eAeroport\u003c/b\u003e",
                     "maneuver" : "ramp-right",
                     "polyline" : {
                        "points" : "qgq{FqlkK\\AB?NIt@_@lAm@d@Uh@Wb@Sj@Sl@Sd@Ox@U~@Wj@Sv@[`@QXOVOXQ\\STQRO`@[d@_@\\Yp@i@t@m@r@i@j@_@t@i@~AiArA_AfAu@lA{@t@i@l@i@\\YV[TWb@m@T_@P]Xi@Tk@d@mApA_Dr@aBf@eA`@aATe@?C@ADc@`@s@Pa@J[`@cAt@cCd@eB^{AVeAT{@\\uAXgAXmAVeA\\oAVeAPk@XaAVu@\\cAZs@Ti@Vi@Xk@R_@`@s@d@w@`@k@d@o@l@s@^c@j@m@LKd@c@t@o@n@e@t@g@v@e@x@c@p@Yt@]n@Sr@Ut@Sz@Sx@Q|@Ov@K|@Iz@GbBGhCCzAE`AG~@Gz@Mb@G\\GpBi@|@YbA_@~@a@l@[n@_@b@Yd@Y`@[l@g@`@]p@o@n@q@f@k@d@k@r@aAp@eApA{BjCsEJQJQZk@\\o@\\m@`@q@x@uAhAsBNYNWf@y@z@yAx@_Bn@kA`AuBr@{Ax@mBjAyCr@oBf@}Aj@cBv@eCd@cBPo@ZgA\\yA\\_Bf@uBd@cCb@cCRkAJo@L_AT}ATeBRaBLoAPuBH_AJqAJeB|@qPV_ETqD\\iGf@gJL}BLiCF_AB_ABo@@g@@c@BoB"
                     },
                     "start_location" : {
                        "lat" : 41.3812113,
                        "lng" : 2.0296903
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "8.6 km",
                        "value" : 8559
                     },
                     "duration" : {
                        "text" : "6 mins",
                        "value" : 356
                     },
                     "end_location" : {
                        "lat" : 41.36645780000001,
                        "lng" : 2.1720344
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eB-10\u003c/b\u003e",
                     "polyline" : {
                        "points" : "gej{Fo`vK?{ACyCEiDAc@?qA?g@?c@?q@@k@Bw@B}@HkAJuABWDUDe@Js@Lw@NaANs@Li@Je@No@\\iAHW`@iA^aAb@aAd@}@R_@d@y@fAoBR[Xg@Zq@Zo@Zq@Na@`@cA\\kAf@gBXkA\\oBLq@Fc@TsBJoAHcAJ}ADuAD}@@k@DkB@y@@W?S?SH}ED{AB}AHaF@[?A?YHkEBmADyCFyCFaC@y@@QF}BFsABg@FqAJwB^yFHaADe@Dc@?A@E@G@G@G?E@GD]Z}CJy@BYD{@LeCBs@@e@@g@BiB?O?oAGkCKaCIsAEe@UwBOiAQmASiAUiAUiAYkAm@cCe@sBs@sC]wA[sAo@kCwRwx@e@kBkA{EgAqEq@qCu@oC_BkGQm@q@cCu@gCy@kCgDqKIYkAwDkAyDUo@So@w@iC]eAK[o@eBYy@]aAOa@Qg@Qm@KYSu@I[GWI[Qq@I_@EWGWMu@k@gDKs@Ge@CMSwAE[QsAIu@E]E]G]CUMu@G[I_@Qu@IWW}@Qg@IWKUM]Wk@KY[m@[k@]k@]g@OSU[_@a@IIKIe@e@{@w@oAiAy@y@_@]Y[WUQSQSQQMOs@{@a@e@iBqBgBsBMOMOa@g@c@g@y@gAa@m@SYaA{As@eA_@m@a@o@QY]m@_@u@a@u@]q@q@kA]m@QYu@iAu@iAs@iAsAqB_@i@MQSUQQQSSQUQSQg@]aAq@aC_ByCqBcAo@q@_@WOUKYOWK{@_@k@We@Sg@W"
                     },
                     "start_location" : {
                        "lat" : 41.3450011,
                        "lng" : 2.0840791
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.6 km",
                        "value" : 550
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 43
                     },
                     "end_location" : {
                        "lat" : 41.3710355,
                        "lng" : 2.1738651
                     },
                     "html_instructions" : "Take the exit toward \u003cb\u003e21 Ciutat Vella\u003c/b\u003e/\u003cb\u003eParal.lel\u003c/b\u003e/\u003cb\u003eLa Rombla\u003c/b\u003e/\u003cb\u003eMontjuïc\u003c/b\u003e/\u003cb\u003ePort vell\u003c/b\u003e",
                     "maneuver" : "ramp-right",
                     "polyline" : {
                        "points" : "kkn{FefgLQWCCAAg@]q@e@aAs@q@g@WSOM_Aw@IGIEMGQIYC_@@O@O@]DOBK@IAKAGAKCg@KSEUCkAOcAKmAOEAC?E@C?OD"
                     },
                     "start_location" : {
                        "lat" : 41.36645780000001,
                        "lng" : 2.1720344
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.3 km",
                        "value" : 321
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 51
                     },
                     "end_location" : {
                        "lat" : 41.3733679,
                        "lng" : 2.1760183
                     },
                     "html_instructions" : "Continue onto \u003cb\u003ePasseig Josep Carner\u003c/b\u003e",
                     "polyline" : {
                        "points" : "_ho{FuqgLIEECIEOMwB_BeBqA{C_CW]EKEICMCKAO"
                     },
                     "start_location" : {
                        "lat" : 41.3710355,
                        "lng" : 2.1738651
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.3 km",
                        "value" : 305
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 77
                     },
                     "end_location" : {
                        "lat" : 41.3754704,
                        "lng" : 2.1776184
                     },
                     "html_instructions" : "At the roundabout, take the \u003cb\u003e3rd\u003c/b\u003e exit and stay on \u003cb\u003ePasseig Josep Carner\u003c/b\u003e",
                     "maneuver" : "roundabout-right",
                     "polyline" : {
                        "points" : "qvo{Fc_hL@E?K?K?K?ICKAOAEACAEACACACKYEEEEGEGEGCGCEAEAC?E?E?EAE@K@MDEBC@EBEDM@O?MAKCIEUQMKgBwAu@m@"
                     },
                     "start_location" : {
                        "lat" : 41.3733679,
                        "lng" : 2.1760183
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.8 km",
                        "value" : 769
                     },
                     "duration" : {
                        "text" : "3 mins",
                        "value" : 155
                     },
                     "end_location" : {
                        "lat" : 41.3812667,
                        "lng" : 2.1823403
                     },
                     "html_instructions" : "At the roundabout, take the \u003cb\u003e1st\u003c/b\u003e exit onto \u003cb\u003ePasseig de Colom\u003c/b\u003e",
                     "maneuver" : "roundabout-right",
                     "polyline" : {
                        "points" : "ucp{FcihL?MAOCMEMEKIIIIKEGEGCEAIAG?G@G@GB][aE}Cm@a@_BmAUQwBaBaAs@{AmAMIAAQMs@i@c@[kA}@e@]}@q@QM_Am@"
                     },
                     "start_location" : {
                        "lat" : 41.3754704,
                        "lng" : 2.1776184
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.4 km",
                        "value" : 425
                     },
                     "duration" : {
                        "text" : "2 mins",
                        "value" : 135
                     },
                     "end_location" : {
                        "lat" : 41.383992,
                        "lng" : 2.1787661
                     },
                     "html_instructions" : "Turn \u003cb\u003eleft\u003c/b\u003e onto \u003cb\u003eVia Laietana\u003c/b\u003e/\u003cb\u003ePlaça d'Antonio López\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eContinue to follow Via Laietana\u003c/div\u003e\u003cdiv style=\"font-size:0.9em\"\u003eParts of this road may be closed at certain times or days\u003c/div\u003e\u003cdiv style=\"font-size:0.9em\"\u003eTurn may not be allowed at certain times or days\u003c/div\u003e",
                     "maneuver" : "turn-left",
                     "polyline" : {
                        "points" : "}gq{FsfiL]b@u@jAMP}@xAs@`AeCpDoBhCGLy@lASR"
                     },
                     "start_location" : {
                        "lat" : 41.3812667,
                        "lng" : 2.1823403
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "40 m",
                        "value" : 40
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 21
                     },
                     "end_location" : {
                        "lat" : 41.3837258,
                        "lng" : 2.1784393
                     },
                     "html_instructions" : "Turn \u003cb\u003eleft\u003c/b\u003e onto \u003cb\u003ePlaça de l'Àngel\u003c/b\u003e",
                     "maneuver" : "turn-left",
                     "polyline" : {
                        "points" : "}xq{FiphLV^Z`@"
                     },
                     "start_location" : {
                        "lat" : 41.383992,
                        "lng" : 2.1787661
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.1 km",
                        "value" : 136
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 54
                     },
                     "end_location" : {
                        "lat" : 41.3828359,
                        "lng" : 2.1773174
                     },
                     "html_instructions" : "Continue onto \u003cb\u003eCarrer de Jaume I\u003c/b\u003e",
                     "polyline" : {
                        "points" : "iwq{FgnhLp@x@\\b@t@fAj@x@"
                     },
                     "start_location" : {
                        "lat" : 41.3837258,
                        "lng" : 2.1784393
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "54 m",
                        "value" : 54
                     },
                     "duration" : {
                        "text" : "1 min",
                        "value" : 24
                     },
                     "end_location" : {
                        "lat" : 41.3824823,
                        "lng" : 2.1768655
                     },
                     "html_instructions" : "Continue onto \u003cb\u003ePlaça de Sant Jaume\u003c/b\u003e",
                     "polyline" : {
                        "points" : "wqq{FgghLJLz@jA"
                     },
                     "start_location" : {
                        "lat" : 41.3828359,
                        "lng" : 2.1773174
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.3 km",
                        "value" : 330
                     },
                     "duration" : {
                        "text" : "3 mins",
                        "value" : 164
                     },
                     "end_location" : {
                        "lat" : 41.3803209,
                        "lng" : 2.1741617
                     },
                     "html_instructions" : "Continue straight onto \u003cb\u003eCarrer de Ferran\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eMay be closed at certain times or days\u003c/div\u003e",
                     "maneuver" : "straight",
                     "polyline" : {
                        "points" : "ooq{FmdhL~@nAd@n@~@lAj@x@^f@^b@@@t@dAb@l@PRn@~@"
                     },
                     "start_location" : {
                        "lat" : 41.3824823,
                        "lng" : 2.1768655
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.6 km",
                        "value" : 560
                     },
                     "duration" : {
                        "text" : "3 mins",
                        "value" : 207
                     },
                     "end_location" : {
                        "lat" : 41.3846638,
                        "lng" : 2.1708063
                     },
                     "html_instructions" : "Turn \u003cb\u003eright\u003c/b\u003e onto \u003cb\u003eLa Rambla\u003c/b\u003e\u003cdiv style=\"font-size:0.9em\"\u003eClosed 11:00 AM – 3:00 PM\u003c/div\u003e",
                     "maneuver" : "turn-right",
                     "polyline" : {
                        "points" : "_bq{FosgLkDrCABKHEBGFGDm@h@i@b@}@t@GFMJyBfBWTIHGDC@C@C@C@QHIBKF_Bx@kDjB"
                     },
                     "start_location" : {
                        "lat" : 41.3803209,
                        "lng" : 2.1741617
                     },
                     "travel_mode" : "DRIVING"
                  },
                  {
                     "distance" : {
                        "text" : "0.2 km",
                        "value" : 226
                     },
                     "duration" : {
                        "text" : "2 mins",
                        "value" : 104
                     },
                     "end_location" : {
                        "lat" : 41.3850482,
                        "lng" : 2.173415
                     },
                     "html_instructions" : "Turn \u003cb\u003eright\u003c/b\u003e onto \u003cb\u003eCarrer de la Canuda\u003c/b\u003e",
                     "maneuver" : "turn-right",
                     "polyline" : {
                        "points" : "c}q{Fq~fLGOB}A?k@?UASKkBAEQyAIa@EQEWCKEMGS"
                     },
                     "start_location" : {
                        "lat" : 41.3846638,
                        "lng" : 2.1708063
                     },
                     "travel_mode" : "DRIVING"
                  }
               ]